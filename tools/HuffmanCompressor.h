#ifndef HUFFMANCOMPRESSOR_H
#define HUFFMANCOMPRESSOR_H

/**
 * This class represents a Huffman tree. Given a occurance table,
 * it can construct a tree to build the codes needed for compression.
 *
 * This piece of code is adapted from 'Data Structures in C' by 
 * Adam Drozek and Donald L. Simon, published by PWS Publishing
 * Company.
 */

#include <vector>
#include <map>
#include "SpecialToken.h"
#include <pgal/VariableCode.h>
#include <pgal/QException.h>
#include <pgal/AbstractCompressor.h>

using namespace std;

class HuffmanCompressor : public AbstractCompressor {
public:
  QEXCEPTIONCLASS( UnknownSymbol );
  QEXCEPTIONCLASS( UnknownCode   );

  HuffmanCompressor( const vector<SpecialToken>& occuranceTable );
  virtual ~HuffmanCompressor() { }
  
  map<SpecialToken,VariableCode> m_SymbolsToCodes;
  map<char,VariableCode>         m_SingleCharsToCodes;
  map<VariableCode,SpecialToken> m_CodesToSymbols;
  map<VariableCode,char>         m_CodesToSingleChars;
  SpecialToken                   m_EndToken;

  void compress( VariableCode& buffer, const char *, int num );
  void decompress( string& buffer, VariableCode& );

protected:
  HuffmanCompressor();

  class TreeNode {
  public:
    TreeNode( const SpecialToken& s ) : m_Token(     new SpecialToken( s ) ),
                                        m_Left(      NULL                  ),
                                        m_Right(     NULL                  ) {}
    TreeNode() : m_Token(     NULL           ), 
                 m_Left(      NULL           ),  
                 m_Right(     NULL           ) { }

    virtual ~TreeNode() { if ( m_Token != NULL ) { delete m_Token; } }

    SpecialToken      *m_Token;
    TreeNode          *m_Left;
    TreeNode          *m_Right;
  };

  void deleteNode( TreeNode * );
  void createCodes( TreeNode *, const VariableCode& );
};

#endif

