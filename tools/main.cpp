/**
 * This class examines the database and counts how many occurances of
 * each tag appear in all the genotypes. Then it produces a source 
 * code file that can be compiled to produce a compressor class for PGAL.
 */

#include <stdio.h>
#include <pgal/PDatabase.h>
#include <pgal/PResultSet.h>
#include <pgal/PRow.h>
#include "SpecialToken.h"
#include <set>
#include <string>
#include <vector>
#include "HuffmanCompressor.h"
#include <errno.h>

using namespace std;

static void printUsage() {
  fprintf( stderr, "buildClass {evolutionaryPartRunNo} {class name}\n" );
  exit( 1 );
}

static const char *escapeString( char *buffer, const char *s ) {
  char *t = buffer;
  while ( *s != '\0' ) {
    if ( *s == '\'' || *s == '"' || *s == '\t' || *s == '\r' || *s == '\n' ||
	 *s == '\\' ) {
      *t++ = '\\';
    }
    if ( *s == '\t' ) {
      *t++ = 't'; s++;
    } else if ( *s == '\n' ) {
      *t++ = 'n'; s++;
    } else if ( *s == '\r' ) {
      *t++ = 'r'; s++;
    } else if ( *s == '\\' ) {
      *t++ = '\\'; s++;
    } else {
      *t++ = *s++;
    }
  }
  *t = '\0';

  return buffer;
}

int main( int argc, char **argv ) {
  try {
    if ( argc != 3 ) {
      printUsage();
    }

    /**
     * First we'll load in all the tags...
     */
    set<SpecialToken> allTags;
    char buffer[ 1024 ];
    while ( fgets( buffer, 1023, stdin ) != NULL ) {
      if ( buffer[ strlen( buffer ) - 1 ] == '\n' ) {
	buffer[ strlen( buffer ) - 1 ] = '\0';
      }

      allTags.insert( SpecialToken( buffer ) );
    }
   
    set<SpecialToken> singleChars;
    /**
     * Right - we'll quickly fill in all the other characters so nothing
     * will be missed out.
     */
    char ch;
    buffer[ 1 ] = '\0';
    for ( ch = 32; ch < 127; ch++ ) {
      buffer[ 0 ] = ch;
      singleChars.insert( SpecialToken( string(buffer),true ) );
    }
    
    buffer[ 0 ] = '\r'; 
    singleChars.insert( SpecialToken( string(buffer),true ) );

    buffer[ 0 ] = '\n'; 
    singleChars.insert( SpecialToken( string(buffer),true ) );

    buffer[ 0 ] = '\t'; 
    singleChars.insert( SpecialToken( string(buffer),true ) );

    /*{ // DEBUG block
      map<SpecialToken,int>::iterator z;
      for ( z = allTags.begin(); z != allTags.end(); z++ ) {
	fprintf( stderr, "%d\t%s\n",
		 z->second, z->first.getToken() );
      }
      }*/

    SmallString num = argv[ 1 ];
    int runNo = num.getLongValue();

    const char *className = argv[ 2 ];

    /**
     * We do a big search of all the genotypes in the given evolutionary
     * part run no.
     */
    PDatabase database( "pgalUser",
			"pgal",
			"127.0.0.1",
			"pgalDB" );
    SmallString query;
    query = "SELECT ID FROM genotype WHERE evolutionaryPartRunID=";
    query += runNo;
    query += " and genotypeValue IS NOT NULL";

    PResultSet *resultSet = database.RunSelectQuery( query, true );
    
    int noOfGenotypes = resultSet->GetNumRows();
    if ( noOfGenotypes == 0 ) {
      fprintf( stderr, "Couldn't find any genotypes with that part run no." );
      exit( 1 );
    }

    vector<unsigned long> genotypeIDs;
    unsigned long i;
    for ( i = 0; i < noOfGenotypes; i++ ) {
      PRow *row = resultSet->GetNextRow();

      genotypeIDs.push_back( row->GetLongField( 0L ) );
    
      delete row;
    }

    delete resultSet;

    set<SpecialToken>::iterator k;
    vector<unsigned long>::iterator j;
    for ( j = genotypeIDs.begin(); j != genotypeIDs.end(); j++ ) {
      query = "SELECT genotypeValue,sizeInBits FROM genotype WHERE ID=";
      query += *j;

      resultSet = database.RunSingleRowSelectQuery( query );

      PRow *row = resultSet->GetNextRow();
      
      const char *bytes = row->GetStringField( 0L );
      unsigned long sizeInBits = row->GetLongField( 1 );

      /**
       * Now lets analyse this file...
       */
      SpecialToken stream( bytes, sizeInBits / 8 );
      
      int num;
      for ( i = 0; i < sizeInBits / 8; ) {
	k = allTags.find( stream );
	if ( k == allTags.end() ) {

	  /*{ // DEBUG block
	    char buf2[ 10 ];
	    strncpy( buf2, stream.getToken(), 9 );
	    fprintf( stderr, "Cannot find \"%s\"\n", buf2 );
	    }*/

	  /**
	   * Right - we'll divide it up as a character.
	   */
	  k = singleChars.find( stream );
	}

	k->addToFrequency( 1L );
 
	stream.advance( k->getSize() );
	i += k->getSize();

	if ( k->isEndToken() || strlen( stream.getToken() ) == 0 ) {
	  break;
	}
      }

      fprintf( stderr, "end 1\n" );

      delete row;
      delete resultSet;
    } // End all genotypes

    /**
     * Now we merge the sets...
     */
    vector<SpecialToken> allItems;
    for ( k = allTags.begin(); k != allTags.end(); k++ ) {
      allItems.push_back( *k );
    }
    
    for ( k = singleChars.begin(); k != singleChars.end(); k++ ) {
      allItems.push_back( *k );
    }
    
    HuffmanCompressor compressor( allItems );

    /**
     * Now we print off the source code...
     */
    char filename[ 100 ];
    sprintf( filename, "%s.h", className );
    FILE *output;
    errno = 0;
    if ( ( output = fopen( filename, "w" ) ) == NULL ) {
      fprintf( stderr, "Cannot open file '%s' - error states: %s\n",
	       filename, strerror( errno ) );
      exit( 1 );
    }

    /**
     * First we print out all the occurances within a comments branch.
     */
    fprintf( output, 
	     "/**\n"
             " * Occurances of each token:\n"
             " *\n" );
    char bigBuf[ 1024 ];
    vector<SpecialToken>::const_iterator b;
    for ( b = allItems.begin(); b != allItems.end(); b++ ) {
      if ( b->isEndToken() == true ) {
	strcat( bigBuf, "<end token>" );
      } else {
	escapeString( bigBuf, b->getToken() );
      }

      fprintf( output, " * %lu\t%s\n",
	       b->getRealFrequency(), bigBuf );
    }
 
    fprintf( output, 
	     " */\n"
	     "\n"
	     "#ifndef %s_H\n"
             "#define %s_H\n"
	     "\n"
	     "/**\n"
             " * This class was generated by the PGAL tool buildClass.\n"
	     " */\n"
	     "\n"
	     "#include \"HuffmanCompressor.h\"\n"
	     "\n"
	     "class %s : public HuffmanCompressor {\n"
	     "public:\n"
	     "  %s();\n"
	     "};\n"
	     "\n"
	     "#endif\n",
	     className, className, className, className );
    fclose( output );

    sprintf( filename, "%s.cpp", className );
    errno = 0;
    if ( ( output = fopen( filename, "w" ) ) == NULL ) {
      fprintf( stderr, "Cannot open file '%s' - error states: %s\n",
	       filename, strerror( errno ) );
      exit( 1 );
    }

    fprintf( output,
	     "#include \"%s.h\"\n"
	     "#include <vector>\n"
	     "#include <string>\n"
	     "#include \"SpecialToken.h\"\n"
	     "\n"
	     "using namespace std;\n"
	     "\n"
	     "%s::%s() {\n"
	     "  struct S_StringPair {\n"
	     "    const char *m_Tag;\n"
	     "    const char *m_Code;\n"
	     "  };\n"
	     "\n"
	     "  struct S_StringPair tagArray[] = {\n",
	     className, className, className );
    
    string buf2;
    string endCode;
    map<SpecialToken,VariableCode>::const_iterator y;
    for ( y = compressor.m_SymbolsToCodes.begin();
	  y != compressor.m_SymbolsToCodes.end();
	  y++ ) {
      if ( y->first.isEndToken() == true ) {
	y->second.printAsBinary( endCode );
      } else {
	buf2.clear();
	y->second.printAsBinary( buf2 );
	fprintf( output,
		 "    {\"%s\",\t\"%s\"},\n", 
		 escapeString( bigBuf, y->first.getToken() ), 
		 buf2.c_str() );
      } 
    }
    
    fprintf( output,
	     "    { NULL,\tNULL }\n" 
	     "  };\n"
	     "\n"
	     "  struct S_StringPair singleCharArray[] = {\n" );
    
    char tempBuf[ 10 ];
    map<char,VariableCode>::const_iterator z;
    for ( z = compressor.m_SingleCharsToCodes.begin();
	  z != compressor.m_SingleCharsToCodes.end();
	  z++ ) {
      buf2.clear();
      z->second.printAsBinary( buf2 );

      sprintf( tempBuf, "%c", z->first );

      fprintf( output, "    {\"%s\",\t\"%s\"},\n", 
	       escapeString( bigBuf, tempBuf ),
	       buf2.c_str() );
    }

    fprintf( output,
	     "    { NULL,\tNULL }\n" 
	     "  };\n"
	     "\n"
	     "  SpecialToken endToken;\n"
             "  VariableCode endCode;\n"
             "  endCode.readAsBinary( \"%s\" );\n"
             "  m_SymbolsToCodes.insert( pair<SpecialToken,VariableCode>\n"
             "                           ( endToken, endCode ) );\n"
	     "  m_CodesToSymbols.insert( pair<VariableCode,SpecialToken>\n"
	     "                           ( endCode, endToken ) );\n"
             "\n"
	     "  int i;\n"
	     "  for ( i = 0; tagArray[ i ].m_Tag != NULL; i++ ) {\n"
	     "    SpecialToken token( (string)tagArray[ i ].m_Tag );\n"
	     "    VariableCode code;\n"
	     "    code.readAsBinary( tagArray[ i ].m_Code );\n"
	     "\n"
	     "    m_SymbolsToCodes.insert( pair<SpecialToken,VariableCode>\n"
	     "                             ( token, code ) );\n"
	     "    m_CodesToSymbols.insert( pair<VariableCode,SpecialToken>\n"
	     "                             ( code, token ) );\n"
	     "  }\n"
	     "\n"
	     "  for ( i = 0; singleCharArray[ i ].m_Tag != NULL; i++ ) {\n"
	     "    VariableCode code;\n"
	     "    code.readAsBinary( singleCharArray[ i ].m_Code );\n"
	     "\n"
	     "    m_SingleCharsToCodes.insert( pair<char,VariableCode>(\n"
	     "        *(singleCharArray[ i ].m_Tag), code ) );\n"
	     "    m_CodesToSingleChars.insert( pair<VariableCode,char>\n"
	     "        ( code, *(singleCharArray[ i ].m_Tag) ) );\n"
	     "  }\n"
	     "}\n", endCode.c_str() );

    fclose( output );
  } catch( QException& e ) {
    fprintf( stderr, "Caught exception - %s\n%s\n",
	     e.toString(), e.getStackTrace() );
    exit( 1 );
  }
}
