#ifndef SPECIALTOKEN_H
#define SPECIALTOKEN_H

/**
 * This represents a special token. It have two modes, and can be
 * compared with a stream directly in a map find method. Hopefully
 * this means it will be very fast to make an item in a stream to
 * it's code in the compress class.
 */

#include <string>

using namespace std;

class SpecialToken {
public:
  SpecialToken( const string&, bool isSingleChar = false );
  SpecialToken( const char *s, int length );
  SpecialToken( const SpecialToken& );
  SpecialToken();

  const char   *getToken() const { return m_Stream; }
  int           getSize()  const { return m_Size;   }
  void          advance( int );
  void          addToFrequency( unsigned long ) const;
  unsigned long getFrequency()     const { return m_Frequency; }
  unsigned long getRealFrequency() const;
  char          getCurrentChar()   const;
  bool          getIsSingleChar()  const { return m_IsSingleChar; }
  bool          isEndToken()       const { return m_IsEndToken; }

  bool          operator<( const SpecialToken& ) const;

protected:
  bool                   m_IsSingleChar;
  string                 m_Token;
  int                    m_Size;
  const char            *m_Stream;
  mutable bool           m_IncrementedFrequency;
  mutable unsigned long  m_Frequency;
  bool                   m_IsEndToken;
};

#endif
