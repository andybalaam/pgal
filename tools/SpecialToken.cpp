#include "SpecialToken.h"

#define LOOK_FOR 3

SpecialToken::SpecialToken( const string& s, bool isSingleChar ) : 
    m_IsSingleChar(         isSingleChar    ),
    m_Token(                s               ),
    m_Size(                 s.size()        ),
    m_Stream(               m_Token.c_str() ),
    m_IncrementedFrequency( false           ),
    m_Frequency(            1               ),
    m_IsEndToken(           false           ) { } 

SpecialToken::SpecialToken( const char *s, 
			    int         length ) : 
    m_IsSingleChar(         false  ),
    m_Size(                 length ),
    m_Stream(               s      ),
    m_IncrementedFrequency( false  ),
    m_Frequency(            1      ),
    m_IsEndToken(           false  ) { }

SpecialToken::SpecialToken() : 
    m_IsSingleChar(         false  ),
    m_Size(                 0      ),
    m_Stream(               NULL   ),
    m_IncrementedFrequency( false  ),
    m_Frequency(            1      ),
    m_IsEndToken(           true   ) { }

SpecialToken::SpecialToken( const SpecialToken& s ) : 
    m_IsSingleChar(         s.m_IsSingleChar         ),
    m_Token(                s.m_Token                ),
    m_Size(                 s.m_Size                 ),
    m_IncrementedFrequency( s.m_IncrementedFrequency ),
    m_Frequency(            s.m_Frequency            ),
    m_IsEndToken(           s.m_IsEndToken           ) {
  if ( m_Token.size() > 0 ) {
    m_Stream = &( ( m_Token.c_str() )[ m_Token.size() - m_Size ] );
  } else {
    m_Stream = s.m_Stream;
  }
}

void SpecialToken::advance( int n ) {
  m_Size   -= n;
  m_Stream += n;
}

bool SpecialToken::operator<( const SpecialToken& token ) const {
  /**
   * The rule is - if either ends, then they are deemed to have both
   * ended. From that point, we can say if either one is < than the
   * other.
   */

  if ( m_IsEndToken == true && token.m_IsEndToken == true ) {
    return false;
  } else if ( m_IsEndToken == true ) {
    return true;
  } else if ( token.m_IsEndToken == true ) {
    return false;
  }

  const char *s = m_Stream;
  const char *t = token.m_Stream;
  
  for ( ; *s == *t; s++, t++ ) {
    if ( *s == '\0' && *t == '\0' ) {
      /**
       * Both are the same! So, the operator fails.
       */
      return false;
    }
  }

  /**
   * The rule is - if both are tokens, then we go on as normal.
   * If one is a token and the other a stream, and the token's
   * ended, then we fail, as the token matches the stream.
   * If both are streams, we go as normal.
   */
  if ( s != m_Stream ) {
    if ( ( *s == '\0' && m_Token.size() != 0 && token.m_Token.size() == 0 ) ||
	 ( *t == '\0' && m_Token.size() == 0 && token.m_Token.size() != 0 ) ) {
      /**
       * The token's ended, the stream goes on. But, the token matches the
       * stream, so we fail.
       */
      return false;
    }
  }
  
  return *s < *t;
}

void SpecialToken::addToFrequency( unsigned long l ) const {
  if ( m_IncrementedFrequency == false ) {
    m_IncrementedFrequency = true;
    m_Frequency = l;
  } else {
    m_Frequency += l;
  }
}

char SpecialToken::getCurrentChar() const {
  if ( m_Stream != NULL ) {
    return *m_Stream;
  } else {
    return '*';
  }
}

unsigned long SpecialToken::getRealFrequency() const {
  if ( m_IncrementedFrequency ) {
    return m_Frequency;
  } else {
    return 0L;
  }
}
