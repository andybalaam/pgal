#include "HuffmanCompressor.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

HuffmanCompressor::HuffmanCompressor() { }

HuffmanCompressor::HuffmanCompressor( 
    const vector<SpecialToken>& occuranceTable ) {
  /**
   * We build an ordered table...
   */
  multimap<unsigned long,TreeNode*> orderedTable;
  vector<SpecialToken>::const_iterator i;
  for ( i = occuranceTable.begin(); i != occuranceTable.end(); i++ ) {
    orderedTable.insert( pair<unsigned long,TreeNode*>
			   ( i->getFrequency(), new TreeNode( *i ) ) );
  }

   /**
   * We also add our own special token, one that marks the end of the
   * binary image.
   */
  SpecialToken endToken;
  orderedTable.insert( pair<unsigned long,TreeNode*>
                       ( 1, new TreeNode( endToken ) ) );

  while ( orderedTable.size() > 1 ) {
    /**
     * We get the two lowest...
     */
    multimap<unsigned long,TreeNode*>::iterator j1, j2;
    j2 = orderedTable.begin();
    j1 = j2++;

    unsigned long newFrequency = j1->first + j2->first;
    
    /**
     * Now we create the node and find its proper place in our tree.
     */
    TreeNode *newNode = new TreeNode;
    newNode->m_Left   = j1->second;
    newNode->m_Right  = j2->second;

    orderedTable.erase( j1 );
    orderedTable.erase( j2 );

    orderedTable.insert( pair<unsigned long,TreeNode*>
			 ( newFrequency, newNode ) );
  }

  /**
   * Right - we have one item in our table and that contains the head
   * node!
   */
  TreeNode *head = orderedTable.begin()->second;

  /**
   * Now we need to actually create the codes themselves.
   */
  VariableCode code;
  createCodes( head, code );

  /**
   * Now we delete the tree
   */
  deleteNode( head );
}

void HuffmanCompressor::deleteNode( TreeNode *p ) {
  if ( p->m_Left != NULL ) {
    deleteNode( p->m_Left );
  }
  
  if ( p->m_Right != NULL ) {
    deleteNode( p->m_Right );
  }

  delete p;
}

void HuffmanCompressor::createCodes( TreeNode            *node, 
				     const VariableCode&  code ) {
  if ( node->m_Left == NULL && node->m_Right == NULL ) {
    if ( node->m_Token->getIsSingleChar() == true ) {
      m_SingleCharsToCodes.insert( pair<char,VariableCode>
				   ( *( node->m_Token->getToken() ), code ) );
      m_CodesToSingleChars.insert( pair<VariableCode,char>
				   ( code, *( node->m_Token->getToken() ) ) );
    } else {
      m_SymbolsToCodes.insert( pair<SpecialToken,VariableCode>
			       ( *( node->m_Token ), code ) );
      m_CodesToSymbols.insert( pair<VariableCode,SpecialToken>
			       ( code, *( node->m_Token ) ) );
    }
  } else {
    VariableCode newCode( code );
    newCode.addBitZero();
    createCodes( node->m_Left, newCode );

    VariableCode newCode2( code );
    newCode2.addBitOne();
    createCodes( node->m_Right, newCode2 );
  }
}

void HuffmanCompressor::compress( VariableCode&  buffer, 
				  const char    *s,
				  int            num ) {
  try {
    /**
     * We compress by matching tokens in our library and placing their
     * equivalents in the VariableCode instance.
     */
    SpecialToken stream( s, num );

    map<SpecialToken,VariableCode>::const_iterator i;
    map<char,VariableCode>::const_iterator j;
    while ( *s != '\0' ) {
      i = m_SymbolsToCodes.find( stream );
      if ( i == m_SymbolsToCodes.end() ) {
	j = m_SingleCharsToCodes.find( stream.getCurrentChar() );
	if ( j == m_SingleCharsToCodes.end() ) {
	  char buf[ 100 ];
	  strncpy( buf, s, strlen( s ) > 99 ? 99 : strlen( s ) );
	  string errMsg;
	  errMsg += "Cannot find token for string beginning '";
	  errMsg += buf;
	  errMsg += "'";
	  
	  UnknownSymbol e( errMsg.c_str() );
	  e.fillInStackTrace();
	  throw e;
	}

	buffer.addVariableCode( j->second );
	s += 1;
	stream.advance( 1 );
      } else {
	buffer.addVariableCode( i->second );
	
	s += i->first.getSize();
	stream.advance( i->first.getSize() );
      }
    }

    /**
     * We need to add an end token.
     */
    i = m_SymbolsToCodes.find( m_EndToken );
    buffer.addVariableCode( i->second );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

void 
HuffmanCompressor::decompress( string& buffer, VariableCode& code ) {
  try {
    int bitPos = 0;
    map<VariableCode,SpecialToken>::const_iterator i;
    map<VariableCode,char>::const_iterator j;
    bool finish = false;
    do {
      i = m_CodesToSymbols.find( code );
      if ( i == m_CodesToSymbols.end() ) {
	j = m_CodesToSingleChars.find( code );
	if ( j == m_CodesToSingleChars.end() ) {
	  char buf[ 100 ];
	  int size = buffer.size() > 99 ? 99 : buffer.size();
	  strncpy( buf, 
		   &( ( buffer.c_str() )[ buffer.size() - size ] ), 
		   size );
	  
	  string errMsg;
	  errMsg += "Cannot decode stream after '";
	  errMsg += buf;
	  errMsg += "'";
	  
	  UnknownCode e( errMsg.c_str() );
	  e.fillInStackTrace(); 
	  throw e;
	}
	
	buffer += j->second;
	bitPos += j->first.getBitSize();
	code.advanceBits( j->first.getBitSize() );
      } else if ( i->second.isEndToken() == false ) {
	buffer += i->second.getToken();
	bitPos += i->first.getBitSize();
	code.advanceBits( i->first.getBitSize() );
      } else if ( i->second.isEndToken() == true ) {
	finish = true;
      }
    } while ( finish == false );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}
