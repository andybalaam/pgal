#include <stdio.h>
#include "SpecialToken.h"
#include <pgal/VariableCode.h>
#include "HuffmanCompressor.h"
#include <string.h>
#include <map>

using namespace std;

static void testSpecialToken() {
  /**
   * We need to compare the tokens to see if they can be placed properly
   * and found again using a stream.
   */
  SpecialToken a( "dn" );
  SpecialToken b( "bcd" );
  SpecialToken c( "hello" );
  
  map<SpecialToken,int> allTokens;
  allTokens.insert( pair<SpecialToken,int>( a, 1 ) );
  allTokens.insert( pair<SpecialToken,int>( b, 2 ) );
  allTokens.insert( pair<SpecialToken,int>( c, 3 ) );

  const char *streamStr =
    "nuabcdnk";

  SpecialToken stream( streamStr, strlen( streamStr ) );
  stream.advance( 5 );

  printf( "Doing find...\n" );

  if ( allTokens.find( stream ) == allTokens.end() ) {
    printf( "Can't find\n" );
  } else {
    printf( "Found it!\n" );
  }
}

static void testVariableCode() {
  VariableCode v;
  v.addBitOne();
  v.addBitOne();

  string binBuf;
  v.printAsBinary( binBuf );
  printf( "%s\n", binBuf.c_str() );

  v.addBitZero();
  v.addBitZero();
  v.addBitOne();
  v.addBitOne();

  binBuf.clear();
  v.printAsBinary( binBuf );
  printf( "%s\n", binBuf.c_str() );

  printf( "Reading:\n" );

  const char *s = "01110";
  VariableCode v2;
  v2.readAsBinary( s );
  binBuf.clear();
  v2.printAsBinary( binBuf );
  printf( "%s\n", binBuf.c_str() );
  
  v2.addBitZero();
  v2.addBitOne();
  binBuf.clear();
  v2.printAsBinary( binBuf );
  printf( "%s\n", binBuf.c_str() );
}

static void testHuffmanCompressor() {
  const char *s = "hello there this should be compressed. hello there "
    "this zzzzreally should be compressed. this this this be this "
    "zzzzzzzzzzzzzthis";

  SpecialToken hello(      "hello"      ); hello.addToFrequency(      2 );
  SpecialToken there(      "there"      ); there.addToFrequency(      2 );
  SpecialToken thisT(      "this"       ); thisT.addToFrequency(      6 );
  SpecialToken should(     "should"     ); should.addToFrequency(     2 );
  SpecialToken be(         "be"         ); be.addToFrequency(         3 );
  SpecialToken compressed( "compressed" ); compressed.addToFrequency( 2 );
  SpecialToken really(     "really"     ); really.addToFrequency(     1 );
  SpecialToken space(      " "          ); space.addToFrequency(     17 );
  SpecialToken dot(        "."          ); dot.addToFrequency(        2 );
  SpecialToken z(  string("z"), true    ); z.addToFrequency(         17 );

  vector<SpecialToken> occuranceTable;
  occuranceTable.push_back( hello      );
  occuranceTable.push_back( there      );
  occuranceTable.push_back( thisT      );
  occuranceTable.push_back( should     );
  occuranceTable.push_back( be         );
  occuranceTable.push_back( compressed );
  occuranceTable.push_back( really     );
  occuranceTable.push_back( space      );
  occuranceTable.push_back( dot        );
  occuranceTable.push_back( z          );

  HuffmanCompressor compressor( occuranceTable );

  /**
   * Now let's print out all the items in the compressor...
   */
  printf( "Codes to symbols:\n" );

  string buffer;
  map<VariableCode,SpecialToken>::const_iterator i;
  for ( i = compressor.m_CodesToSymbols.begin();
	i != compressor.m_CodesToSymbols.end();
	i++ ) {
    buffer.clear();
    i->first.printAsBinary( buffer );
    printf( "%s\t%s\n", buffer.c_str(), i->second.getToken() );
  }

  map<VariableCode,char>::const_iterator j;
  for ( j = compressor.m_CodesToSingleChars.begin();
	j != compressor.m_CodesToSingleChars.end();
	j++ ) {
    buffer.clear();
    j->first.printAsBinary( buffer );
    printf( "%s\t%c\n", buffer.c_str(), j->second );
  }
}

static void testFindCode() {
  VariableCode a; a.readAsBinary( "110" );
  VariableCode b; b.readAsBinary( "1001" );
  VariableCode c; c.readAsBinary( "1000" );

  map<VariableCode,int> allCodes;
  allCodes.insert( pair<VariableCode,int>( a, 1 ) );
  allCodes.insert( pair<VariableCode,int>( b, 2 ) );
  allCodes.insert( pair<VariableCode,int>( c, 3 ) );

  { // DEBUG block
    fprintf( stderr, "size = %d\n",
	     allCodes.size() );
  }

  unsigned char s[] = "1001010110110010100111001000111110100101011011001111001010011100100011111011001100110011100110";

  VariableCode stream( s, 10, strlen( (char*)s ) * 8 - 1 );
  
  map<VariableCode,int>::const_iterator i = allCodes.find( stream );
  if ( i != allCodes.end() ) {
    printf( "Found it! (%d)\n", i->second );
  } else {
    printf( "Can't find it!\n" );
  }
}

static void testAddingVariableCodes() {
  const char *s = "0101010";
  VariableCode v1;

  v1.readAsBinary( s );
  string buffer;
  buffer.clear();
  v1.printAsBinary( buffer );
  printf( "v1 = %s\n", buffer.c_str() );

  const char *t = "1100";
  VariableCode v2;

  v2.readAsBinary( t );
  buffer.clear();
  v2.printAsBinary( buffer );
  printf( "v2 = %s\n", buffer.c_str() );

  v1.addVariableCode( v2 );
  buffer.clear();
  v1.printAsBinary( buffer );
  printf( "now v1 = %s\n", buffer.c_str() );
}

static void testCompression() {
  try {
    const char *s = 
      "hello there this should be compressed. hello zzzthere "
      "this really </bias>zzzzzzshouldzzzz be compressed.zzzz this this this be this";
    
    SpecialToken bias(       "</bias>"    ); bias.addToFrequency(       1 );
    SpecialToken hello(      "hello"      ); hello.addToFrequency(      2 );
    SpecialToken there(      "there"      ); there.addToFrequency(      2 );
    SpecialToken thisT(      "this"       ); thisT.addToFrequency(      6 );
    SpecialToken should(     "should"     ); should.addToFrequency(     2 );
    SpecialToken be(         "be"         ); be.addToFrequency(         3 );
    SpecialToken compressed( "compressed" ); compressed.addToFrequency( 2 );
    SpecialToken really(     "really"     ); really.addToFrequency(     1 );
    SpecialToken space(      " "          ); space.addToFrequency(     17 );
    SpecialToken dot(        "."          ); dot.addToFrequency(        2 );
    SpecialToken z(  string("z"), true    ); z.addToFrequency(         17 );

    vector<SpecialToken> occuranceTable;
    occuranceTable.push_back( bias       );
    occuranceTable.push_back( hello      );
    occuranceTable.push_back( there      );
    occuranceTable.push_back( thisT      );
    occuranceTable.push_back( should     );
    occuranceTable.push_back( be         );
    occuranceTable.push_back( compressed );
    occuranceTable.push_back( really     );
    occuranceTable.push_back( space      );
    occuranceTable.push_back( dot        );
    occuranceTable.push_back( z          );

    HuffmanCompressor compressor( occuranceTable );
    
    string buffer;
    map<VariableCode,SpecialToken>::const_iterator i;
    for ( i = compressor.m_CodesToSymbols.begin();
	  i != compressor.m_CodesToSymbols.end();
	  i++ ) {
      if ( i->second.isEndToken() == false ) {
	buffer.clear();
	i->first.printAsBinary( buffer );
	printf( "%s\t%s\n", buffer.c_str(), i->second.getToken() );
      }
    }
    
    map<VariableCode,char>::const_iterator j;
    for ( j = compressor.m_CodesToSingleChars.begin();
	  j != compressor.m_CodesToSingleChars.end();
	  j++ ) {
      buffer.clear();
      j->first.printAsBinary( buffer );
      printf( "%s\t%c\n", buffer.c_str(), j->second );
    }

    /**
     * Now let's compress...
     */
    VariableCode compressedStr;
    compressor.compress( compressedStr, s, strlen( s ) );
    
    /**
     * ...and decompress
     */
    int noOfBits;
    const unsigned char *t = compressedStr.getRawBits( &noOfBits );

    { // DEBUG block
      string h;
      compressedStr.printAsBinary( h );
      fprintf( stderr, "Token string = %s\n",
	       h.c_str() );
    }
    
    VariableCode toDecompress( t, 0, noOfBits - 1 );

    buffer.clear();
    compressor.decompress( buffer, toDecompress );
    
    printf( "After compression and decompression: (%d-%d=%f)\n%s\n",
	    buffer.size()*8, noOfBits, 
	    (float)noOfBits / ( (float)buffer.size()*8.0f ), 
	    buffer.c_str() );
  } catch( QException& e ) {
    fprintf( stderr, "Caught exception: %s\n%s\n",
	     e.toString(), e.getStackTrace() );
    exit( 1 );
  }
}
  
int main( int argc, char **argv ) {
  int code = 0;
  if ( argv[ 1 ] != NULL ) {
    code = atoi( argv[ 1 ] );
  }

  switch( code ) {
  case 0:
    testSpecialToken();
    break;

  case 1:
    testVariableCode();
    break;

  case 2:
    testHuffmanCompressor();
    break;
    
  case 3:
    testFindCode();
    break;

  case 4:
    testAddingVariableCodes();
    break;

  case 5:
    testCompression();
    break;

  default:
    fprintf( stderr, "Unknown test!\n" );
    exit( 1 );
    break;
  }

  return 0;
}
