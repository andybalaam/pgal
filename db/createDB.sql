CREATE TABLE genotype (
  ID                    INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
  evolutionaryPartRunID INT UNSIGNED NOT NULL,
  genotypeValue         MEDIUMBLOB,
  parentID              INT UNSIGNED,
  sizeInBits            INT UNSIGNED NOT NULL,
  firstGenerationNo     INT UNSIGNED,
  lastGenerationNo      INT UNSIGNED,
  typeID                INT UNSIGNED NOT NULL,
  INDEX( evolutionaryPartRunID ),
  INDEX( firstGenerationNo ),
  INDEX( lastGenerationNo ),
  INDEX( parentID )
);

CREATE TABLE trial (
  ID                    INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
  generationID          INT UNSIGNED NOT NULL,
  flags                 TINYINT UNSIGNED NOT NULL,
  INDEX( generationID )
);

CREATE TABLE trialGenotype (
  ID                    INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
  generationID          INT UNSIGNED NOT NULL,
  trialID               INT UNSIGNED NOT NULL,
  genotypeID            INT UNSIGNED NOT NULL,
  fitness               FLOAT(4) NOT NULL,
  flags                 TINYINT UNSIGNED NOT NULL,
  INDEX( genotypeID ),
  INDEX( trialID ),
  INDEX( generationID )
);

CREATE TABLE generation (
  ID                    INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
  generationNo          INT UNSIGNED,
  evolutionaryPartRunID INT UNSIGNED,
  timeStamp             TIMESTAMP,
  quartile1             FLOAT(4),
  quartile2             FLOAT(4),
  quartile3             FLOAT(4),
  quartile4             FLOAT(4),
  quartile5             FLOAT(4),
  quartile6             FLOAT(4),
  quartile7             FLOAT(4),
  quartile8             FLOAT(4),
  quartile9             FLOAT(4),
  quartile10            FLOAT(4),
  INDEX( evolutionaryPartRunID ),
  INDEX( generationNo )
);

CREATE TABLE generationFitnesses (
  ID                    INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
  generationID          INT UNSIGNED NOT NULL,
  fitnessNo             INT UNSIGNED NOT NULL,
  quartile1             FLOAT(4),
  quartile2             FLOAT(4),
  quartile3             FLOAT(4),
  quartile4             FLOAT(4),
  quartile5             FLOAT(4),
  quartile6             FLOAT(4),
  quartile7             FLOAT(4),
  quartile8             FLOAT(4),
  quartile9             FLOAT(4),
  quartile10            FLOAT(4),
  INDEX( generationID )
);

CREATE TABLE evolutionaryRun (
  ID                    INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
  initialPopulationSize INT UNSIGNED,
  programVersion        TEXT
);

CREATE TABLE evolutionaryPartRun (
  ID                    INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
  evolutionaryRunID     INT UNSIGNED,
  endTime               TIMESTAMP,
  startTime             TIMESTAMP,
  notes                 TEXT,
  INDEX( evolutionaryRunID )
);

CREATE TABLE timePeriodInfo (
  ID                    INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
  evolutionaryPartRunID INT UNSIGNED,
  timePeriodSeconds     INT UNSIGNED,
  noOfGenerations       INT UNSIGNED,
  realTime              TIMESTAMP,
  INDEX( evolutionaryPartRunID )
);

ALTER TABLE genotype    MAX_ROWS=1000000000 AVG_ROW_LENGTH=5000;
ALTER TABLE trial       MAX_ROWS=1000000000;
ALTER TABLE generation  MAX_ROWS=1000000000;
