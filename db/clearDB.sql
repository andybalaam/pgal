DROP TABLE genotype;
DROP TABLE trial;
DROP TABLE generation;
DROP TABLE generationFitnesses;
DROP TABLE evolutionaryRun;
DROP TABLE evolutionaryPartRun;
DROP TABLE timePeriodInfo;
DROP TABLE trialGenotype;

