if [ -e $HOME/.pgal/dbrc.sh ]; then
	source $HOME/.pgal/dbrc.sh
else
	echo "No database configuration file ~/.pgal/dbrc.sh! \n Run pgal_db_setup first."
	exit
fi
