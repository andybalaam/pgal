# areYouSure.sh

# Asks the user whether they're sure (defaults to yes)

echo -n "Are you sure? [Y] "
read SURE

# Make this better
if [ -z $SURE ]; then SURE=Y; fi

if [[ $SURE != [Yy]* ]]; then
	echo "Aborted."
	exit
fi
