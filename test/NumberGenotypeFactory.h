/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef NUNBERGENOTYPEFACTORY_H
#define NUMBERGENOTYPEFACTORY_H

#include <pgal/AbstractGenotypeFactory.h>

class NumberGenotypeFactory : public AbstractGenotypeFactory {
public:
  AbstractGenotype *createNewRandomGenotype();
  AbstractGenotype *createNewMutatedGenotype( const AbstractGenotype * );
  AbstractGenotype *createNewCrossedGenotype( const AbstractGenotype *,
					      const AbstractGenotype * );
  AbstractGenotype *createGenotypeFromBits( const unsigned char *bytes,
					    int                  noOfBits );
};

#endif
