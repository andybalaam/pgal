/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "NumberTrialFactory.h"
#include "NumberGenotypeFactory.h"
#include <pgal/RankSelectionDescriptor.h>
#include <pgal/RandomNumberGenerator.h>
#include <pgal/EvolutionaryRun.h>
#include <pgal/ParetoFrontFitnessComparator.h>
#include <stdlib.h>
#include <time.h>

#define POPULATION_SIZE 30

int main( int argc, char **argv ) {
  try {
    NumberGenotypeFactory      genotypeFactory;
    NumberTrialFactory         trialFactory;

    RankSelectionDescriptor    generationDescriptor( POPULATION_SIZE );
    
    ParetoFrontFitnessComparator p( 2, 2, 0 );
    generationDescriptor.setFitnessComparator( &p );

    //generationDescriptor.setNoOfTrialsPerGenotype( 4 );

    EvolutionaryRun *evolutionaryRun;
    
    if ( argc == 1 ) {
      evolutionaryRun = new EvolutionaryRun( POPULATION_SIZE,
					     &genotypeFactory,
					     &generationDescriptor,
					     &trialFactory,
					     2,
					     "pgalUser",
					     "pgal",
					     "localhost",
					     "pgalDB",
                                             100 );
    } else {
      evolutionaryRun = new EvolutionaryRun( 1,
					     POPULATION_SIZE,
					     &genotypeFactory,
					     &generationDescriptor,
					     &trialFactory,
					     2,
					     "pgalUser",
					     "pgal",
					     "localhost",
					     "pgalDB",
                                             100 );
    }

    evolutionaryRun->setWriteGenotypeBodyEvery( 100 );
    
    evolutionaryRun->start( "Hello" );

    delete evolutionaryRun;
  } catch( QException& e ) {
    fprintf( stderr, "Caught exception - %s\n%s\n",
	     e.getMessage(),
	     e.getStackTrace() );
    exit( 1 );
  }

  return 0;
}
