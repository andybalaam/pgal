/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "NumberGenotypeFactory.h"
#include "NumberGenotype.h"

AbstractGenotype *NumberGenotypeFactory::createNewRandomGenotype() {
  return new NumberGenotype;
}

AbstractGenotype *NumberGenotypeFactory::createNewMutatedGenotype( 
   const AbstractGenotype *g1 ) {
  const NumberGenotype *genotype = dynamic_cast<const NumberGenotype *>( g1 );

  NumberGenotype *toReturn = new NumberGenotype( *genotype );

  toReturn->mutate();

  return toReturn;
}

AbstractGenotype *NumberGenotypeFactory::createNewCrossedGenotype( 
    const AbstractGenotype *,
    const AbstractGenotype * ) {
  return NULL;
}

AbstractGenotype *NumberGenotypeFactory::createGenotypeFromBits( 
    const unsigned char *bytes,
    int                  noOfBits ) {
  return new NumberGenotype( bytes, noOfBits );
}

