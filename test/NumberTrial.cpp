/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "NumberTrial.h"
#include <pgal/PopulationMember.h>
#include <vector>

#define NO_OF_SUBTRIALS 4

using namespace std;

NumberTrial::NumberTrial( NumberGenotype *g ) : m_Genotype( g ) { }

void NumberTrial::performTrial( TrialInfo& trialInfo ) {
  try {
    vector<FLOAT> subTrials;
    int i;
    FLOAT average = 0;
    for ( i = 0; i < NO_OF_SUBTRIALS / 2; i++ ) {
      FLOAT fitness = m_Genotype->calculateFitness();
      average += fitness;
      subTrials.push_back( fitness );
    }
    
    for ( i = NO_OF_SUBTRIALS / 2; i < NO_OF_SUBTRIALS; i++ ) {
      FLOAT fitness = m_Genotype->calculateFitness() * 10;
      average += fitness;
      subTrials.push_back( fitness );
    }
    
    average /= NO_OF_SUBTRIALS;
    
    trialInfo.setGenotypeInfo( m_Genotype->getIndexNo(),
			       subTrials,
			       average,
			       PopulationMember::KNOWN_FITNESS,
			       0 );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

