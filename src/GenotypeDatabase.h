/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef GENOTYPEDATABASE_H
#define GENOTYPEDATABASE_H

/**
 * This class allows access to the genotype database.  It is in the process
 * of being migrated towards using PDatabase instead of home-grown database
 * access code, and towarsd using SmallString instead of stl string.
 */
#include <mysql/mysql.h>
#include "AbstractGenotype.h"
#include "AbstractGenotypeFactory.h"
#include "PDatabase.h"
#include "QException.h"
#include <map>
#include <limits.h>
#include <vector>
#include "TrialInfo.h"
#include "PopulationMember.h"
#include "MemFile.h"

using namespace std;

class GenotypeDatabase : public PDatabase {
public:
  QEXCEPTIONCLASS( GenotypeDatabaseException );
  QEXCEPTIONDERIVEDCLASS( InvalidEvolutionaryPartRunID, 
			  GenotypeDatabaseException );

  GenotypeDatabase( const char *username,
		    const char *password,
		    const char *hostname,
		    const char *databaseName );
  virtual ~GenotypeDatabase() { }
  
  /**
   * @throw PDatabase::PDatabaseException
   */
  unsigned long 
    createEvolutionaryRunRecord( unsigned long  initialPopulationSize,
				 const char    *programVersion );

  /**
   * @throw PDatabase::PDatabaseException
   */
  unsigned long 
    createEvolutionaryPartRunRecord( unsigned long  evolutionaryRun,
				     const char    *notes );
  
  unsigned long
    createTimePeriodInfoRecord( unsigned long evolutionaryPartRunID,
				unsigned long timePeriodSeconds,
				unsigned long noOfGenerations );

  /**
   * @throw PDatabase::PDatabaseException
   */
  unsigned long
    createTrialRecord( const TrialInfo&,
		       unsigned long generationID );

  /**
   * @throw PDatabase::PDatabaseException
   */
  unsigned long
    createGenerationRecord( unsigned long generationID,
			    unsigned long evolutionaryPartRun );

  /**
   * @throw PDatabase::PDatabaseException
   */
  void addQuartileData( unsigned long  generationID,
		        const FLOAT   *quartiles );

  /**
   * @throw PDatabase::PDatabaseException
   */
  void addExtraQuartileData( unsigned long  generationID,
                const unsigned int fitnessNo,
		        const FLOAT       *quartiles );
                
  unsigned long loadLatestGeneration( 
      unsigned long                         evolutionaryRun,
      AbstractGenotypeFactory              *genotypeFactory,
      unsigned long                        *newGenerationNo,
      map<unsigned long, PopulationMember> *population 
  );
  
  /**
   * Loads or lists the population at the given generation.
   *
   * @param generationNo      the generation (unsigned long) we are concerned
   *                          with or -1 for the very latest population
   * @param evolutionaryRunID the ID of the evolutionary run we are concerned
   *                          with
   * @param genotypeFactory   a factory to create the genotypes with, or NULL
   *                          if you just want to list them, not load them
   * @param population        a pointer to a map of IDs against pointers to
   *                          the loaded genotypes of each population member.
   *                          Make this NULL if you just want to list the IDs,
   *                          not load them
   * @param fitnesses         a pointer to a map of IDs against the latest 
   *                          fitness found for this genotype, or NULL if you
   *                          are not intersted in fitnesses
   * @param states            a pointer to a map of IDs against the fitness
   *                          flags: KNOWN, UNKNOWN or INVALID GENOTYPE or NULL
   *                          if you are not interested in fitnesses
   * @return                  the population size at this generation
   *
   * @throw PDatabase::PDatabaseException
   */
  unsigned long loadPopulationAtGeneration( 
      long                                  generationNo, 
      unsigned long                         evolutionaryRunID,
      AbstractGenotypeFactory              *genotypeFactory,
      unsigned long                        *newGenerationNo,
      map<unsigned long, PopulationMember> *population
  );
  
  unsigned long loadPopulationAtGeneration( 
      long                         generationNo,
      unsigned long                evolutionaryRunID,
      map<unsigned long, MemFile> *genotypeValues,
      map<unsigned long, int>     *sizeInBits,
      map<unsigned long, int>     *typeNos
  );

  /*
   * @throw PDatabase::PDatabaseException
   */
  unsigned long findHowManyGenerations( unsigned long evolutionaryRunID );
  
  unsigned long findPopulationSize( unsigned long evolutionaryPartRunID );

  /**
   * @throw PDatabase::PDatabaseException
   */
  unsigned long *findAllEvolutionaryRuns( unsigned long 	*numberOfRuns,
					  SmallString          **startDates,
					  SmallString          **endDates,
					  SmallString          **notes);
  /*
   * @throw PDatabase::PDatabaseException
   */
  unsigned long * 
    findAllEvolutionaryPartRuns( unsigned long   evolutionaryRunID,
				 unsigned long  *numberOfParts,
				 unsigned long  *populationSize,
				 SmallString   **startDates,
				 SmallString   **endDates,
				 SmallString   **notes);
  /**
   * The returned array here must also be deleted manually.
   * @throw PDatabase::PDatabaseException
   */
  unsigned char *loadGenotype( unsigned long    genotypeID,
			       unsigned long   *sizeInBits,
			       unsigned long   *typeID );

  /**
   * @throw PDatabase::PDatabaseException
   */
  void finishEvolutionaryPartRun( unsigned long evolutionaryPartRunID );
 
  void setLastGenerationForGenotype( unsigned long genotypeNo,
				     unsigned long lastGeneration );

  /**
   * @throw PDatabase::PDatabaseException
   */
  unsigned long addGenotype( const AbstractGenotype *genotype,
			     unsigned long           evolutionaryPartRun,
                             unsigned long           generationNo,
			     unsigned long           parent = ULONG_MAX,
			     bool                    addBody = true );

  void findLatestGenotypesFromEvolutionaryPartRun( 
    map<unsigned long,vector<FLOAT> >& IDToFitness,
    map<unsigned long,bool>&           IDToHasBodyFlag,
    map<unsigned long,unsigned long>&  IDToGenerationNo, 
    map<unsigned long,unsigned long>&  IDToType,
    map<unsigned long,MemFile>&        IDToGenotypeBodies,
    int                                evolutionaryPartRunID, 
    int                                numberInList,
    bool                               bodyNotNull,
    bool                               loadGenotypeBodies );
};

#endif
