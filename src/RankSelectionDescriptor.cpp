/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "RankSelectionDescriptor.h"
#include "EvolutionaryRun.h"
#include "QErrorLog.h"

AverageFitnessComparator RankSelectionDescriptor::m_DefaultFitnessComparator;

RankSelectionDescriptor::RankSelectionDescriptor( int populationSize ) :
    m_PopulationSize( populationSize ),
    m_RandomNumbers( RandomNumberGenerator::getDefaultGoodGenerator() ),
    m_AllFitnesses( NULL ),
    m_NoOfElite( 0 ),
    m_NoOfTrialsPerGenotype( 1 ) {
  if ( m_PopulationSize <= 2 ) {
    SmallString msg;
    msg += "Invalid population size of ";
    msg += m_PopulationSize;
    msg += " - it must be larger than 2";

    RankSelectionDescriptorException e( msg );
    e.fillInStackTrace();
    throw e;
  }

  m_AllFitnesses = new S_fitnesses *[ m_PopulationSize ];
  
  int i;
  for ( i = 0; i < m_PopulationSize; i++ ) {
    m_AllFitnesses[ i ] = new S_fitnesses;
  }

  setFitnessComparator( &m_DefaultFitnessComparator );
}

RankSelectionDescriptor::~RankSelectionDescriptor() {
  if ( m_AllFitnesses != NULL ) {
    int i;
    for ( i = 0; i < m_PopulationSize;i++ ) {
      delete m_AllFitnesses[ i ];
    }

    delete[] m_AllFitnesses;
  }
}

void RankSelectionDescriptor::getGenotypesToSendToTrial(
    queue<TrialInfo>&                          toSendToTrial,
    const map<unsigned long,PopulationMember>& population ) {
  map<unsigned long,PopulationMember>::const_iterator i;

  if ( population.size() != m_PopulationSize ) {
    SmallString errMsg;
    errMsg += "Found unexpected population size of ";
    errMsg += population.size();
    errMsg += " instead of ";
    errMsg += m_PopulationSize;
 
    RankSelectionDescriptorException e( errMsg );
    e.fillInStackTrace();
    throw e;
  }

  /**
   * We'll pick the entire lot...
   */
  int j;
  for ( i = population.begin(); i != population.end(); i++ ) {
    TrialInfo trialInfo;
    trialInfo.addGenotype( i->first );
    
    for ( j = 0; j < m_NoOfTrialsPerGenotype; j++ ) {
      toSendToTrial.push( trialInfo );
    }
  }

  /*{ // DEBUG block
    SmallString msg;
    msg += "Sending these to trial:";
    
    vector<TrialInfo>::iterator j;
    for ( j = toSendToTrial.begin(); j != toSendToTrial.end(); j++ ) {
      j->toString( msg );
      msg += "\n";
    }

    QErrorLog::writeError( msg );
    }*/
}

void RankSelectionDescriptor::getNextGenerationInformation( 
    set<unsigned long>&                  toBeRemoved,
    queue<unsigned long>&                toBeReproduced,
    map<unsigned long,PopulationMember>& population,
    const vector<TrialInfo>&             trials ) {
  /**
   * We'll order them, and the chances of each genotype 
   * reproducing is proportional to it's order in the 
   * rank.
   */

  /**
   * Set our structure to unset...
   */
  int j;
  for ( j = 0; j < m_PopulationSize; j++ ) {
    m_AllFitnesses[ j ]->state = PopulationMember::UNKNOWN_FITNESS;
    m_AllFitnesses[ j ]->fitnesses.clear();
    m_AllFitnesses[ j ]->alreadySet = false;
  }

  /**
   * We have to quickly copy all the genotype details to
   * our area.
   */
  vector<TrialInfo>::const_iterator i;
  unsigned long                     genotypeID;
  vector<FLOAT>                     fitnesses;
  FLOAT                             databaseFitness;
  unsigned char                     state;
  int                               typeNo;
  map<unsigned long,int>            position;
  for ( j = 0, i = trials.begin(); 
	i != trials.end(); 
	j++, i++ ) {
    i->getGenotypeInfo( 0,
			&genotypeID,
			fitnesses,
			&databaseFitness,
			&state,
			&typeNo );
    /**
     * Have we already put this in once before?
     */
    int index = j;
    map<unsigned long,int>::iterator before;
    before = position.find( genotypeID );
    if ( before != position.end() ) {
      index = before->second;
      j--;
    } else {
      position.insert( pair<unsigned long,int>( genotypeID, j ) );
    }

    if ( m_AllFitnesses[ index ]->state != 
	 PopulationMember::INVALID_GENOTYPE ) {
      i->getGenotypeInfo( 0,
			  &genotypeID,
			  fitnesses,
			  &databaseFitness,
			  &state,
			  &typeNo );
      
      m_AllFitnesses[ index ]->genotypeNo = genotypeID;
      m_AllFitnesses[ index ]->fitnesses.push_back( fitnesses );
      m_AllFitnesses[ index ]->state = state;
      m_AllFitnesses[ index ]->alreadySet = true;

      m_AllFitnesses[ index ]->databaseFitness = 
	m_AllFitnesses[ index ]->fitnessComparator->calculateFitness( 
	  m_AllFitnesses[ index ]->fitnesses );
    }
  }

  /**
   * Now lets check to make sure we have at least one fitness for 
   * each genotype
   */
  for ( j = 0; j < m_PopulationSize; j++ ) {
    if ( m_AllFitnesses[ j ]->alreadySet == false ) {
      RankSelectionDescriptorException e( "Haven't got the fitnesses for all "
					  "genotypes" );
      e.fillInStackTrace();
      throw e;
    }
  }

  qsort( m_AllFitnesses,
	 m_PopulationSize,
	 sizeof( S_fitnesses * ),
	 sortFitnessStructures );
  /**
   * We'll quickly sort out the fitnesses for stats...
   */
  for ( j = 0; j < m_PopulationSize; j++ ) {
    map<unsigned long,PopulationMember>::iterator x;
    x = population.find( m_AllFitnesses[ j ]->genotypeNo );
    if ( x == population.end() ) {
      char errMsg[ 1024 ];
      sprintf( errMsg, "Programming error - trying to find the population "
	       "member of %lu but I can't find it",
	       m_AllFitnesses[ j ]->genotypeNo );
      RankSelectionDescriptorException e( errMsg );
      e.fillInStackTrace();
      throw e;
    }

    x->second.setFitness( m_AllFitnesses[ j ]->databaseFitness );
    x->second.setFitnesses( m_AllFitnesses[ j ]->fitnesses[0] );
    x->second.setState( m_AllFitnesses[ j ]->state );
  }

  /**
   * Now we have to reproduce populationSize genotypes
   * with the probability proportional to their place
   * in the rank.
   */
  
  FLOAT currentProbability = 0.0;
  for ( j = 0; j < m_PopulationSize; j++ ) {
    m_AllFitnesses[ j ]->lowerProbability = currentProbability;
    currentProbability += j + 1;
    m_AllFitnesses[ j ]->higherProbability = currentProbability;
  }

  /*{ // DEBUG block
    SmallString msg;
    msg += "Here is the order/fitnesses:";
    for ( j = 0; j < m_PopulationSize; j++ ) {
      msg += "\n";
      msg += j;
      msg += ":\t";
      msg += m_AllFitnesses[ j ]->genotypeNo;
      msg += ":\tfit=";
      vector<vector<FLOAT> >::const_iterator y;
      for ( y = m_AllFitnesses[ j ]->fitnesses.begin();
	    y != m_AllFitnesses[ j ]->fitnesses.end();
	    y++ ) {
	msg += "(";
	vector<FLOAT>::const_iterator d;
	for ( d = y->begin(); d != y->end(); d++ ) {
	  msg += *d;
	  msg += " ";
	}
	msg += ") ";
      }

      msg += ":\t";
      switch( m_AllFitnesses[ j ]->state ) {
      case 0x00:
	msg += "KNOWN_FITNESS   ";
	break;

      case 0x01:
	msg += "UNKNOWN_FITNESS ";
        break;

      case 0x02:
	msg += "INVALID_GENOTYPE";
	break;

      default:
	msg += "Error!          ";
      }

      msg += ":\t";
      msg += m_AllFitnesses[ j ]->lowerProbability;
      msg += ":\t";
      msg += m_AllFitnesses[ j ]->higherProbability;
    }
    
    QErrorLog::writeError( msg );
    }*/

  while ( toBeReproduced.size() < m_PopulationSize - m_NoOfElite ) {
    int genotypeToReproduce = -1;

    while ( genotypeToReproduce == -1 ) {
      double randomNo = m_RandomNumbers->randomDouble() * currentProbability;

      /**
       * Where does this lie in our ranking?
       */
      for ( j = 0; j < m_PopulationSize && genotypeToReproduce == -1; j++ ) {
	if ( randomNo >= m_AllFitnesses[ j ]->lowerProbability &&
	     randomNo <  m_AllFitnesses[ j ]->higherProbability ) {
	  genotypeToReproduce = m_AllFitnesses[ j ]->genotypeNo;
	  break;
	}
      }

      if ( genotypeToReproduce == -1 ) {
	SmallString errMsg;
	errMsg += "Cannot find a genotype for the random number ";
	errMsg += randomNo;
	
	RankSelectionDescriptorException e( errMsg );
	e.fillInStackTrace();
	throw e;
      }
      
      /**
       * Is it valid?
       */
      if ( m_AllFitnesses[ j ]->state == 
	   PopulationMember::INVALID_GENOTYPE ) {
	genotypeToReproduce = -1;
      } else {
	toBeReproduced.push( genotypeToReproduce );
      } 
    }
  }

  /**
   * Remove all existing genotypes except the elite
   */
  for ( j = 0; j < m_PopulationSize - m_NoOfElite; j++ ) {
    toBeRemoved.insert( m_AllFitnesses[ j ]->genotypeNo );
  }

  /**
   * Now we remove all the fitnesses, ready for next time.
   */
  for ( j = 0; j < m_PopulationSize; j++ ) {
    m_AllFitnesses[ j ]->fitnesses.clear();
  }

/*{ // DEBUG block
    map<unsigned long,int> tempFitnesses;
    for ( j = 0; j < m_PopulationSize; j++ ) {
      tempFitnesses.insert( 
        pair<unsigned long,int>( m_AllFitnesses[ j ].genotypeNo, j ) );
    }

    SmallString msg;
    msg += "Reproducing these:";
    vector<unsigned long>::iterator z;
    for ( z = toBeReproduced.begin(); z != toBeReproduced.end(); z++ ) {
      msg += "\n";
      msg += *z;
      msg += ":\t";

      map<unsigned long,int>::iterator v = tempFitnesses.find( *z );
      msg += m_AllFitnesses[ tempFitnesses[ v->second ] ].fitness;
    }
  
    msg += "\nTo be removed:";
    set<unsigned long>::iterator y;
    for ( y = toBeRemoved.begin(); y != toBeRemoved.end(); y++ ) {
      msg += " ";
      msg += *y;
    }

    msg += "\n";
    QErrorLog::writeError( msg );
    }*/
}

int RankSelectionDescriptor::sortFitnessStructures(
    const void *a1, 
    const void *a2 ) {
  S_fitnesses *s1, *s2;
  
  s1 = *(S_fitnesses **)a1;
  s2 = *(S_fitnesses **)a2;

  if ( s1->state == PopulationMember::INVALID_GENOTYPE ) {
    return -1;
  } else if ( s2->state == PopulationMember::INVALID_GENOTYPE ) {
    return 1;
  } else {
    return s1->fitnessComparator->compareFitness( s1->fitnesses, 
						  s2->fitnesses );
  }
}

bool RankSelectionDescriptor::setNoOfElite( int noOfElite ) {
  if ( noOfElite < 0 ) {
    return false;
  } 

  m_NoOfElite = noOfElite;
  
  return true;
}

bool RankSelectionDescriptor::setNoOfTrialsPerGenotype( int i ) {
  if ( i < 1 ) {
    return false;
  }

  m_NoOfTrialsPerGenotype = i;

  return true;
}

void 
RankSelectionDescriptor::setFitnessComparator(AbstractFitnessComparator *c) {
  /**
   * OK - this is a bit kludgy, but sortFitnessStructures is a static
   * function but we need to pass some class members to it, so we hide
   * it within the S_fitness structure.
   */
  int i;
  for ( i = 0; i < m_PopulationSize; i++ ) {
    m_AllFitnesses[ i ]->fitnessComparator = c;
  }
}
