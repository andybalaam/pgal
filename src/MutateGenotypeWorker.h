#ifndef MUTATEGENOTYPEWORKER_H
#define MUTATEGENOTYPEWORKER_H

/**
 * This worker class mutates a genotype.
 */

#include "PWorker.h"
#include "AbstractGenotype.h"
#include "AbstractGenotypeFactory.h"
#include "MemoryPool.h"

class MutateGenotypeWorker : public PWorker {
public:
  MutateGenotypeWorker( AbstractGenotypeFactory *factory,
			const AbstractGenotype  *genotype ) :
    m_Factory(     factory  ),
    m_Genotype(    genotype ),
    m_NewGenotype( NULL     ) { }

  void start();

  /*static void *operator new( size_t s ) {
    return (void *)s_Pool.allocateMemory();
  }

  static void operator delete( void *p, size_t s = 0 ) {
    s_Pool.freeMemory( (unsigned char *)p );
  }*/

  AbstractGenotype       *m_NewGenotype;
  const AbstractGenotype *m_Genotype;

protected:
  AbstractGenotypeFactory *m_Factory;

  static MemoryPool s_Pool;
};

#endif
