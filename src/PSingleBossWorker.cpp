#include "PSingleBossWorker.h"
#include "PWorker.h"
#include "QException.h"

void PSingleBossWorker::run( PWorker *worker ) {
  try {
    worker->start();
    m_FinishedWorkers.push( worker );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

PWorker *PSingleBossWorker::waitForProcessToFinish( bool ) {
  if ( m_FinishedWorkers.empty() == true ) {
    return false;
  }

  PWorker *toReturn = m_FinishedWorkers.front();
  m_FinishedWorkers.pop();

  return toReturn;
}

