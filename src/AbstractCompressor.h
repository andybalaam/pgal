#ifndef ABSTRACTCOMPRESSOR_H
#define ABSTRACTCOMPRESSOR_H

/**
 * This class can be used to compress genotypes before they are sent
 * over the network. The use of this is optional.
 */

#include "VariableCode.h"

class AbstractCompressor {
public:
  virtual void compress( VariableCode& buffer, const char *, int num ) = 0;
  virtual void decompress( string& buffer, VariableCode& )             = 0;
};

#endif
