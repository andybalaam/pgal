#ifndef PSIMPLETHREADSBOSSWORKER_H
#define PSIMPLETHREADSBOSSWORKER_H

/**
 * This is a simple pthreads implementation of the Boss/Worker
 * design pattern.
 */

#include <pthread.h>
#include "PBossWorker.h"
#include "QException.h"
#include <queue>

using namespace std;

class PSimpleThreadsBossWorker : public PBossWorker {
public:
  QEXCEPTIONCLASS( NoFreeProcesses );

  PSimpleThreadsBossWorker( int noOfThreads );
  virtual ~PSimpleThreadsBossWorker();

  /**
   * From PBossWorker
   */
  void     run(                   PWorker *          );
  PWorker *waitForProcessToFinish( bool    forceWait );
  int      getNoOfActiveProcesses() const;
  int      getNoOfFreeProcesses()   const;
  int      getNoOfFinishedWorkers() const;
  int      getNoOfThreads()         const { return m_NoOfThreads; }

protected:
  int                       m_NoOfThreads;
  int                       m_NoOfActiveProcesses;
  int                       m_NoOfFreeProcesses;
  pthread_t                *m_ArrayOfThreads;
  queue<PWorker *>          m_FinishedWorkers;
  
  mutable pthread_mutex_t   m_FinishedWorkersMutex;
  mutable pthread_mutex_t   m_GroupMutex;
  pthread_cond_t            m_GroupCond;
  pthread_cond_t            m_AvailableCond;
  pthread_cond_t            m_FinishedWorkAvailableCond;
  queue<PWorker *>          m_NextWorkers;
  bool                      m_ExitFlag;

  static void *staticThreadStart( void * );
  void         threadStart();
};

#endif
