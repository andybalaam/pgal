#ifndef VARIABLECODE_H
#define VARIABLECODE_H

/**
 * This class represents a binary code that can be of large length.
 * You can perform simple operations on it, such as adding a
 * number or shifting it left. You can also read or write in
 * in ascii format.
 */

#include <string>
#include <stdio.h>

using namespace std;

class VariableCode {
public:
  VariableCode();
  VariableCode( const VariableCode& );
  VariableCode( const unsigned char *s, 
		long                 startPointBits, 
		long                 endPointBits );
  virtual ~VariableCode();

  void        printAsBinary( string& ) const;
  void        readAsBinary(  const char * );
  void        advanceBits( unsigned long );
  void        addBitZero();
  void        addBitOne();
  void        addVariableCode( const VariableCode& );
  void        removeBit();

  const unsigned char *getRawBits( int *bitSize ) const;
  unsigned int         getBitSize() const;

  bool operator<( const VariableCode& ) const;

protected:
  struct S_RealStruct {
    unsigned int   m_NoOfPointers;
    unsigned int   m_BitSize;
    unsigned int   m_CurrentSize;
    unsigned char *m_Array;
  };
  
  mutable struct S_RealStruct *m_Instance;
  
  const unsigned char *m_Stream;
  long                 m_StartPointBits;
  long                 m_EndPointBits;

  void copyInstance();
  void clearInstance();
};

#endif
