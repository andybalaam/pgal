/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef BITFIELD_H
#define BITFIELD_H

/**
 * This defines some useful macros for use in
 * accessing bits in bitfields.
 */

#define CALCULATE_BIT(A)     (1<<(A))
#define GETBIT(A,B)          (((A)[(B)/8]>>((B)%8))&1)
#define BITON(A,B)           (((A)[(B)/8])|=(1<<((B)%8)))
#define BITOFF(A,B)          (((A)[(B)/8])&=(~(1<<((B)%8))))
#define BYTES_IN_BITFIELD(A) (((A)/8)+(((A)%8==0)?0:1))

#endif
