#include "RunTrialWorker.h"
#include "PopulationMember.h"
#include "EvolutionaryRun.h"
#include "QErrorLog.h"
#include <string>
#include <vector>

using namespace std;

MemoryPool RunTrialWorker::s_Pool( sizeof( RunTrialWorker ), 100 );

RunTrialWorker::RunTrialWorker( AbstractTrialFactory       *trialFactory,
                                const TrialInfo&            trialInfo,
				vector<AbstractGenotype*>&  genotypes,
				unsigned long               generationNo,
				const MemFile&              extraBytes ) :
    m_TrialInfo(    trialInfo    ),
    m_Trial(        NULL         ),
    m_Genotypes(    genotypes    ),
    m_GenerationNo( generationNo ),
    m_ExtraBytes(   extraBytes   ),
    m_TrialFactory( trialFactory ) { }

void RunTrialWorker::start() {
  try {  
    unsigned long sizeInBytes;
    const unsigned char *bytes = m_ExtraBytes.getMemFile( &sizeInBytes );
  
    try {
      m_Trial = m_TrialFactory->createTrial( m_Genotypes,
					     m_GenerationNo,
					     bytes,
					     sizeInBytes );
    } catch( QException& e ) {
      e.fillInStackTrace();
      throw;
    }

    m_Trial->performTrial( m_TrialInfo );

    delete m_Trial;
  } catch( EvolutionaryRun::InvalidTrial& e ) {
    QErrorLog::writeError( "Invalid 1" );
    m_TrialInfo.setTrialState( TrialInfo::INVALID_TRIAL );

    unsigned long  genotypeID;
    vector<FLOAT>  fitnesses;
    FLOAT          databaseFitness;
    unsigned char  state;
    int            typeNo;
    int            i;
    for ( i = 0; 
          m_TrialInfo.getGenotypeInfo( i, 
                                       &genotypeID, 
                                       fitnesses, 
                                       &databaseFitness, 
                                       &state, 
                                       &typeNo ) == true;
          i++ ) {
      m_TrialInfo.setGenotypeInfo( genotypeID,
                                   fitnesses,
                                   0,
                                   PopulationMember::INVALID_GENOTYPE,
                                   typeNo );
    }
    QErrorLog::writeError( "Invalid 2" );
  } catch( QException& e ) {
    e.fillInStackTrace();

    string errMsg;
    errMsg += "Exception caught in thread - ";
    errMsg += e.toString();
    errMsg += "\n";
    errMsg += e.getStackTrace();
    QErrorLog::writeError( errMsg.c_str() );
  
    throw;
  }
}

const unsigned char *RunTrialWorker::readObject( const unsigned char *s ) {
  /**
   * All we really have to do is read in the trial info.
   */
  s = m_TrialInfo.readObject( s );
  
  return s;
}

unsigned char *RunTrialWorker::writeObject( unsigned char *s ) {
  s = m_TrialInfo.writeObject( s );
  return s;
}
