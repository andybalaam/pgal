/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "QObservable.h"

/**
 * This class presents a class that can be observed. It is
 * loosely based on the Java java.util.Observable class.
 */

void QObservable::addObserver( QObserver *o ) {
  m_AllObservers.insert( o );
}

void QObservable::deleteObserver( QObserver *o ) {
  m_AllObservers.erase( o );
}

void QObservable::deleteObservers() {
  m_AllObservers.erase( m_AllObservers.begin(), m_AllObservers.end() );
}

void QObservable::notifyObservers() {
  set<QObserver *>::iterator i;
  for ( i = m_AllObservers.begin(); i != m_AllObservers.end(); i++ ) {
    (*i)->update( this, NULL );
  }
}

void QObservable::notifyObservers( void *arg ) {
  set<QObserver *>::iterator i;
  for ( i = m_AllObservers.begin(); i != m_AllObservers.end(); i++ ) {
    (*i)->update( this, arg );
  }
}
