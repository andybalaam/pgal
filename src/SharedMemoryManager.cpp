#include "SharedMemoryManager.h"
#include "QErrorLog.h"
#include <errno.h>
#include <string>
#include <string.h>

using namespace std;

SharedMemoryManager::SharedMemoryManager( int    noOfBlocks,
					  size_t sizeOfEachBlock ) :
    m_RawMemory(       NULL            ),
    m_NoOfBlocks(      noOfBlocks      ),
    m_SizeOfEachBlock( sizeOfEachBlock ) { 
  /**
   * We need to grab some shared memory.
   */
  unsigned int totalSize = noOfBlocks * sizeOfEachBlock;

  errno = 0;
  m_ShmID = shmget( IPC_PRIVATE, totalSize, IPC_CREAT | 0666 );
  if ( m_ShmID == -1 ) {
    string errMsg;
    errMsg += "Cannot grab shared memory - error states: ";
    errMsg += strerror( errno );

    CannotAllocateSharedMemory e( errMsg.c_str() );
    e.fillInStackTrace();
    throw e;
  }

  errno = 0;
  void *tempRawMemory = shmat( m_ShmID, NULL, 0 );
  if ( tempRawMemory == (void*)-1 ) {
    char errMsg[ 1024 ];
    sprintf( errMsg, "Cannot find shared memory with ID of %d",
	     m_ShmID );
    
    CannotFindSharedMemory e( errMsg );
    e.fillInStackTrace();
    throw e;
  }

  m_RawMemory = (unsigned char *)tempRawMemory;
}

SharedMemoryManager::~SharedMemoryManager() {
  if ( m_ShmID != -1 ) {
    errno = 0;
    if ( shmctl( m_ShmID, IPC_RMID, 0 ) == -1 ) {
      char errMsg[ 1024 ];
      sprintf( errMsg, "Cannot properly release shared memory with key %d: "
  	       "error states: %s",
	       m_ShmID, strerror( errno ) );

      QErrorLog::writeError( errMsg );
    }
  }
}

unsigned char *SharedMemoryManager::grabSpace( int blockNo ) {
  if ( blockNo >= m_NoOfBlocks ) {
    char errMsg[ 1024 ];
    sprintf( errMsg, "Block no (%d) larger than no of blocks (%d)",
	     blockNo, m_NoOfBlocks );

    CannotGrabMemory e( errMsg );
    e.fillInStackTrace();
    throw e;
  }

  return &( ( (unsigned char *)m_RawMemory )[ blockNo * m_SizeOfEachBlock ] );
}
