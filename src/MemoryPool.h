#ifndef MEMORYPOOL_H
#define MEMORYPOOL_H

/**
 * This class is an attempt to speed up memory allocation. It
 * keeps a pool of nodes of a given size, all connected via
 * a linked list. You can allocate or free blocks of memory.
 * They are then either added to or removed from the linked list.
 * Each block of allocated memory is treated as though it had
 * a structure just before it, which contains the left and right
 * nodes.
 *
 * Note that you must use the same memory pool manager to free
 * the memory as the one used to allocate it. Freeing NULL is
 * OK. Memory may become fragmented. The value returned is rounded
 * to 16 bytes so it should be suitable for storing any data type.
 * Note also that this is for use really in the 'list' or
 * 'map' container, not really the 'vector' one. So if space for
 * more than one element is allocated, it is taken from the
 * free store. Deallocating it in the normal way is OK.
 */

#include <vector>
#include <pthread.h>

#define INCREASE_BY_SIZE 20

using namespace std;

class MemoryPool {
public:
  MemoryPool( int blockSize, 
	      int increaseBySize = INCREASE_BY_SIZE );
  virtual ~MemoryPool();

  inline unsigned char *allocateMemory();
  inline void           freeMemory( unsigned char *memory );

  int   getNoOfFreeBlocks()      const { return m_NoOfFreeBlocks;      }
  int   getNoOfAllocatedBlocks() const { return m_NoOfAllocatedBlocks; }

protected:
  struct S_NodeItem {
    struct S_NodeItem *m_Left;
    struct S_NodeItem *m_Right;
    unsigned char emptyFill[ 16 - ( 2 * sizeof( struct S_NodeItem * ) ) ];
  };

  int                      m_BlockSize;
  int                      m_IncreaseBySize;
  int                      m_NoOfFreeBlocks;
  int                      m_NoOfAllocatedBlocks;
  vector<unsigned char *>  m_AllocatedMemory;
  struct S_NodeItem       *m_HeadOfFreeList;

  void allocateMoreMemory( int noOfBlocks );

  void initialiseMemorySpace( unsigned char *startOfMemory,
			      int            noOfBlocks );

  pthread_mutex_t m_Mutex;
};

inline unsigned char *MemoryPool::allocateMemory() {
  /**
   * This returns a single block of memory. However, if there isn't any
   * left, we have to allocate another block.
   */

  pthread_mutex_lock( &m_Mutex );

  if ( m_HeadOfFreeList == NULL ) {
    allocateMoreMemory( m_IncreaseBySize );
  }
  
  unsigned char *s = (unsigned char *)m_HeadOfFreeList;
  m_HeadOfFreeList = m_HeadOfFreeList->m_Right;
  
  if ( m_HeadOfFreeList != NULL ) {
    m_HeadOfFreeList->m_Left = NULL;
  }

  m_NoOfAllocatedBlocks++;
  m_NoOfFreeBlocks--;

  pthread_mutex_unlock( &m_Mutex );

  return s + sizeof( struct S_NodeItem );
}

inline void MemoryPool::freeMemory( unsigned char *s ) {
  pthread_mutex_lock( &m_Mutex );

  if ( s != NULL ) {
    s -= sizeof( struct S_NodeItem );
    (( struct S_NodeItem *)s)->m_Left  = NULL;
    (( struct S_NodeItem *)s)->m_Right = m_HeadOfFreeList;

    if ( m_HeadOfFreeList != NULL ) {
      m_HeadOfFreeList->m_Left = (( struct S_NodeItem *)s);
    }

    m_HeadOfFreeList = (struct S_NodeItem *)s;

    m_NoOfFreeBlocks++;
    m_NoOfAllocatedBlocks--;
  }
  pthread_mutex_unlock( &m_Mutex );
}

#endif
