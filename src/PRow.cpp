/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "PRow.h"

#include "PResultSet.h"
#include "SmallString.h"

PRow::PRow( MYSQL_ROW   mysql_row, 
            PResultSet *parent ) :
  m_Row( mysql_row ),
  m_ResultSet( parent ) { }

long PRow::GetLongField( const char *fieldName ) {
  return atoi( m_Row[ m_ResultSet->GetFieldNo( fieldName ) ] );
}

long PRow::GetLongField( long fieldNo ) {
  return atoi( m_Row[ fieldNo ] );
}
	
FLOAT PRow::GetFloatField( const char *fieldName ) {
  return atof( m_Row[ m_ResultSet->GetFieldNo( fieldName ) ] );
}

FLOAT PRow::GetFloatField( long fieldNo ) {
  return atof( m_Row[ fieldNo ] );
}

char *PRow::GetStringField( const char *fieldName ) {
  return GetStringField( m_ResultSet->GetFieldNo( fieldName ) );
}

char *PRow::GetStringField( long fieldNo ) {
  return m_Row[ fieldNo ];
}

bool PRow::GetFieldIsNull( const char *fieldName ) {
  return GetFieldIsNull( m_ResultSet->GetFieldNo( fieldName ) );
}

bool PRow::GetFieldIsNull( long fieldNo ) {
  return ( m_Row[ fieldNo ] == NULL ) ? true : false;
}

bool PRow::IsValid() {
  return ( m_Row != NULL ) ? true : false;
}
