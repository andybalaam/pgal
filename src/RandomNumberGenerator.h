/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef RANDOMNUMBERGENERATOR_H
#define RANDOMNUMBERGENERATOR_H

/**
 * This generates random numbers and allows the seed to be saved.
 */

class RandomNumberGenerator {
public:
  virtual void           setRandomSeed( unsigned long )         = 0;
  virtual void           randomise()                            = 0;
  virtual unsigned short random16BitInt( unsigned short range ) = 0;
  virtual unsigned long  randomLong()                           = 0;
  virtual double         randomDouble()                         = 0;
  virtual bool           randomBit()                            = 0;

  virtual double         gaussianRand();

  static RandomNumberGenerator *getDefaultGoodGenerator();
  static RandomNumberGenerator *getDefaultFastGenerator();

protected:
  static RandomNumberGenerator *s_DefaultGoodGenerator;
  static RandomNumberGenerator *s_DefaultFastGenerator;
};

#endif
