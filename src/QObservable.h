/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef QOBSERVABLE_H
#define QOBSERVABLE_H

#include "QObserver.h"
#include <set>
using namespace std;

/**
 * This class presents a class that can be observed. It is
 * loosely based on the Java java.util.Observable class.
 */

class QObservable {
public:
  QObservable()                      { m_HasChangedFlag = false;     }
  virtual ~QObservable() { }
  
  void addObserver( QObserver * );
  int  countObservers()              { return m_AllObservers.size(); }
  void deleteObserver( QObserver * );
  void deleteObservers();
  void notifyObservers();
  void notifyObservers( void *arg );
  
  bool hasChanged()                  { return m_HasChangedFlag;    }
  void clearChanged()                { m_HasChangedFlag = false;   }
  void setChanged()                  { m_HasChangedFlag = true;    }
  
protected:
  set<QObserver *> m_AllObservers;
  bool             m_HasChangedFlag;
};

#endif
