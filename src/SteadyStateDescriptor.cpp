/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "SteadyStateDescriptor.h"
#include "EvolutionaryRun.h"

SteadyStateDescriptor::SteadyStateDescriptor( int populationSize ) :
    m_PopulationSize( populationSize ),
    m_RandomNumbers( RandomNumberGenerator::getDefaultGoodGenerator() ) {
  if ( m_PopulationSize <= 1 ) {
    SmallString msg;
    msg += "Population of ";
    msg += m_PopulationSize;
    msg += " invalid";

    SteadyStateDescriptorException e( msg );
    e.fillInStackTrace();
    throw e;
  }
}

void SteadyStateDescriptor::getGenotypesToSendToTrial(
    vector<unsigned long>                        *toSendToTrial,
    const map<unsigned long,AbstractGenotype *>&  population ) {
  map<unsigned long,AbstractGenotype *>::const_iterator i;

  if ( population.size() != m_PopulationSize ) {
    SmallString errMsg;
    errMsg += "Found unexpected population size of ";
    errMsg += population.size();
    errMsg += " instead of ";
    errMsg += m_PopulationSize;
 
    SteadyStateDescriptorException e( errMsg );
    e.fillInStackTrace();
    throw e;
  }

  /**
   * We'll pick two and the winner stays...
   */
  
  m_GenotypeNo1 = m_RandomNumbers->random16BitInt( 12 );
  m_GenotypeNo2 = m_GenotypeNo1;
  while ( m_GenotypeNo2 == m_GenotypeNo1 ) {
    m_GenotypeNo2 = m_RandomNumbers->random16BitInt( 12 );
  }

  int j;
  for ( j = 0, i = population.begin(); 
	j < m_GenotypeNo1; 
	j++, i++ )
    ;
  m_GenotypeNo1 = i->first;

  for ( j = 0, i = population.begin(); 
	j < m_GenotypeNo2; 
	j++, i++ )
    ;
  m_GenotypeNo2 = i->first;

  toSendToTrial->push_back( m_GenotypeNo1 );
  toSendToTrial->push_back( m_GenotypeNo2 );
}

void SteadyStateDescriptor::getNextGenerationInformation(
    set<unsigned long>                      *toBeRemoved,
    vector<unsigned long>                   *toBeReproduced,
    const map<unsigned long,FLOAT>&          fitnesses,
    const map<unsigned long,unsigned char>&  states ) {
  /**
   * We find the fitnesses of our two previous ones, and
   * mutate the winner stays and reproduces with probability
   * of 90%.
   */
  unsigned long winner;
  unsigned long loser;

  map<unsigned long,FLOAT>::const_iterator fitness1 = 
    fitnesses.find( m_GenotypeNo1 );

  map<unsigned long,FLOAT>::const_iterator fitness2 = 
    fitnesses.find( m_GenotypeNo2 );

  map<unsigned long,unsigned char>::const_iterator state1 =
    states.find( m_GenotypeNo1 );

  map<unsigned long,unsigned char>::const_iterator state2 =
    states.find( m_GenotypeNo2 );

  if ( state1->second == EvolutionaryRun::INVALID_GENOTYPE ) {
    winner = m_GenotypeNo2;
    loser  = m_GenotypeNo1;
  } else if ( state2->second == EvolutionaryRun::INVALID_GENOTYPE ) {
    winner = m_GenotypeNo1;
    loser  = m_GenotypeNo2;
  } else if ( fitness1->second > fitness2->second ) {
    winner = m_GenotypeNo1;
    loser  = m_GenotypeNo2;
  } else {
    winner = m_GenotypeNo2;
    loser  = m_GenotypeNo1;
  }

  state1 = states.find( loser );
  if ( m_RandomNumbers->randomDouble() > 0.9 &&
       state1->second != EvolutionaryRun::INVALID_GENOTYPE ) {
    unsigned long temp = winner;
    winner = loser;
    loser = temp;
  }

  /**
   * Right, loser goes...
   */
  toBeRemoved->insert( loser );
  
  /**
   * ...and winner is copied.
   */
  toBeReproduced->push_back( winner );
}
