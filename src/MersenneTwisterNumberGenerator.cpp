/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


/* A C-program for MT19937: Integer version (1999/10/28)          */
/*  genrand() generates one pseudorandom unsigned integer (32bit) */
/* which is uniformly distributed among 0 to 2^32-1  for each     */
/* call. sgenrand(seed) sets initial values to the working area   */
/* of 624 words. Before genrand(), sgenrand(seed) must be         */
/* called once. (seed is any 32-bit integer.)                     */
/*   Coded by Takuji Nishimura, considering the suggestions by    */
/* Topher Cooper and Marc Rieffel in July-Aug. 1997.              */

/* This library is free software; you can redistribute it and/or   */
/* modify it under the terms of the GNU Library General Public     */
/* License as published by the Free Software Foundation; either    */
/* version 2 of the License, or (at your option) any later         */
/* version.                                                        */
/* This library is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of  */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            */
/* See the GNU Library General Public License for more details.    */
/* You should have received a copy of the GNU Library General      */
/* Public License along with this library; if not, write to the    */
/* Free Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA   */ 
/* 02111-1307  USA                                                 */

/* Copyright (C) 1997, 1999 Makoto Matsumoto and Takuji Nishimura. */
/* Any feedback is very welcome. For any question, comments,       */
/* see http://www.math.keio.ac.jp/matumoto/emt.html or email       */
/* matumoto@math.keio.ac.jp                                        */

/* REFERENCE                                                       */
/* M. Matsumoto and T. Nishimura,                                  */
/* "Mersenne Twister: A 623-Dimensionally Equidistributed Uniform  */
/* Pseudo-Random Number Generator",                                */
/* ACM Transactions on Modeling and Computer Simulation,           */
/* Vol. 8, No. 1, January 1998, pp 3--30.                          */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#include "MersenneTwisterNumberGenerator.h"

/* Period parameters */
#define M 397
#define MATRIX_A 0x9908b0df   /* constant vector a */
#define UPPER_MASK 0x80000000 /* most significant w-r bits */
#define LOWER_MASK 0x7fffffff /* least significant r bits */

/* Tempering parameters */
#define TEMPERING_MASK_B 0x9d2c5680
#define TEMPERING_MASK_C 0xefc60000
#define TEMPERING_SHIFT_U(y)  (y >> 11)
#define TEMPERING_SHIFT_S(y)  (y << 7)
#define TEMPERING_SHIFT_T(y)  (y << 15)
#define TEMPERING_SHIFT_L(y)  (y >> 18)

long MersenneTwisterNumberGenerator::s_InitialNum1 = 0;
long MersenneTwisterNumberGenerator::s_InitialNum2 = 0;

void MersenneTwisterNumberGenerator::setInitialNum1( long n ) {
  s_InitialNum1 = n;
}

void MersenneTwisterNumberGenerator::setInitialNum2( long n ) {
  s_InitialNum2 = n;
}

MersenneTwisterNumberGenerator::MersenneTwisterNumberGenerator() {
  
#ifdef MAKE_THREAD_SAFE
  pthread_mutexattr_t defaultMutexAttributes;
  pthread_mutexattr_init( &defaultMutexAttributes );
  
  pthread_mutex_init( &m_Mutex, &defaultMutexAttributes );
#endif

  m_DoneRandomise      = false;
  m_DoneFirstRandomNum = false;
  m_mti = N + 1;
}

/* Initializing the array with a seed */
void MersenneTwisterNumberGenerator::setRandomSeed( unsigned long seed ) {
  /*{ // DEBUG block
    fprintf( stderr, "Random seed = %lu\n", seed );
    }*/
  
  int i;
  
  for (i=0;i<N;i++) {
    m_mt[i] = seed & 0xffff0000;
    seed = 69069 * seed + 1;
    m_mt[i] |= (seed & 0xffff0000) >> 16;
    seed = 69069 * seed + 1;
  }
  m_mti = N;
}

void MersenneTwisterNumberGenerator::randomise() {

#ifdef MAKE_THREAD_SAFE
  pthread_mutex_lock( &m_Mutex );
#endif

  if ( m_DoneRandomise == false ) {
    long num;
    FILE *input = fopen( "/dev/random", "rb" );
    fread( &num, sizeof( long ), 1, input );
    fclose( input );
    
    setRandomSeed( time( NULL ) + ( num % 0xEFFFFF ) );

    m_DoneRandomise = true;
  }

#ifdef MAKE_THREAD_SAFE
  pthread_mutex_unlock( &m_Mutex );
#endif
}

unsigned long MersenneTwisterNumberGenerator::randomLong() {
  /*if ( m_DoneFirstRandomNum == false ) {
    setRandomSeed( s_InitialNum1 + s_InitialNum2 );
    m_DoneFirstRandomNum = true;
    }*/

#ifdef MAKE_THREAD_SAFE
  pthread_mutex_lock( &m_Mutex );
#endif

    unsigned long y;
    static unsigned long mag01[2]={0x0, MATRIX_A};
    /* mag01[x] = x * MATRIX_A  for x=0,1 */

    if (m_mti >= N) { /* generate N words at one time */
        int kk;

        if (m_mti == N+1)   /* if sgenrand() has not been called, */
            setRandomSeed(4357); /* a default initial seed is used   */

        for (kk=0;kk<N-M;kk++) {
            y = (m_mt[kk]&UPPER_MASK)|(m_mt[kk+1]&LOWER_MASK);
            m_mt[kk] = m_mt[kk+M] ^ (y >> 1) ^ mag01[y & 0x1];
        }
        for (;kk<N-1;kk++) {
            y = (m_mt[kk]&UPPER_MASK)|(m_mt[kk+1]&LOWER_MASK);
            m_mt[kk] = m_mt[kk+(M-N)] ^ (y >> 1) ^ mag01[y & 0x1];
        }
        y = (m_mt[N-1]&UPPER_MASK)|(m_mt[0]&LOWER_MASK);
        m_mt[N-1] = m_mt[M-1] ^ (y >> 1) ^ mag01[y & 0x1];

        m_mti = 0;
    }

    y = m_mt[m_mti++];

#ifdef MAKE_THREAD_SAFE
  pthread_mutex_unlock( &m_Mutex );
#endif

    y ^= TEMPERING_SHIFT_U(y);
    y ^= TEMPERING_SHIFT_S(y) & TEMPERING_MASK_B;
    y ^= TEMPERING_SHIFT_T(y) & TEMPERING_MASK_C;
    y ^= TEMPERING_SHIFT_L(y);

    return y;
}

double MersenneTwisterNumberGenerator::randomDouble() {
    return ( ((double)randomLong() + 1.0) * 2.3283064359965952e-10 ); /* reals: (0,1)-interval */
}

unsigned short int MersenneTwisterNumberGenerator::random16BitInt(
      unsigned short int range ) {
  unsigned short int r = (((randomLong() >> 16 ) & 0xFFFF ) * range ) / 0xFFFF;
  
  while ( r == range ) {
    r = (((randomLong() >> 16 ) & 0xFFFF ) * range ) / 0xFFFF;
  }

  return r;
}

bool MersenneTwisterNumberGenerator::randomBit() {
  if ( random16BitInt( 2 ) == 0 ) {
    return false;
  } else {
    return true;
  }
}

