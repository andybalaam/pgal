/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef TOPTHIRDSDESCRIPTOR_H
#define TOPTHIRDSDESCRIPTOR_H

#include "AbstractGenerationDescriptor.h"
#include "RandomNumberGenerator.h"
#include "QException.h"

class TopThirdsDescriptor : public AbstractGenerationDescriptor {
public:
  QEXCEPTIONCLASS( TopThirdsDescriptorException );

  TopThirdsDescriptor( int populationSize );
  virtual ~TopThirdsDescriptor();

  void getGenotypesToSendToTrial( 
      vector<unsigned long>                        *toSendToTrial,
      const map<unsigned long,AbstractGenotype *>&  population );

  void getNextGenerationInformation( 
      set<unsigned long>                      *toBeRemoved,
      vector<unsigned long>                   *toBeReproduced,
      const map<unsigned long,FLOAT>&          fitnesses,
      const map<unsigned long,unsigned char>&  states );
  
protected:
  struct S_fitnesses {
    FLOAT         fitness;
    unsigned long genotypeNo;
    unsigned char state;
  };

  static int sortFitnessStructures( const void *, const void * );

  int                    m_PopulationSize;
  struct S_fitnesses    *m_AllFitnesses;
  RandomNumberGenerator *m_RandomNumbers;
};

#endif
