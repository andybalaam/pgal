/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "FastNumberGenerator.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

bool FastNumberGenerator::s_DoneRandomise = false;

void FastNumberGenerator::setRandomSeed( unsigned long s ) {
  srand( s );
}

void FastNumberGenerator::randomise() {
  if ( s_DoneRandomise == false ) {
    setRandomSeed( time( NULL ) );
    s_DoneRandomise = true;
  }
}

unsigned short FastNumberGenerator::random16BitInt( unsigned short r ) {
  return randomLong() % r;
}

unsigned long FastNumberGenerator::randomLong() {
  union U_longInt {
    unsigned long longNum;
    unsigned char charNum[ sizeof( unsigned long ) ];
  } longInt;

  int i;
  for ( i = 0; i < sizeof( unsigned long ); i++ ) {
    longInt.charNum[ i ] = ( rand() >> 16 ) % 256;
  }

  return longInt.longNum;
}

double FastNumberGenerator::randomDouble() {
  return (double)rand() / (double)RAND_MAX;
}

bool FastNumberGenerator::randomBit() {
  if ( randomDouble() >= 0.5 ) {
    return false;
  } else {
    return true;
  }
}
