#ifndef PSEMAPHORE_H
#define PSEMAPHORE_H


#include <pthread.h>
#include <sys/ipc.h>
#include <sys/sem.h>

#include "QException.h"

/**
 * Semaphore class used to make sure single access to
 * resources are possible.
 */

class PSemaphore {
public:
    QEXCEPTIONCLASS(        PSemaphoreException                         );
    QEXCEPTIONDERIVEDCLASS( CannotCreateSemaphore,  PSemaphoreException );
    QEXCEPTIONDERIVEDCLASS( CannotDestroySemaphore, PSemaphoreException );
    QEXCEPTIONDERIVEDCLASS( CannotPostSemaphore,    PSemaphoreException );
    QEXCEPTIONDERIVEDCLASS( CannotReleaseSemaphore, PSemaphoreException );
    QEXCEPTIONDERIVEDCLASS( NotImplemented,         PSemaphoreException );

    PSemaphore();
    PSemaphore( bool grabStraightAway );
    virtual ~PSemaphore();

    void post();
    void release();  
    bool isPosted() { return m_CurrentlyLocked; }

protected:
    int           m_SID;
    bool          m_CurrentlyLocked;

    static sembuf s_SEMBUF_POST;
    static sembuf s_SEMBUF_RELEASE;
};

#endif

