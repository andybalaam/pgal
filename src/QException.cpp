/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "QException.h"

QException::QException() {
    m_Msg = getName();
}

QException::QException( long lineNo, const char *filename ) {
    m_Msg = getName();
    addToStackTrace( lineNo, filename, "First thrown" );
}

QException::QException( const char *msg ) {
    m_Msg = msg;
}

QException::QException( long lineNo, const char *filename, const char *msg ) {
    m_Msg = msg;
    SmallString errMsg;
    errMsg += "First thrown: ";
    errMsg += msg;
    addToStackTrace( lineNo, filename, errMsg );
}

QException::QException( const QException& e ) {
    m_Msg = e.m_Msg;
    m_StackTrace = e.m_StackTrace;
}

QException::QException( const QException& e, const char *newMsg ) {
    m_Msg = newMsg;
    m_StackTrace = e.m_StackTrace;
}

void QException::makeEqualTo( const QException& e ) {
    m_Msg = e.m_Msg;
    m_StackTrace = e.m_StackTrace;
}

const char *QException::getName() const {
    return "QException";
}

const char *QException::getMessage() const {
    return m_Msg;
}

const char *QException::toString() const {
    if ( ( m_Msg.getLength() || strlen( getName() ) ) &&
         m_StringVersion.getLength() == 0 ) {
        m_StringVersion  = getName();
        m_StringVersion += ":\t";
        m_StringVersion += m_Msg;
    }

    return m_StringVersion;
}

const char *QException::getStackTrace() const {
    return m_StackTrace;
}

void QException::__fillInExceptionABC1234( long lineNo, const char *fileName ) {
    addToStackTrace( lineNo, fileName, m_Msg );
}

void QException::addToStackTrace( long lineNo, const char *fileName, const char *msg ) {
    SmallString tempVariable = m_StackTrace;
    m_StackTrace  = getName();
    m_StackTrace += ":\t";

    const char *f = &(fileName[ strlen( fileName ) ]);
    while ( f > fileName && *f != '\\' && *f != '/' ) {
      f--;
    }

    m_StackTrace += f;
    m_StackTrace += ":\t";
    m_StackTrace += lineNo;
    m_StackTrace += ":\t";
    m_StackTrace += msg;
    
    if ( tempVariable.getLength() ) {
        m_StackTrace += "\n";
        m_StackTrace += tempVariable;
    }
}

const unsigned char *QException::readObject( const unsigned char *s ) {
  m_Msg = "";
  m_Msg += (const char *)s;
  s += m_Msg.getLength() + 1;

  m_StackTrace = "";
  m_StackTrace += (const char *)s;
  s += m_StackTrace.getLength() + 1;

  m_StringVersion = "";
  m_StringVersion += (const char *)s;
  s += m_StringVersion.getLength() + 1;

  return s;
}

unsigned char *QException::writeObject( unsigned char *s ) {
  strcpy( (char *)s, (const char *)m_Msg );
  s += strlen( (const char *)s ) + 1;

  strcpy( (char *)s, (const char *)m_StackTrace );
  s += strlen( (const char *)s ) + 1;

  strcpy( (char *)s, (const char *)m_StringVersion );
  s += strlen( (const char *)s ) + 1;

  return s;
}
