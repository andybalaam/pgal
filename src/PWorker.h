#ifndef PWORKER_H
#define PWORKER_H

class PWorker {
public:
  virtual ~PWorker() { }

  virtual void start() = 0;
};

#endif
