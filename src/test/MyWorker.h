#ifndef MYWORKER_H
#define MYWORKER_H

#include "Serializable.h"
#include "PWorker.h"
#include <vector>

using namespace std;

class MyWorker : public PWorker, public Serializable {
public:
  /**
   * This simple class builds a huge list of numbers and
   * adds them up.
   */
  MyWorker();

  bool checkIsValid();

  /**
   * From PWorker
   */
  void start();
  
  /**
   * From Serializable
   */
  const unsigned char *readObject( const unsigned char * );
  unsigned char       *writeObject( unsigned char * );

protected:
  long         m_AddedNumber;
  vector<long> m_AllNumbers;
};

#endif
