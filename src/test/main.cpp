/**
 * This program tests the PProcessBossWorker class.
 */

#include "PProcessBossWorker.h"
#include "MyWorker.h"
#include "PProcessBossWorker.h"

int main() {
  try {
    PProcessBossWorker bossWorker( 5, 1024 );
    
    /**
     * Now we'll set up a lot of workers to run...
     */
    int i;
    for ( i = 0; i < 45; i++ ) {
      bossWorker.run( new MyWorker );
    }
    
    for ( i = 0; i < 45; i++ ) {
      MyWorker *myWorker = 
	dynamic_cast<MyWorker*>
	( bossWorker.waitForProcessToFinish( true ) );
      
      if ( myWorker->checkIsValid() == false ) {
	fprintf( stderr, "%d: failed\n", i );
      } else {
	fprintf( stderr, "%d: succeeded\n", i );
      }
    }
  } catch( QException& e ) {
    fprintf( stderr, "Caught exception: %s\n%s\n",
	     e.toString(), e.getStackTrace() );
    exit( 1 );
  }

  return 0;
}
