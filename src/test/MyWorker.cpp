#include "MyWorker.h"
#include <stdlib.h>

MyWorker::MyWorker() : m_AddedNumber( 0 ) {
  int i;
  long randNo;
  for ( i = 0; i < 50; i++ ) {
    randNo = rand() % 100;
    m_AllNumbers.push_back( randNo );
  }
}

bool MyWorker::checkIsValid() {
  long addedNo = 0;

  vector<long>::const_iterator i;
  for ( i = m_AllNumbers.begin(); i != m_AllNumbers.end(); i++ ) {
    addedNo += *i;
  }

  if ( addedNo == m_AddedNumber ) {
    return true;
  } else {
    return false;
  }
}

void MyWorker::start() {
  int i;
  int j = rand() % 100000 + 10000;

  if ( rand() % 4 == 0 ) {
    j = 1;
  }
  
  for ( i = 0; i < j; i++ ) {
    m_AddedNumber = 0;
    vector<long>::const_iterator i;
    for ( i = m_AllNumbers.begin(); i != m_AllNumbers.end(); i++ ) {
      m_AddedNumber += *i;
    }
  }
}

const unsigned char *MyWorker::readObject( const unsigned char *s ) {
  m_AddedNumber = atol( (const char *)s );
  while ( *s != '\n' ) {
    s++;
  }
  s++;

  m_AllNumbers.clear();

  int i;
  for ( i = 0; i < 50; i++ ) {
    m_AllNumbers.push_back( atol( (const char *)s ) );
    while ( *s != '\n' ) {
      s++;
    }
    s++;
  }

  return s;
}

unsigned char *MyWorker::writeObject( unsigned char *s ) {
  char number[ 100 ];
  sprintf( number, "%ld\n", m_AddedNumber );
  strcpy( (char *)s, number );
  s += strlen( number );

  vector<long>::const_iterator i;
  for ( i = m_AllNumbers.begin(); i != m_AllNumbers.end(); i++ ) {
    sprintf( number, "%ld\n", *i );
    strcpy( (char *)s, number );
    s += strlen( number );
  }

  return s;
}
