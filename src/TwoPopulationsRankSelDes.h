/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef TWOPOPULATIONSRANKSELDES_H
#define TWOPOPULATIONSRANKSELDES_H

#include "AbstractGenerationDescriptor.h"
#include "AbstractFitnessComparator.h"
#include "AverageFitnessComparator.h"
#include "QException.h"
#include "RandomNumberGenerator.h"
#include "PopulationMember.h"
#include "TrialInfo.h"

class TwoPopulationsRankSelDes : public AbstractGenerationDescriptor {
public:
  QEXCEPTIONCLASS( TwoPopulationsRankSelDesException );

  TwoPopulationsRankSelDes( int populationSize );
  virtual ~TwoPopulationsRankSelDes();

  void getGenotypesToSendToTrial( 
      queue<TrialInfo>&                          toSendToTrial,
      const map<unsigned long,PopulationMember>& population );

  void getNextGenerationInformation( 
      set<unsigned long>&                   toBeRemoved,
      queue<unsigned long>&                 toBeReproduced,
      map<unsigned long,PopulationMember>&  population,
      const vector<TrialInfo>&              trials );

  bool setNoOfElite( int noOfElite );
  bool setNoOfTrialsPerGenotype( int );
  void setFitnessComparator( AbstractFitnessComparator * );

protected:
  class S_fitnesses {
  public:
    vector<vector<FLOAT> >     fitnesses;  
    FLOAT                      databaseFitness;
    unsigned long              genotypeNo;
    unsigned char              state;
    FLOAT                      lowerProbability;
    FLOAT                      higherProbability;
    bool                       alreadySet;
    AbstractFitnessComparator *fitnessComparator;
  };

  static int sortFitnessStructures( const void *, const void * );

  static AverageFitnessComparator m_DefaultFitnessComparator;

  int                     m_SubPopulationSize;
  struct S_fitnesses    **m_Fitnesses1;
  struct S_fitnesses    **m_Fitnesses2;
  RandomNumberGenerator  *m_RandomNumbers;
  int                     m_NoOfElite;
  int                     m_NoOfTrialsPerGenotype;
};

#endif
