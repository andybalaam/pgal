#ifndef AVERAGEFITNESSCOMPARATOR_H
#define AVERAGEFITNESSCOMPARATOR_H

/**
 * This class gets the average fitness between the given fitnesses.
 * It averages everything, every subtrial in every trial.
 */

#include "AbstractFitnessComparator.h"

class AverageFitnessComparator : public AbstractFitnessComparator {
public:
  /**
   * This returns -1 if a < b, 0 if a == b, and 1 if a > b
   */
  int   compareFitness(   const vector<vector<FLOAT> >& a, 
			  const vector<vector<FLOAT> >& b );

  FLOAT calculateFitness( const vector<vector<FLOAT> >& );
};

#endif
