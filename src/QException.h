/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef QEXCEPTION_H
#define QEXCEPTION_H

#include "SmallString.h"
#include "Serializable.h"

#define fillInStackTrace() __fillInExceptionABC1234(__LINE__,__FILE__)

#define QEXCEPTIONCLASS(A)                                                          \
                                                                                    \
class A : public QException {                                                       \
    protected:                                                                      \
        const char *getName() const { return #A; }                                  \
                                                                                    \
    public:                                                                         \
        A( const char *msg ) : QException( msg )   {}                               \
        A( long l, const char *f, const char *m ) : QException( l, f, m ) {}        \
        A( const QException& e ) : QException( e ) {}                               \
        A( const QException& e, const char *newMsg ) : QException( e, newMsg ) {}   \
};

#define QEXCEPTIONDERIVEDCLASS(A,B)                                                 \
                                                                                    \
class A : public B {                                                                \
    protected:                                                                      \
        const char *getName() const { return #A; }                                  \
                                                                                    \
    public:                                                                         \
        A( const char *msg ) : B( msg )   {}                                        \
        A( long l, const char *f, const char *m ) : B( l, f, m ) {}                 \
        A( const QException& e ) : B( e ) {}                                        \
        A( const QException& e, const char *newMsg ) : B( e, newMsg ) {}            \
};

class QException : public Serializable {
    private:
        SmallString m_Msg;
        SmallString m_StackTrace;
        mutable SmallString m_StringVersion;

    protected:
        virtual const char *getName() const;
        virtual void        makeEqualTo( const QException& e );
    
        void                addToStackTrace( long lineNo, 
                                             const char *filename,
                                             const char *msg ); 
    public:
        QException();
        QException( long lineNo, const char *fileName );
        QException( const char *msg );
        QException( long lineNo, const char *fileName, const char *msg );
        QException( const QException& );
        QException( const QException&, const char *newMsg );
	virtual ~QException() { }

        virtual QException& operator=( const QException& e ) { makeEqualTo( e ); return *this; }

        const char *getMessage()    const;
        const char *toString()      const;
        const char *getStackTrace() const;
        void        __fillInExceptionABC1234( long lineNo, 
                                              const char *fileName );

	const unsigned char *readObject( const unsigned char * );
	unsigned char       *writeObject( unsigned char * );
};

#endif

