/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef SMALLSTRING_H
#define SMALLSTRING_H

extern "C" {
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <ctype.h>
}

class SmallString {
    private:
        char       *m_Start;
        long        m_StringLength;         /* Equivalent to strlen */
        long        m_TotalLength;          /* Real length of area  */
        long        m_IncreaseBySize; 

        inline void Write( const char * );

    public:
        inline SmallString();
        inline SmallString( const SmallString& );
        inline SmallString( const char *       );
        inline SmallString( double             );
        inline SmallString( long               );
		inline SmallString( unsigned long      );
        inline SmallString( int                );
		inline SmallString( unsigned int 	   );
        inline SmallString( char               );
        inline ~SmallString();

        inline void        Empty();
        inline const char *GetStr() const;
        inline void        Add( const SmallString& );
        inline void        Add( const char *       );
        inline void        Add( double             );
        inline void        Add( long               );
		inline void        Add( unsigned long	   );
        inline void        Add( int                );
		inline void        Add( unsigned int	   );
        inline void        Add( char               );

        inline SmallString& operator= ( const SmallString& );
        inline SmallString& operator= ( const char *       );
		
        inline SmallString& operator+=( const SmallString& );
        inline SmallString& operator+=( const char *       );
        inline SmallString& operator+=( double             );
        inline SmallString& operator+=( long               );
		inline SmallString& operator+=( unsigned long      );
        inline SmallString& operator+=( int                );
		inline SmallString& operator+=( unsigned int       );
        inline SmallString& operator+=( char               );
		
        inline operator const char *()   const { return GetStr();       }
        inline long     getLength()      const { return m_StringLength; }
        inline long     getLongValue()   const;
        inline double   getDoubleValue() const;

        inline void     RemoveWhitespace();
        inline void     ChangeIntoPrintableOnly();
};

inline SmallString operator+(const SmallString&, const SmallString& );
inline SmallString operator+(const char* c, const SmallString& s);
inline SmallString operator+(const SmallString& s, const char* c);
inline SmallString operator+(double d, const SmallString& s);
inline SmallString operator+(const SmallString& s, double d);
inline SmallString operator+(long l, const SmallString& s);
inline SmallString operator+(const SmallString& s, long l);
inline SmallString operator+(int i, const SmallString& s);
inline SmallString operator+(const SmallString& s, int i);
inline SmallString operator+(unsigned int i, const SmallString& s);
inline SmallString operator+(const SmallString& s, unsigned int i);
inline SmallString operator+(char c, const SmallString& s);
inline SmallString operator+(const SmallString& s, char c);

inline SmallString::SmallString() {
    m_IncreaseBySize = 127;

    m_Start = new char[ m_IncreaseBySize + 1 ];
    *m_Start = '\0';
 
    m_StringLength = 0L;
    m_TotalLength = m_IncreaseBySize;
}

inline SmallString::SmallString( const SmallString& s ) {
    m_IncreaseBySize = 127;

    m_Start = new char[ m_IncreaseBySize + 1 ];
    *m_Start = '\0';
 
    m_StringLength = 0L;
    m_TotalLength = m_IncreaseBySize;

    Add( s.GetStr() );
}
    
inline SmallString::SmallString( const char *s ) {
    m_IncreaseBySize = 127;

    m_Start = new char[ m_IncreaseBySize + 1 ];
    *m_Start = '\0';
 
    m_StringLength = 0L;
    m_TotalLength = m_IncreaseBySize;

    if ( s != NULL ) {
       Add( s );
    }
}

inline SmallString::SmallString( double d ) {
    m_IncreaseBySize = 127;

    m_Start = new char[ m_IncreaseBySize + 1 ];
    *m_Start = '\0';
 
    m_StringLength = 0L;
    m_TotalLength = m_IncreaseBySize;

    Add( d );
}

inline SmallString::SmallString( long l ) {
    m_IncreaseBySize = 127;

    m_Start = new char[ m_IncreaseBySize + 1 ];
    *m_Start = '\0';
 
    m_StringLength = 0L;
    m_TotalLength = m_IncreaseBySize;

    Add( l );
}

inline SmallString::SmallString( unsigned long l ) {
    m_IncreaseBySize = 127;

    m_Start = new char[ m_IncreaseBySize + 1 ];
    *m_Start = '\0';
 
    m_StringLength = 0L;
    m_TotalLength = m_IncreaseBySize;

    Add( l );
}

inline SmallString::SmallString( int i ) {
    m_IncreaseBySize = 127;

    m_Start = new char[ m_IncreaseBySize + 1 ];
    *m_Start = '\0';
 
    m_StringLength = 0L;
    m_TotalLength = m_IncreaseBySize;

    Add( i );
}

inline SmallString::SmallString( unsigned int i ) {
    m_IncreaseBySize = 127;

    m_Start = new char[ m_IncreaseBySize + 1 ];
    *m_Start = '\0';
 
    m_StringLength = 0L;
    m_TotalLength = m_IncreaseBySize;

    Add( i );
}

inline SmallString::SmallString( char c ) {
    m_IncreaseBySize = 127;

    m_Start = new char[ m_IncreaseBySize + 1 ];
    *m_Start = '\0';
 
    m_StringLength = 0L;
    m_TotalLength = m_IncreaseBySize;

    Add( c );
}

inline SmallString::~SmallString() {
    delete[] m_Start;
}

inline void SmallString::Write( const char *s ) {
    if ( s == NULL )
        return;

    if ( m_TotalLength <= m_StringLength + (long)strlen( s ) ) {
        // This should round the new size up to the nearest m_IncreaseBySize.
        long newSize = ( ( ( m_StringLength + strlen( s ) ) / m_IncreaseBySize ) + 1 ) *
                       m_IncreaseBySize;

        char *tempBuffer = new char[ newSize + 1 ];
        strcpy( tempBuffer, m_Start );

        // Now delete the old space.
        char *t = m_Start;
        m_Start = tempBuffer;
        delete[] t;
        m_TotalLength = newSize;
    }
    strcat( m_Start, s );
    m_StringLength += strlen( s );
}

inline void SmallString::Empty() {
    if ( m_TotalLength > m_IncreaseBySize ) {
        delete[] m_Start;
        m_Start = new char[ m_IncreaseBySize + 1 ];
        m_TotalLength = m_IncreaseBySize;
    }
    *m_Start = '\0';
    m_StringLength = 0;
}

inline const char *SmallString::GetStr() const {
    return m_Start;
}

inline void SmallString::Add( const SmallString& s ) {
    Write( s.GetStr() );
}

inline void SmallString::Add( const char *s ) {
    if ( s && *s != '\0' )
        Write( s );
}

inline void SmallString::Add( double d ) {
    char number[ 100 ];
    sprintf( number, "% 3.2f", d );
    Write( number );
}

inline void SmallString::Add( long l ) {
    char number[ 100 ];
    sprintf( number, "%ld", l );
    Write( number );
}

inline void SmallString::Add( unsigned long l ) {
    char number[ 100 ];
    sprintf( number, "%ld", l );
    Write( number );
}

inline void SmallString::Add( int i ) {
    char number[ 100 ];
    sprintf( number, "%d", i );
    Write( number );
}

inline void SmallString::Add( unsigned int i ) {
	char number[ 100 ];
    sprintf( number, "%d", i );
    Write( number );
}

inline void SmallString::Add( char ch ) {
    char temp[ 2 ];
    *temp = ch;
    temp[ 1 ] = '\0';
    Write( temp );
}

inline SmallString& SmallString::operator=( const SmallString& s ) {
    Empty(); Add( s ); return *this;
}

inline SmallString& SmallString::operator=( const char *s ) {
    Empty(); Add( s ); return *this;
}

inline SmallString& SmallString::operator+=( const SmallString& s ) {
    Add( s ); return *this;
}

inline SmallString& SmallString::operator+=( const char *s ) {
    Add( s ); return *this;
}

inline SmallString& SmallString::operator+=( double d ) {
    Add( d ); return *this;
}

inline SmallString& SmallString::operator+=( long l ) {
    Add( l ); return *this;
}

inline SmallString& SmallString::operator+=( unsigned long l ) {
    Add( l ); return *this;
}

inline SmallString& SmallString::operator+=( int i ) {
    Add( i ); return *this;
}

inline SmallString& SmallString::operator+=( unsigned int i) {
	Add( i ); return *this;
}

inline SmallString& SmallString::operator+=( char ch ) {
    Add( ch ); return *this;
}

// ---------------------------------------------------------------------------

inline SmallString operator+( const SmallString& s1, const SmallString& s2 ) {
	SmallString ans(s1);
    ans += s2;
	return ans;
}

inline SmallString operator+( const SmallString& s, const char* c ) {
	SmallString ans(s);
    ans += c;
	return ans;
}
inline SmallString operator+( const char* c, const SmallString& s ) {
	SmallString ans(c);
    ans += s;
	return ans;
}

inline SmallString operator+( const SmallString& s, double d ) {
	SmallString ans(s);
    ans += d;
	return ans;
}
inline SmallString operator+( double d, const SmallString& s ) {
	SmallString ans(d);
    ans += s;
	return ans;
}

inline SmallString operator+( const SmallString& s, long l ) {
	SmallString ans(s);
    ans += l;
	return ans;
}
inline SmallString operator+( long l, const SmallString& s ) {
	SmallString ans(l);
    ans += s;
	return ans;
}

inline SmallString operator+( const SmallString& s, int i ) {
	SmallString ans(s);
    ans += i;
	return ans;
}
inline SmallString operator+( int i, const SmallString& s ) {
	SmallString ans(i);
    ans += s;
	return ans;
}

inline SmallString operator+( const SmallString& s, unsigned int i ) {
	SmallString ans(s);
    ans += i;
	return ans;
}
inline SmallString operator+( unsigned int i, const SmallString& s ) {
	SmallString ans(i);
    ans += s;
	return ans;
}

inline SmallString operator+( const SmallString& s, char c ) {
	SmallString ans(s);
    ans += c;
	return ans;
}
inline SmallString operator+( char c, const SmallString& s ) {
	SmallString ans(c);
    ans += s;
	return ans;
}

// ---------------------------------------------------------------------------

inline long SmallString::getLongValue() const {
    char number[ 100 ];
    int i;


    for ( i = 0; 
          ( i < 100 && isdigit( m_Start[ i ] ) ) || 
          ( i == 0  && ( *m_Start == '-' || *m_Start == '+' ) ); 
          i++ )
        number[ i ] = m_Start[ i ];

    number[ i ] = '\0';

    // An error really - the *is* no number at the
    // beginning of the string!
    if ( i == 0 ||
         ( i == 1 && ( *m_Start == '-' ||
                       *m_Start == '+' ) ) )
        return 0;

    return atol( number );
}

inline double SmallString::getDoubleValue() const {
    SmallString number;
    int i;
    bool doneDot = false;

    for ( i = 0; 
          isdigit( m_Start[ i ] ) || 
          ( i == 0  && ( m_Start[ 0 ] == '-' || m_Start[ 0 ] == '+' ) ||
          ( m_Start[ i ] == '.' && !doneDot ) ); 
          i++ ) {
        if ( m_Start[ i ] == '.' ) 
            doneDot = true;

        number += m_Start[ i ];
    }

    // is the next part 'e'?
    if ( i > 0 && 
         ( m_Start[ i ] == 'e' || m_Start[ i ] == 'E' ) ) {
        SmallString exponent = &m_Start[ i + 1 ];
        long e = exponent.getLongValue();

        if ( e != 0 ) {
            number += m_Start[ i++ ];
            int j;
            for ( j = 0; 
                  isdigit( exponent[ j ] ) || 
                  ( j == 0  && ( exponent[ 0 ] == '-' || exponent[ 0 ] == '+' ) ); 
                  j++ )
                number += exponent[ j ];
        }
    }

    // An error really - the *is* no number at the
    // beginning of the string!
    if ( i == 0 ||
         ( i == 1 && ( *m_Start == '-' ||
                       *m_Start == '+' ) ) )
        return 0.0;

    return atof( number );
}

void SmallString::RemoveWhitespace() {
    char *temp = new char[ strlen( m_Start ) + 1 ];
    char *s, *t;
    for ( s = temp, t = m_Start; *t; t++ ) {
        if ( !isspace( *t ) ) {
            *s++ = *t;
        }
    }

    *s = '\0';
    Empty();
    Add( temp );

    delete[] temp;
}

void SmallString::ChangeIntoPrintableOnly() {
    // This function converts all non-printable characters 
    // to a '.' character.

    SmallString result;
    for ( const char *s = m_Start; *s; s++ ) 
        if ( isprint( *s ) )
            result += *s;
        else
            result += ".";

    Empty();
    Add( result );
} // End Method OnlyPrintable

#endif

