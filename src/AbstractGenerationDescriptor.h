/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef ABSTRACTGENERATIONDESCRIPTOR_H
#define ABSTRACTGENERATIONDESCRIPTOR_H

#include "AbstractGenotype.h"
#include "TrialInfo.h"
#include "PopulationMember.h"
#include <map>
#include <set>
#include <vector>
#include <queue>

using namespace std;

/**
 * This class allows the user to specify exactly how the
 * library chooses which genotypes to kill off and
 * reproduce.
 */
class AbstractGenerationDescriptor {
public:
  virtual ~AbstractGenerationDescriptor() { }

  virtual void getGenotypesToSendToTrial( 
      queue<TrialInfo>&                           toSendToTrial,
      const map<unsigned long,PopulationMember>&  population ) = 0;
  
  virtual void getNextGenerationInformation( 
      set<unsigned long>&                  toBeRemoved,
      queue<unsigned long>&                toBeReproduced,
      map<unsigned long,PopulationMember>& population,
      const vector<TrialInfo>&             trials ) = 0;
};

#endif
