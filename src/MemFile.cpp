/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "MemFile.h"
#include <stdlib.h>
#include <string.h>

void MemFile::makeCopy() {
  struct internalMemFile *temp = new struct internalMemFile;
  temp->memory = new unsigned char[ m_MemFileInstance->spaceAllocated ];
  memcpy( temp->memory, 
	  m_MemFileInstance->memory, 
	  m_MemFileInstance->currentSize );
  temp->currentSize = m_MemFileInstance->currentSize;
  temp->endOfMemory = &(temp->memory[ temp->currentSize ]);
  temp->noOfPointers = 1;

  m_MemFileInstance->noOfPointers--;

  m_MemFileInstance = temp;
}

void MemFile::increaseSize( int n ) {
  unsigned char *tempSpace;

  m_MemFileInstance->spaceAllocated = 
    INCREASE_BY * ( 1 + ( m_MemFileInstance->currentSize + n ) / INCREASE_BY );
  
  tempSpace = new unsigned char[ m_MemFileInstance->spaceAllocated ];
  memcpy( tempSpace, 
	  m_MemFileInstance->memory, 
	  m_MemFileInstance->currentSize );

  delete[] m_MemFileInstance->memory;
  m_MemFileInstance->memory = tempSpace;
  m_MemFileInstance->endOfMemory = &tempSpace[ m_MemFileInstance->currentSize ];
}

void MemFile::makeNewEmptyInstance() {
  m_MemFileInstance = new struct internalMemFile;
  m_MemFileInstance->memory = new unsigned char[ START_LENGTH ];
  m_MemFileInstance->endOfMemory = m_MemFileInstance->memory;
  m_MemFileInstance->currentSize = 0;
  m_MemFileInstance->spaceAllocated = START_LENGTH;

  m_MemFileInstance->noOfPointers = 1;
}

MemFile::MemFile() {
  makeNewEmptyInstance();
  m_CurrentWritingPos = 0;
}

MemFile::MemFile( const MemFile& mf ) {
  m_MemFileInstance = mf.m_MemFileInstance;
  m_MemFileInstance->noOfPointers++;
  m_CurrentWritingPos = m_MemFileInstance->currentSize;
}

MemFile::~MemFile() {
  if ( --(m_MemFileInstance->noOfPointers) == 0 ) {
    delete[] m_MemFileInstance->memory;
    delete m_MemFileInstance;
  }
}

MemFile& MemFile::operator=( const MemFile& mf ) {
  makeEmpty();
  append( mf );

  m_CurrentWritingPos = m_MemFileInstance->currentSize;

  return *this;
}

void MemFile::append( const unsigned char *b, unsigned long n ) {
  if ( m_MemFileInstance->noOfPointers > 1 ) {
    makeCopy();
  }

  if ( m_MemFileInstance->currentSize + n > m_MemFileInstance->spaceAllocated ) {
    increaseSize( n );
  }

  memcpy( m_MemFileInstance->endOfMemory, b, n );
  m_MemFileInstance->endOfMemory += n;
  m_MemFileInstance->currentSize += n;
  m_CurrentWritingPos = m_MemFileInstance->currentSize;
}

void MemFile::append( const MemFile& mf ) {
  unsigned long n;
  const unsigned char *b = mf.getMemFile( &n );

  append( b, n );
}

int MemFile::getSize() const {
  return m_MemFileInstance->currentSize;
}

const unsigned char *MemFile::getMemFile( unsigned long *size ) const {
  *size = m_MemFileInstance->currentSize;
  return m_MemFileInstance->memory;
}

void MemFile::makeEmpty() {
  if ( m_MemFileInstance->noOfPointers > 1 ) {
    m_MemFileInstance->noOfPointers--;
    makeNewEmptyInstance();
  } else {
    if ( m_MemFileInstance->spaceAllocated > SHRINK_IF ) {
      delete[] m_MemFileInstance->memory;
      delete m_MemFileInstance;

      makeNewEmptyInstance();
    } else {
      m_MemFileInstance->currentSize = 0;
      m_MemFileInstance->endOfMemory = m_MemFileInstance->memory;
    }
  }
  m_CurrentWritingPos = 0;
}

void MemFile::write( const unsigned char *bytes,
		     unsigned long        noOfBytes ) {
  /**
   * This functions write as to a file i.e. it has a position
   * and can over write the stuff that's already there.
   */ 

  if ( m_CurrentWritingPos == m_MemFileInstance->currentSize ) {
    append( bytes, noOfBytes );
  } else if ( noOfBytes + m_CurrentWritingPos <=
	      m_MemFileInstance->currentSize ) {
    memcpy( (void *)&(m_MemFileInstance->memory[ m_CurrentWritingPos ]),
	    bytes,
	    noOfBytes );
    m_CurrentWritingPos += noOfBytes;
  } else {
    /**
     * We need to make sure the memfile is of the right length...
     */
    unsigned long tempPos = m_CurrentWritingPos;
    unsigned char *tempArea = 
      new unsigned char[ noOfBytes + m_CurrentWritingPos -
			 m_MemFileInstance->currentSize ];
    append( tempArea, 
	    noOfBytes + m_CurrentWritingPos -
	    m_MemFileInstance->currentSize );
 
    delete[] tempArea;

    m_CurrentWritingPos = tempPos;

    memcpy( (void *)&(m_MemFileInstance->memory[ m_CurrentWritingPos ]),
	    bytes,
	    noOfBytes );
    m_CurrentWritingPos += noOfBytes;
  }
}

void MemFile::seek( unsigned long pos ) {
  if ( pos > m_MemFileInstance->currentSize ) {
    char errMsg[ 1024 ];
    sprintf( errMsg, "Invalid position %lu as size is only %lu",
	     pos, m_CurrentWritingPos );
    
    MemFileException e( errMsg );
    e.fillInStackTrace();
    throw e;
  }
  
  m_CurrentWritingPos = pos;
}

unsigned long MemFile::tell() {
  return m_CurrentWritingPos;
}

int MemFile::mgetc() {
  /**
   * Returns the character being pointed at.
   */
  if ( m_CurrentWritingPos >= m_MemFileInstance->currentSize ) {
    return EOF;
  }

  return m_MemFileInstance->memory[ m_CurrentWritingPos++ ];
}

void MemFile::mungetc( int ch ) {
  if ( m_CurrentWritingPos >= m_MemFileInstance->currentSize ) {
    return;
  }

  if ( ch != EOF ) {
    m_MemFileInstance->memory[ --m_CurrentWritingPos ] = (unsigned char)ch;
  }
}
