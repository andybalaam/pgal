/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef QCONFIGFILE_H
#define QCONFIGFILE_H

/**
 * This class represents a simple generic config file. It is designed
 * to be subclassed by the individual config file class. The an example
 * of a config file is:
 *
 * # This is commented out.
 * VariableName1=A string value
 * VariableName2    =   4.56
 * 
 * # Another comment
 */

#include <string>
#include <vector>
#include <stdio.h>
#include "SmallString.h"
#include "QException.h"
#include "MemFile.h"

using namespace std;

class QConfigFile {
public:
  QEXCEPTIONCLASS( QConfigFileException );
  QEXCEPTIONDERIVEDCLASS( InvalidFilename, QConfigFileException );
  QEXCEPTIONDERIVEDCLASS( SyntaxErrorException, QConfigFileException );

protected:
  enum { TOKEN_EQUALS = 10000,
	 TOKEN_STRING,
	 TOKEN_END };

  struct S_pairing {
    SmallString name;
    SmallString value;
  };

  vector<struct S_pairing>  m_NamesToValues;
  SmallString               m_Filename;
  const char              **m_ValidNames;
  SmallString               m_BeginToken;
  SmallString               m_EndToken;

  QConfigFile( const char  *filename,
	       const char **validNames,
	       const char  *beginToken,
	       const char  *endToken );

  int  getToken( MemFile& input, SmallString& );
};

#endif
