#ifndef POPULATIONMEMBER_H
#define POPULATIONMEMBER_H

/**
 * This class represents an individual member within the
 * population. Remember that it deals with deleting the
 * AbstractGenotype instance so you don't have to once
 * you've added it.
 */
 
#include <vector>
using namespace std;
 
 
#include "AbstractGenotype.h"

class PopulationMember {
public:
  static const unsigned char KNOWN_FITNESS;
  static const unsigned char UNKNOWN_FITNESS;
  static const unsigned char INVALID_GENOTYPE;

  PopulationMember( unsigned long     ID,
		    AbstractGenotype *g,
		    unsigned char     state );
  PopulationMember( const PopulationMember& );
  virtual ~PopulationMember();

  void setState(   unsigned char   s )          { m_State     = s;     }
  void setFitness( FLOAT           f )          { m_Fitness   = f;     }
  void setFitnesses( vector<FLOAT> v )          { m_Fitnesses = v;     }

  unsigned long           getID()         const { return m_ID;         }
  unsigned char           getState()      const { return m_State;      }
  FLOAT                   getFitness()    const { return m_Fitness;    }
  vector<FLOAT>           getFitnesses()  const { return m_Fitnesses;  }

  const AbstractGenotype *getGenotype() const { 
    return m_CommonStructurePointer->m_Genotype; 
  }
 
  PopulationMember& operator=( const PopulationMember& );

  bool operator<(  const PopulationMember& p ) const { return m_ID < p.m_ID; }
  bool operator>(  const PopulationMember& p ) const { return m_ID > p.m_ID; }
  bool operator==( const PopulationMember& p ) const { 
    return m_ID == p.m_ID;
  }
  bool operator!=( const PopulationMember& p ) const { 
    return m_ID != p.m_ID;
  }

protected:
  struct S_counterStruct {
    int               m_NoOfPointers;
    AbstractGenotype *m_Genotype;
  } *m_CommonStructurePointer;

  unsigned long m_ID;
  unsigned char m_State;
  FLOAT         m_Fitness;
  vector<FLOAT> m_Fitnesses;
};

#endif
