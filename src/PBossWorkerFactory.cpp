#include "PBossWorkerFactory.h"
#include "PSingleBossWorker.h"
#include "PSimpleThreadsBossWorker.h"
#include "PProcessBossWorker.h"
#include <stdio.h>

PBossWorker *
PBossWorkerFactory::createBossWorker( int noOfWorkers ) {
  if ( noOfWorkers == 0 ) {
    return new PSingleBossWorker;
  } else {
    //return new PSimpleThreadsBossWorker( noOfWorkers );
    return new PProcessBossWorker( noOfWorkers, 512*1024 );
  }

  /*char errMsg[ 1024 ];
  sprintf( errMsg, "Cannot build BossWorker with %d workers", noOfWorkers );
      
  InvalidNoOfWorkers e( errMsg );
  e.fillInStackTrace();
  throw e;*/
}
