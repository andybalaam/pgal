/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "EvolutionaryRun.h"
#include "RandomNumberGenerator.h"
#include "QErrorLog.h"
#include "PResultSet.h"
#include "bitfield.h"
#include "CreateGenotypeWorker.h"
#include "PBossWorkerFactory.h"
#include "PBossWorker.h"
#include "RunTrialWorker.h"
#include "MutateGenotypeWorker.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include "SmallString.h"
#include <map>
#include <vector>
#include <set>
#include <algorithm>
#include "PopulationMember.h"

using namespace std;

const char *EvolutionaryRun::PROGRAM_VERSION = "genotypeLib " __DATE__ " I.M.";

static sig_atomic_t exitFlag = 0;

void EvolutionaryRun::exitProgram( int sigNum ) {
  exitFlag = 1;
}

void EvolutionaryRun::checkForExit() {
  if ( exitFlag != 0 ||
       ( m_MaxNoOfGenerations != 0 &&
         m_Generation > m_MaxNoOfGenerations ) ) {
    ExitProgram e( "Finished program" );
    e.fillInStackTrace();
    throw e;
  }
}

/**
 * Creates an evolutionaryRun record in the database.
 */
void EvolutionaryRun::createEvolutionaryRunRecord() {
  try {
    if ( m_GenotypeDatabase == NULL ) {
      EvolutionaryRunException e( "Haven't got connection to "
				  "genotype database" );
      e.fillInStackTrace();
      throw e;
    }
    
    m_EvolutionaryRun = m_GenotypeDatabase->createEvolutionaryRunRecord(
        m_InitialPopulationSize,
	PROGRAM_VERSION
    );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

/**
 * Creates an evolutionaryPartRun record in the database.
 */
void 
EvolutionaryRun::createEvolutionaryPartRunRecord( const char *notes ) {
  try {
    struct sigaction sa;

    if ( m_GenotypeDatabase == NULL ) {
      EvolutionaryRunException e( "Haven't got connection to "
				  "genotype database" );
      e.fillInStackTrace();
      throw e;
    }

    m_EvolutionaryPartRun =
      m_GenotypeDatabase->createEvolutionaryPartRunRecord( m_EvolutionaryRun, 
							   notes );

    memset( &sa, 0, sizeof( sa ) );
    sa.sa_handler = &exitProgram;
    sigaction( SIGTERM, &sa, NULL );
    sigaction( SIGINT,  &sa, NULL );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

/**
 * Creates a trial record in the database.
 */
unsigned long 
EvolutionaryRun::createTrialRecord( const TrialInfo& trialInfo ) {
  try {
    if ( m_GenotypeDatabase == NULL ) {
      EvolutionaryRunException e( "Haven't got connection to "
				  "genotype database" );
      e.fillInStackTrace();
      throw e;
    }

    return m_GenotypeDatabase->createTrialRecord( trialInfo,
						  m_GenerationIndexNo );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

/**
 * Creates an evolutionaryRun record in the database.
 */
unsigned long 
EvolutionaryRun::createGenerationRecord( unsigned long generation ) {
  try {
    if ( m_GenotypeDatabase == NULL ) {
      EvolutionaryRunException e( "Haven't got connection to "
                                  "genotype database" );
      e.fillInStackTrace();
      throw e;
    }
    
    return m_GenotypeDatabase->createGenerationRecord( generation,
						       m_EvolutionaryPartRun );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }  
}

/**
 * This loads in the last population of the given evolutionary
 * run from the database. It does this in two steps - first, a
 * list is retrieved containing all the index numbers of the
 * genotypes in the last population, then each genotype is seperately
 * loaded. The database functions may throw an exception, so this
 * should be tested for.
 */
void EvolutionaryRun::loadLastPopulation() {
  try {
    unsigned long i, tempNum;
    
    if ( m_GenotypeDatabase == NULL ) {
      EvolutionaryRunException e( "Haven't got connection to "
				  "genotype database" );
      e.fillInStackTrace();
      throw e;
    }
    
    /**
     * First we have to empty the current population...
     */
    m_Population.clear();
    
    /**
     * Then load in the new one...
     */
    m_GenotypeDatabase->loadLatestGeneration( m_EvolutionaryRun,
					      m_GenotypeFactory,
					      &m_LatestGeneration,
					      &m_Population );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  } 
}

/**
 * All this really does is set up a few default variables. Nothing
 * interesting is done here because I don't want to risk throwing
 * an exception in the constructor. The main initialisation (e.g.
 * connecting to the database, etc.) is done in the 'start()' method.
 */
EvolutionaryRun::EvolutionaryRun( unsigned long            initialPopSize,
                                  AbstractGenotypeFactory *genotypeFactory,
                                  AbstractGenerationDescriptor *generation,
                                  AbstractTrialFactory    *trialFactory,
				  int                      maxNoOfProcesses,
				  const char              *databaseUsername,
				  const char              *databasePassword,
				  const char              *hostname,
				  const char              *databaseName,
				  long                     maxNoOfGenerations,
				  GenotypeDatabase        *defaultDatabase ) :
  m_InitialPopulationSize(     initialPopSize      ),
  m_MaxNoOfProcesses(          maxNoOfProcesses    ),
  m_GenotypeDatabase(          NULL                ),
  m_GenerationDescriptor(      generation          ),
  m_GenotypeFactory(           genotypeFactory     ),
  m_TrialFactory(              trialFactory        ),
  m_EvolutionaryRun(           LONG_MAX            ),
  m_EvolutionaryPartRun(       0                   ),
  m_FinishEvolutionaryPartRun( true                ),
  m_DatabaseUsername(          databaseUsername    ),
  m_DatabasePassword(          databasePassword    ),
  m_Hostname(                  hostname            ),
  m_DatabaseName(              databaseName        ),
  m_NoOfNewGenotypes(          0                   ),
  m_MaxNoOfGenerations(        maxNoOfGenerations  ),
  m_Generation(                0                   ),
  m_LatestGeneration(          1                   ),
  m_StartGenerationInterface(  NULL                ),
  m_EndGenerationInterface(    NULL                ),
  m_TimePeriod(                10*60               ),
  m_GenerationsCounter(        0                   ),
  m_WriteGenotypeBodyEvery(    1                   ),
  m_NoOfPopulations(           1                   ),
  m_UseDefaultDatabase(        false               ) {
  m_RandomNumberGenerator = RandomNumberGenerator::getDefaultGoodGenerator();
  m_TimeStamp = time( NULL );
  m_BossWorker = PBossWorkerFactory::createBossWorker( m_MaxNoOfProcesses );
  if ( defaultDatabase != NULL ) {
    m_GenotypeDatabase = defaultDatabase;
    m_UseDefaultDatabase = true;
  }
}

EvolutionaryRun::EvolutionaryRun( unsigned long            evolutionaryRun,
                                  unsigned long            initialPopSize,
                                  AbstractGenotypeFactory *genotypeFactory,
                                  AbstractGenerationDescriptor *generation,
                                  AbstractTrialFactory    *trialFactory,
                                  int                      maxNoOfProcesses,
				  const char              *databaseUsername,
				  const char              *databasePassword,
				  const char              *hostname,
				  const char              *databaseName,
				  long                     maxNoOfGenerations,
				  GenotypeDatabase        *defaultDatabase ) :
  m_InitialPopulationSize(     initialPopSize      ),
  m_MaxNoOfProcesses(          maxNoOfProcesses    ),
  m_GenotypeDatabase(          NULL                ),
  m_GenerationDescriptor(      generation          ),
  m_GenotypeFactory(           genotypeFactory     ),
  m_TrialFactory(              trialFactory        ),
  m_EvolutionaryRun(           evolutionaryRun     ),
  m_EvolutionaryPartRun(       0                   ),
  m_FinishEvolutionaryPartRun( true                ),
  m_DatabaseUsername(          databaseUsername    ),
  m_DatabasePassword(          databasePassword    ),
  m_Hostname(                  hostname            ),
  m_DatabaseName(              databaseName        ),
  m_NoOfNewGenotypes(          0                   ),
  m_MaxNoOfGenerations(        maxNoOfGenerations  ),
  m_Generation(                0                   ),
  m_LatestGeneration(          1                   ),
  m_StartGenerationInterface(  NULL                ),
  m_EndGenerationInterface(    NULL                ),
  m_TimePeriod(                10*60               ),
  m_GenerationsCounter(        0                   ),
  m_WriteGenotypeBodyEvery(    1                   ),
  m_NoOfPopulations(           1                   ),
  m_Compressor(                NULL                ),
  m_UseDefaultDatabase(        false               ) {
  m_RandomNumberGenerator = RandomNumberGenerator::getDefaultGoodGenerator();
  m_TimeStamp = time( NULL );
  m_BossWorker = PBossWorkerFactory::createBossWorker( m_MaxNoOfProcesses );
  if ( defaultDatabase != NULL ) {
    m_GenotypeDatabase = defaultDatabase;
    m_UseDefaultDatabase = true;
  }
}

EvolutionaryRun::~EvolutionaryRun() {
  if ( m_FinishEvolutionaryPartRun == true ) {
    m_GenotypeDatabase->finishEvolutionaryPartRun( m_EvolutionaryPartRun );
  }

  if ( m_UseDefaultDatabase == false ) {
    delete m_GenotypeDatabase;
  }

  delete m_BossWorker;

  exitFlag = 0;
}

/**
 * This is the main bulk of the class. It initialises the
 * database connection, creates and sets up the starting
 * population, etc. Any database or evolutionaryRun exceptions
 * will originate from within here.
 */
void EvolutionaryRun::start( const char *notes ) {
  /**
   * First, we'll try to connect to the database. It will
   * throw it's own exception if there's a problem.
   */
  if ( m_GenotypeDatabase == NULL ) {
    m_GenotypeDatabase = new GenotypeDatabase( m_DatabaseUsername,
					       m_DatabasePassword,
					       m_Hostname,
					       m_DatabaseName );
  }

  /**
   * Now, we'll have to work out what to do. There are two paths
   * we could go down - either continue an existing run, or
   * start a new one.
   */
  if ( m_EvolutionaryRun == LONG_MAX ) {
    /**
     * OK - we start from scratch...
     */
    createInitialPopulation( notes );
  } else {
    loadPreviousPopulation( notes );
  }

  runEvolutionaryRun();
}

void EvolutionaryRun::createInitialPopulation(const char *notes) {
  unsigned long i;

  createEvolutionaryRunRecord();
  createEvolutionaryPartRunRecord( notes );
  createGenerationRecord( 0 );

  /**
   * In this method, we create a number of genotypes, add
   * them to the database as having unknown fitnesses.
   * We also need to add a generation record.
   */
  
  /**
   * Now we'll read all the threads as they finish.
   */
  while ( m_Population.size() < m_InitialPopulationSize ) {
    /**
     * If the number left to process is less than or equal
     * to the number of active processes, then we know these 
     * last ones are currently being processes and so we have 
     * no more to set off and we should wait for everything to 
     * finish.
     */
    CreateGenotypeWorker *finishedWorker = 
      new CreateGenotypeWorker( m_GenotypeFactory );

    finishedWorker->start();

    if ( finishedWorker != NULL ) {
      unsigned long genotypeNo = 
	m_GenotypeDatabase->addGenotype( finishedWorker->m_Genotype, 
					 m_EvolutionaryPartRun,
					 0 );
      
      finishedWorker->m_Genotype->setIndexNo( genotypeNo );

      PopulationMember member( genotypeNo, 
			       finishedWorker->m_Genotype, 
			       PopulationMember::UNKNOWN_FITNESS );
      m_Population.insert(
          pair<unsigned long,PopulationMember>( genotypeNo, member ) );
      
      delete finishedWorker;
    } // End if finished worker
  }
}

/**
 * This method is called if a previous population has to be loaded from
 * the database.
 */
void EvolutionaryRun::loadPreviousPopulation(const char *notes) {
  int i, sizeInBits, tempNum;
  const unsigned char *bytes;

  createEvolutionaryPartRunRecord( notes );
  loadLastPopulation();
}

void EvolutionaryRun::performTrials( queue<TrialInfo>&  toSendToTrial,
				     vector<TrialInfo>& processed ) {
  try {
    multiset<TrialInfo> beingProcessed;
    bool                forceWait = false;
    int                 j;
    unsigned long       genotypeID;
    FLOAT               fitness;
    unsigned char       state;
    int                 typeNo;

    int totalToSendToTrial = toSendToTrial.size();

    /**
     * We'll set all the threads to run and then read them as they
     * finish.
     */
    while ( toSendToTrial.empty() == false ) {
      TrialInfo temp = toSendToTrial.front();
      beingProcessed.insert( temp );
      toSendToTrial.pop();
      
      vector<AbstractGenotype*> genotypes;
      
      for ( j = 0; j < temp.getNoOfGenotypes(); j++ ) {
	temp.getGenotypeInfo( j,
			      &genotypeID,
			      &fitness,
			      &state,
			      &typeNo );
	
	map<unsigned long,PopulationMember>::iterator g;
	g = m_Population.find( genotypeID );
	if ( g == m_Population.end() ) {
	  char errMsg[ 1024 ];
	  sprintf( errMsg, "Cannot find genotype in population (%lu)",
		   genotypeID );
	  
	  InvalidGenotype e( errMsg );
	  e.fillInStackTrace();
	  throw e;
	}
	
	genotypes.push_back( const_cast<AbstractGenotype*>( 
	    g->second.getGenotype() ) );
      }
       
      RunTrialWorker *newWorker = 
	new RunTrialWorker( m_TrialFactory,
			    temp,
			    genotypes,
			    m_Generation,
			    temp.getExtraDataAsMemFile() );
      
      m_BossWorker->run( newWorker );
    }

    /**
     * Now we'll wait for them all to finish.
     */
    while ( processed.size() < totalToSendToTrial ) {
      RunTrialWorker *finishedWorker = dynamic_cast<RunTrialWorker*>( 
          m_BossWorker->waitForProcessToFinish( true ) );
      
      checkForExit();
      
      if ( finishedWorker != NULL ) {
	multiset<TrialInfo>::iterator i;
	i = beingProcessed.find( finishedWorker->m_TrialInfo );
	if ( i == beingProcessed.end() ) {
	  SmallString errMsg;
	  errMsg += "Found fitness for trial \' ";
	  finishedWorker->m_TrialInfo.toString( errMsg );
	  errMsg += "\' I have not asked for! "
	            "(programming error)";
	  
	  finishedWorker->m_TrialInfo.toString( errMsg );
	  
	  EvolutionaryRunException e( errMsg );
	  e.fillInStackTrace();
	  throw e;
	}
	
	beingProcessed.erase( i );
	processed.push_back( finishedWorker->m_TrialInfo );
	
	/**
	 * If the trial is invalid, we have to set all the genotypes
	 * within that trial to invalid to.
	 */
	map<unsigned long,PopulationMember>::iterator k;
	if ( finishedWorker->m_TrialInfo.getTrialState() == 
	     TrialInfo::INVALID_TRIAL ) {
	  for ( j = 0; 
		j < finishedWorker->m_TrialInfo.getNoOfGenotypes(); 
		j++ ) {
	    finishedWorker->m_TrialInfo.getGenotypeInfo( j,
							 &genotypeID,
							 &fitness,
							 &state,
							 &typeNo );
	    
	    k = m_Population.find( genotypeID );
	    if ( k == m_Population.end() ) {
	      char errMsg[ 1024 ];
	      sprintf( errMsg, "Got a genotype fitness back (%lu) "
		       "that I didn't know about", 
		       genotypeID );
	      
	      EvolutionaryRunException e( errMsg );
	      e.fillInStackTrace();
	      throw e;
	    }
	    
	    k->second.setState( PopulationMember::INVALID_GENOTYPE );
	  }
	} else { 
	  for ( j = 0; 
		j < finishedWorker->m_TrialInfo.getNoOfGenotypes(); 
		j++ ) {
	    finishedWorker->m_TrialInfo.getGenotypeInfo( j,
							 &genotypeID,
							 &fitness,
							 &state,
							 &typeNo );
	    
	    k = m_Population.find( genotypeID );
	    if ( k == m_Population.end() ) {
	      char errMsg[ 1024 ];
	      sprintf( errMsg, "Got a genotype fitness back (%lu) "
		       "that I didn't know about", 
		       genotypeID );
	      
	      EvolutionaryRunException e( errMsg );
	      e.fillInStackTrace();
	      throw e;
	    }
	    
	    k->second.setState( state );
	  }
	} // End if
	
	delete finishedWorker;
      }

      checkForExit();
    }
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

void 
EvolutionaryRun::reproducePopulation( queue<unsigned long>& toBeReproduced ) {
  try {
    /**
     * We must do the reproduction. And then we do all the database 
     * stuff all at once, hopefully as an atomic task. We have to 
     * make sure that we don't save any of the genotypes that are to 
     * be removed. Then we actually removed these genotypes.
     */

    int totalToReproduce = toBeReproduced.size();

    bool forceWait;
    while ( toBeReproduced.empty() == false ) {
      unsigned long j = toBeReproduced.front();
      toBeReproduced.pop();
      
      map<unsigned long,PopulationMember>::iterator k;
      k = m_Population.find( j );
      if ( k == m_Population.end() ) {
	SmallString errMsg;
	errMsg += "Asked to reproduce genotype ";
	errMsg += j;
	errMsg += ", but cannot find it in my population";
	
	EvolutionaryRunException e( errMsg );
	e.fillInStackTrace();
	throw e;
      }
      
      MutateGenotypeWorker *newWorker =
	new MutateGenotypeWorker( m_GenotypeFactory,
				  k->second.getGenotype() );
     
      newWorker->start(); 
      
      if ( newWorker != NULL ) {
	/**
	 * Now we add this to the database...
	 */
	unsigned long newGenotypeNo = m_GenotypeDatabase->addGenotype( 
	    newWorker->m_NewGenotype,
	    m_EvolutionaryPartRun,
	    m_Generation, 
	    newWorker->m_Genotype->getIndexNo(),
	    m_WriteGenotypeBodyEvery == 1 ?
	    true : false );

	newWorker->m_NewGenotype->setIndexNo( newGenotypeNo );
      
	/*{ // DEBUG block
	  SmallString msg;
	  msg += "Parent of ";
	  msg += newGenotypeNo;
	  msg += " is ";
	  msg += k->second->getIndexNo();
	  fprintf( stderr, "%s\n", (const char *)msg );
	  }*/
	
	PopulationMember member( newGenotypeNo,
				 newWorker->m_NewGenotype,
				 PopulationMember::UNKNOWN_FITNESS );
	
	m_Population.insert( pair<unsigned long,PopulationMember>
			     ( newGenotypeNo, member ) );
	
	delete newWorker;

	totalToReproduce--;
      } // End if finishedWorker != NULL
    }
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

void EvolutionaryRun::runEvolutionaryRun() {
  int i;

  /**
   * This goes around in a never-ending loop. It given a
   * list of the genotypes to the user, which returns
   * which genotypes should be tested. Then these are
   * set off to be processed by the different processes.
   * Then once the fitnesses of the desired ones have
   * been processed, the user code is asked which ones
   * to remove and mutate. All the others are kept in
   * the population. If the number to remove is the same
   * as the number to mutate, then the population stays
   * steady. Note that the number to mutate may contain
   * the same genotype more than once. However, the 
   * number to remove may only contain a given genotype
   * once. Note also that a genotype may be on both
   * lists i.e. it may be reproduced and removed.
   */

  queue<TrialInfo>     toSendToTrial;
  vector<TrialInfo>    processed;
  set<unsigned long>   toBeRemoved;
  queue<unsigned long> toBeReproduced;

  try {
    for ( m_Generation = m_LatestGeneration; ; m_Generation++ ) {
      /**
       * First we run a user function if required...
       */
      if ( m_StartGenerationInterface != NULL ) {
	m_StartGenerationInterface->startGeneration( m_Generation );
      }
      
      /**
       * Now we build a list of the population.
       */
      m_GenerationDescriptor->getGenotypesToSendToTrial( toSendToTrial,
							 m_Population );

      checkForExit();

      performTrials( toSendToTrial, processed );

      /**
       * Now, we have a list of trials that have occurred. Now we have
       * to convert all those trials into the next generation. This
       * is complicated because:
       *
       * 1) A genotype may exist in more than one trial
       * 2) A genotype may not exist at all in any trial
       * 3) A genotype may be marked as invalid in one
       *    trial and OK in another
       * 4) A genotype may exist within an invalid trial
       *
       * However, this isn't our problem! It has to be dealt with by
       * the generation descriptor.
       *
       * Actually, the fitness value in PopulationMember is only for
       * writing to the database for statistical purposes and not for
       * actually calculating anything. The values can be the same
       * of course. You only need to set returned fitnesses if you
       * want the database values the same.
       */
      map<unsigned long,FLOAT> returnedFitnesses;
      m_GenerationDescriptor->getNextGenerationInformation( toBeRemoved,
							    toBeReproduced,
							    m_Population,
							    processed );
      checkForExit();

      /****************************************************************
       * All database write-access occurs in this critical section...
       ****************************************************************/

      /**
       * Add the generation record...
       */
      m_GenerationIndexNo = 
	createGenerationRecord( m_Generation );

      /**
       * Now we add all the trial record data...
       */
      vector<TrialInfo>::iterator u;
      for ( u = processed.begin();
	    u != processed.end();
	    u++ ) {
	createTrialRecord( *u );
      }

      calculateQuartileData();
      
      reproducePopulation( toBeReproduced );

      /**
       * Right, now we remove all the ones we're supposed to...
       */
      set<unsigned long>::iterator q;
      for ( q = toBeRemoved.begin(); q != toBeRemoved.end(); q++ ) {
	m_GenotypeDatabase->setLastGenerationForGenotype( *q,
							  m_Generation );
	
	map<unsigned long,PopulationMember>::iterator m;
	m = m_Population.find( *q );
	if ( m == m_Population.end() ) {
	  SmallString errMsg;
	  errMsg += "Asked to remove a genotype ";
	  errMsg += *q;
	  errMsg += " that I don't have in my population";
	  
	  EvolutionaryRunException e( errMsg );
	  e.fillInStackTrace();
	  throw e;
	}
	m_Population.erase( m );
      }
      
      /**
       * Now we add to the time period information table...
       */
      m_GenerationsCounter++;
      unsigned long currentTime = time( NULL );
      
      if ( currentTime > m_TimeStamp + m_TimePeriod ) {
	m_GenotypeDatabase->createTimePeriodInfoRecord( 
	    m_EvolutionaryPartRun,
	    currentTime - m_TimeStamp,
	    m_GenerationsCounter );
      
	m_TimeStamp = currentTime;
	m_GenerationsCounter = 0;
      }

      if ( m_Generation % m_WriteGenotypeBodyEvery == 0 ) {
	makeSureCurrentGenotypeRecordsHaveBody();
      }

      /**
       * Now run any user function if required...
       */
      if ( m_EndGenerationInterface != NULL ) {
	m_EndGenerationInterface->endGeneration( m_Generation );
      }

      /********************************************************
       * End of database critical section.
       ********************************************************/
      
      /**
       * Now to clear up for the next generation...
       */
      
      checkForExit();
      
      toBeRemoved.clear();
      processed.clear();
    }
  } catch( ExitProgram& e ) {
    e.fillInStackTrace();

    makeSureCurrentGenotypeRecordsHaveBody();

    throw;
  } catch( TooManyErrorsException& e ) {
    makeSureCurrentGenotypeRecordsHaveBody();
  }
}

void EvolutionaryRun::calculateQuartileData() {
  try {
    FLOAT quartiles[ 10 ];
    
    // First do the databaseFitness
    calculateSubPopulationQuartileData( 0, -1, quartiles );
    calculateSubPopulationQuartileData( 1, -1, &( quartiles[ 5 ] ) );

    m_GenotypeDatabase->addQuartileData( m_GenerationIndexNo, quartiles );
    
    // And then do all of the fitnesses
    for( int i = 0; ; i++ ) {    
      if( (!calculateSubPopulationQuartileData( 0, i, quartiles ))
          || (!calculateSubPopulationQuartileData( 1, i, &( quartiles[ 5 ] ))))
        {
          break;
        }
     
      m_GenotypeDatabase->addExtraQuartileData( m_GenerationIndexNo, i,
						quartiles );
    }
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

/**
 * Given a genotypeTypeID (0 or 1 for the first or second genotype type), and
 * a fitnessNo (-1 for the databaseFitness, and >=0 for one of the extra ones),
 * and a pointer to an array quartiles of size 10 of FLOATS (floats or doubles),
 * this method fills the array with the quartiles of the fitness scores of the
 * individuals stored in m_Population.  If fitnessNo is larger than the vector
 * of fitnesses stored in any of the individuals, a return value of FALSE is
 * immediately returned, and the contents of quartiles is undefined.
 */
bool 
EvolutionaryRun::calculateSubPopulationQuartileData( int    genotypeTypeID,
                             int    fitnessNo,
						     FLOAT *quartiles ) {
  try {
    /**
     * Lets calculate the quartiles first. We have to do this twice for
     * both subpopulations.
     */
    quartiles[ 0 ] = 0;
    quartiles[ 1 ] = 0;
    quartiles[ 2 ] = 0;
    quartiles[ 3 ] = 0;
    quartiles[ 4 ] = 0;

    vector<FitnessUnit> allFitnesses;
    map<unsigned long,PopulationMember>::const_iterator i;

    for ( i = m_Population.begin(); i != m_Population.end(); i++ ) { 
      if ( i->second.getGenotype()->getType() == genotypeTypeID ) {
	FitnessUnit tempUnit;
	if ( i->second.getState() == PopulationMember::INVALID_GENOTYPE ) {
	  tempUnit.fitness = 0;
	} else if( fitnessNo == -1 ) {
	  tempUnit.fitness = i->second.getFitness();
	} else {
      vector<FLOAT> fitnesses = i->second.getFitnesses();
      if( fitnessNo >= fitnesses.size() ) {
        return FALSE;
      }
      tempUnit.fitness = fitnesses[fitnessNo];
    }
	tempUnit.ID = i->first;
	
	allFitnesses.push_back( tempUnit );
      }
    }

    if ( allFitnesses.size() > 0 ) {
      sort( allFitnesses.begin(), allFitnesses.end() );
     
      int popSize = allFitnesses.size();
   
      quartiles[ 0 ] = 
	allFitnesses[ popSize - 1 ].fitness;
      
      quartiles[ 1 ] = 
	allFitnesses[ (int)( (FLOAT)popSize * 3.0 / 4.0 )].fitness;
      
      quartiles[ 2 ] = 
	allFitnesses[ (int)( (FLOAT)popSize / 2.0 ) ].fitness;
      
      quartiles[ 3 ] = 
	allFitnesses[ (int)( (FLOAT)popSize / 4.0 ) ].fitness;
      
      quartiles[ 4 ] = 
	allFitnesses[ 0 ].fitness;
    } else {
      quartiles[ 0 ] = 0;
      quartiles[ 1 ] = 0;
      quartiles[ 2 ] = 0;
      quartiles[ 3 ] = 0;
      quartiles[ 4 ] = 0;
    }
    
    return TRUE;
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

void 
EvolutionaryRun::setStartGenerationInterface( StartGenerationInterface *i ) {
  m_StartGenerationInterface = i;
}

void 
EvolutionaryRun::setEndGenerationInterface( EndGenerationInterface *i ) {
  m_EndGenerationInterface = i;
}

void
EvolutionaryRun::makeSureCurrentGenotypeRecordsHaveBody() {
  /**
   * This method goes through all the population and checks
   * to see if the given genotype database record has a 
   * body. If it doesn't, it adds one.
   */

  /**
   * If we always add a body, we don't even need to check...
   */
  if ( m_WriteGenotypeBodyEvery == 1 ) {
    return;
  }

  map<unsigned long,PopulationMember>::iterator i;
  for ( i = m_Population.begin(); i != m_Population.end(); i++ ) {
    /*SmallString query;
    query += "SELECT ID FROM genotype WHERE ID=";
    query += i->first;
    query += " AND genotypeValue IS NULL";

    PResultSet *resultSet = m_GenotypeDatabase->RunSelectQuery( query, true );

    if ( resultSet->GetNumRows() == 1 ) {*/
    if ( !( i->second.getGenotype()->getIsSaved() ) ) {
      /**
       * OK - we have to add the body of the genotype...
       */
      unsigned long sizeInBits;
      const unsigned char *bytes;
      char *buf;
      SmallString query;
      
      bytes = i->second.getGenotype()->getGenotypeAsBits( &sizeInBits );
      buf = new char[ 2 * BYTES_IN_BITFIELD( sizeInBits ) + 10 ];
      mysql_escape_string( buf,
			   (char *)bytes,
			   BYTES_IN_BITFIELD( sizeInBits ) );

      query =  "UPDATE genotype "
               "SET genotypeValue = '";
      query += buf;
      query += "' WHERE ID = ";
      query += i->first;

      m_GenotypeDatabase->RunActionQuery( query );
      i->second.getGenotype()->setIsSaved( TRUE );

      delete[] buf;
    }
  }
}

void EvolutionaryRun::setNoOfPopulations( int n ) {
  m_NoOfPopulations = n;
}
