#ifndef CREATEGENOTYPEWORKER_H
#define CREATEGENOTYPEWORKER_H

/**
 * This class creates a genotype in the background.
 */

#include "PWorker.h"
#include "AbstractGenotypeFactory.h"
#include "QErrorLog.h"
#include <string>

using namespace std;

class CreateGenotypeWorker : public PWorker {
public:
  CreateGenotypeWorker( AbstractGenotypeFactory *factory ) :
    m_Factory(  factory ),
    m_Genotype( NULL    ) { }
  
  void start() { try {
      m_Genotype = m_Factory->createNewRandomGenotype(); 
    } catch( QException& e ) {
      e.fillInStackTrace();

      string errMsg;
      errMsg += "Caught exception in thread - ";
      errMsg += e.toString();
      errMsg += "\n";
      errMsg += e.getStackTrace();
      QErrorLog::writeError( errMsg.c_str() );

      throw;
    } }

  AbstractGenotype              *m_Genotype;
  
protected:
  AbstractGenotypeFactory *m_Factory;
};

#endif

