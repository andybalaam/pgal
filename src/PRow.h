/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef PROW_H
#define PROW_H

/**
 * A thin wrapper around a MYSQL_ROW.  May become thicker over time, with
 * extra functionality and especially error checking.
 */

#include <mysql/mysql.h>

#include <vector>

using namespace std;

class PResultSet;
class SmallString;

class PRow {
public:
  PRow( MYSQL_ROW mysql_row, PResultSet *parent );
  
  long  GetLongField(   const char *fieldName        );
  long  GetLongField(   long        fieldNo          );
  FLOAT GetFloatField(  const char *fieldName        );
  FLOAT GetFloatField(  long        fieldNo          );
  char*	GetStringField( const char *fieldName        );
  char*	GetStringField( long        fieldNo          );
  
  bool  GetFieldIsNull( const char *fieldName        );
  bool  GetFieldIsNull( long fieldNo                 );
  
  bool IsValid();
	
private:
  PRow( PRow& other );
  
  MYSQL_ROW   m_Row;
  PResultSet *m_ResultSet;
};

#endif
