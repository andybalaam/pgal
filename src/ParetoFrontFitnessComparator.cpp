/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "ParetoFrontFitnessComparator.h"
#include <stdio.h>
#include <values.h>
#include <vector>
#include <algorithm>

using namespace std;

ParetoFrontFitnessComparator::ParetoFrontFitnessComparator( 
    int sizeOfUnorderedSet,
    int sizeOfOrderedSet,
    int statVariable ) :
    m_SizeOfUnorderedSet( sizeOfUnorderedSet ),
    m_SizeOfOrderedSet(   sizeOfOrderedSet   ) {
  int i;
  switch( statVariable ) {
  case AVERAGE_UNORDERED_SET:
    for ( i = 0; i < m_SizeOfUnorderedSet; i++ ) {
      m_IndexNos.push_back( i );
    }
    break;

  case AVERAGE_ALL:
    for ( i = 0; i < m_SizeOfUnorderedSet + m_SizeOfOrderedSet; i++ ) {
      m_IndexNos.push_back( i );
    }
    break;

  default:
    if ( statVariable >= 0 && 
	 statVariable < m_SizeOfUnorderedSet + m_SizeOfOrderedSet ) {
      m_IndexNos.push_back( statVariable );
    } else {
      char errMsg[ 1024 ];
      sprintf( errMsg, "Invalid stat variable of %d in Pareto Front (%d,%d)",
	       statVariable, m_SizeOfUnorderedSet, m_SizeOfOrderedSet );

      InvalidStatVariableException e( errMsg );
      e.fillInStackTrace();
      throw e;
    }
  }
}

FLOAT ParetoFrontFitnessComparator::calculateFitness( 
    const vector<vector<FLOAT> >& fitnesses ) {
  /**
   * We pick the average of the index nos we've previously stored.
   */
  vector<vector<FLOAT> >::const_iterator i;
  vector<int>::const_iterator j;
  
  FLOAT average = 0;
  int   added   = 0;
  for ( i = fitnesses.begin(); i != fitnesses.end(); i++ ) {
    if ( (int)i->size() != m_SizeOfUnorderedSet + m_SizeOfOrderedSet ) {
      char errMsg[ 1024 ];
      sprintf( errMsg, "Number of sub-fitnesses = %d, should be %d (%d + %d)",
	       i->size(), 
	       m_SizeOfUnorderedSet + m_SizeOfOrderedSet,
	       m_SizeOfUnorderedSet,
	       m_SizeOfOrderedSet );

      InvalidFitnessesSize e( errMsg );
      e.fillInStackTrace();
      throw e;
    }

    for ( j = m_IndexNos.begin(); j != m_IndexNos.end(); j++, added++ ) {
      average += i->at( *j );
    }
  }

  return average / (float)added;
}

/**
 * This returns -1 if a < b, 0 if a == b, and 1 if a > b
 */
int ParetoFrontFitnessComparator::compareFitness(    
    const vector<vector<FLOAT> >& a, 
    const vector<vector<FLOAT> >& b ) {
  /**
   * We compare the unordered sets first, and then the ordered sets.
   * We average all the trials to produce a new vector of average
   * subtrials, then we compare those.
   */
  if ( getSizeOfUnorderedSet() != getSizeOfUnorderedSet() ||
       getSizeOfOrderedSet()   != getSizeOfOrderedSet() ) {
    char errMsg[ 1024 ];
    sprintf( errMsg, 
	     "Set sizes differ when comparing A and B: A=%d,%d B=%d,%d",
	     getSizeOfUnorderedSet(),
	     getSizeOfOrderedSet(),
	     getSizeOfUnorderedSet(),
	     getSizeOfOrderedSet() );
    
    InvalidSetSizeException e( errMsg );
    e.fillInStackTrace();
    throw e;
  }

  int noOfTrials = a.size();

  int unorderedComparison = 0;
  if ( getSizeOfUnorderedSet() > 0 ) {
    vector<FLOAT> averageA( getSizeOfUnorderedSet() );
    vector<FLOAT> averageB( getSizeOfUnorderedSet() );
    
    vector<vector<FLOAT> >::const_iterator i;
    vector<FLOAT>::const_iterator          j;
    vector<FLOAT>::iterator                n;

    /**
     * First fill with zeros...
     */
    fill_n( averageA.begin(), getSizeOfUnorderedSet(), 0 );
    fill_n( averageB.begin(), getSizeOfUnorderedSet(), 0 );

    int k;
    for ( i = a.begin(); i != a.end(); i++ ) {
      for ( j = i->begin(), k = 0; k < getSizeOfUnorderedSet(); j++, k++ ) {
	averageA[ k ] += *j;
      }
    }

    for ( n = averageA.begin(); n != averageA.end(); n++ ) {
      *n = *n / getSizeOfUnorderedSet();
    }

    for ( i = b.begin(); i != b.end(); i++ ) {
      for ( j = i->begin(), k = 0; k < getSizeOfUnorderedSet(); j++, k++ ) {
	averageB[ k ] += *j;
      }
    }

    for ( n = averageB.begin(); n != averageB.end(); n++ ) {
      *n = *n / getSizeOfUnorderedSet();
    }

    /*{ // DEBUG 
      fprintf( stderr, "unordered averaged sets are:\n" );
      for ( j = averageA.begin(); j != averageA.end(); j++ ) {
	fprintf( stderr, "%f ", *j );
      }
      fprintf( stderr, "\n" );

      for ( j = averageB.begin(); j != averageB.end(); j++ ) {
	fprintf( stderr, "%f ", *j );
      }
      fprintf( stderr, "\n" );
      }*/

    /**
     * Now we have the average values for each sub trial.
     * We now get the highest and lowest values for both and see
     * which are higher than the other.
     */

    FLOAT lowestA = FLT_MAX, highestA = FLT_MIN;
    FLOAT lowestB = FLT_MAX, highestB = FLT_MIN;
    
    for ( i = a.begin(); i != a.end(); i++ ) {
      for ( j = i->begin(), k = 0; k < getSizeOfUnorderedSet(); j++, k++ ) {
	if ( lowestA > *j ) {
	  lowestA = *j;
	} 
	if ( highestA < *j ) {
	  highestA = *j;
	}
      }
    }

    for ( i = b.begin(); i != b.end(); i++ ) {
      for ( j = i->begin(), k = 0; k < getSizeOfUnorderedSet(); j++, k++ ) {
	if ( lowestB > *j ) {
	  lowestB = *j;
	} 
	if ( highestB < *j ) {
	  highestB = *j;
	}
      }
    }

    /**
     * Now we can perform a compare...
     */
    if ( lowestA > highestB ) {
      unorderedComparison = 1;
    } 
    
    if ( lowestB > highestA ) {
      unorderedComparison = -1;
    }
  }

  int orderedComparison = 0;
  if ( getSizeOfOrderedSet() > 0 ) {
    /**
     * Now we have to do the ordered comparison. We have to
     * do an item by item comparison. We still need to find the
     * average sub-trials though.
     */
    vector<FLOAT> averageA( getSizeOfOrderedSet() );
    vector<FLOAT> averageB( getSizeOfOrderedSet() );
    
    vector<vector<FLOAT> >::const_iterator i;
    vector<FLOAT>::const_iterator          j;
    vector<FLOAT>::iterator                n;

    /**
     * First fill with zeros...
     */
    fill_n( averageA.begin(), getSizeOfOrderedSet(), 0 );
    fill_n( averageB.begin(), getSizeOfOrderedSet(), 0 );

    int k;
    for ( i = a.begin(); i != a.end(); i++ ) {
      for ( j = i->begin() + getSizeOfUnorderedSet(), k = 0; 
	    k < getSizeOfOrderedSet(); 
	    j++, k++ ) {
	//fprintf( stderr, "averageA[ %d ] = %.3f\n", k, *j );
	averageA[ k ] += *j;
      }
    }

    for ( n = averageA.begin(); n != averageA.end(); n++ ) {
      *n = *n / noOfTrials;
    }

    for ( i = b.begin(); i != b.end(); i++ ) {
      for ( j = i->begin() + getSizeOfUnorderedSet(), k = 0; 
	    k < getSizeOfOrderedSet(); 
	    j++, k++ ) {
        //fprintf( stderr, "averageB[ %d ] = %.3f\n", k, *j );
	averageB[ k ] += *j;
      }
    }

    for ( n = averageB.begin(); n != averageB.end(); n++ ) {
      *n = *n / noOfTrials;
    }
  
    /*{ // DEBUG 
      fprintf( stderr, "ordered averaged sets are:\n" );
      for ( j = averageA.begin(); j != averageA.end(); j++ ) {
	fprintf( stderr, "%f ", *j );
      }
      fprintf( stderr, "\n" );

      for ( j = averageB.begin(); j != averageB.end(); j++ ) {
	fprintf( stderr, "%f ", *j );
      }
      fprintf( stderr, "\n" );
      }*/

    /**
     * Now we compare item by item...
     */
    vector<FLOAT>::const_iterator m;
    for ( j = averageA.begin(), m = averageB.begin();
	  j != averageA.end();
	  j++, m++ ) {
      if ( *j > *m ) {
	if ( orderedComparison == -1 ) {
	  orderedComparison = 0;
	  break;
	}
	orderedComparison = 1;
      } else if ( *j < *m ) {
	if ( orderedComparison == 1 ) {
	  orderedComparison = 0;
	  break;
	}
	orderedComparison = -1;
      }
    }
  }

  if ( getSizeOfUnorderedSet() > 0 &&
       getSizeOfOrderedSet() == 0 ) {
    //fprintf( stderr, "goat 1 - return %d\n", unorderedComparison );
    return unorderedComparison;
  } else if ( getSizeOfUnorderedSet() == 0 &&
	      getSizeOfOrderedSet() > 0 ) {
    //fprintf( stderr, "goat 2 - return %d\n", orderedComparison );
    return orderedComparison;
  } else if ( unorderedComparison == orderedComparison ) {
    //fprintf( stderr, "goat 3 - return %d\n", orderedComparison );
    return orderedComparison;
  } else {
    //fprintf( stderr, "goat 4 - %d,%d\n", 
    //	     unorderedComparison, orderedComparison );
    return 0;
  }
}
