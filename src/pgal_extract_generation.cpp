/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "./QException.h"
#include "./GenotypeDatabase.h"
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

QEXCEPTIONCLASS( InvalidPopulationSizeException );
QEXCEPTIONCLASS( ProgrammingError );

/**
 * This program extracts a given generation of a given evolutionary part 
 * run, and outputs it in an SQL format suitable for the seed for an 
 * initial population.
 */

static void printUsage() {
  fprintf( stderr,
	   "pgal_extract_generation [-e {evolutionary part}] [-h]\n"
           "                        [-f {forced evolutionary part run ID}]\n"
           "                        [-g {generation no}] [-t {type no}]\n" );
  exit( 1 );
}

int main( int argc, char **argv ) {
  try {
    /**
     * First we parse the command parameters.
     */
    int typeNo                    = -1,
      evolutionaryRunID           =  1,
      forcedEvolutionaryPartRunID =  1,
      generationNo                =  0,
      nextOption;

    bool specifiedPartRunID = false,
      specifiedTypeNo       = false;
    
    const char *shortOptions = "e:g:ht:f:";
   
    const struct option longOptions[] = {
      { "evolutionaryRunID",   1, NULL, 'e' },
      { "help",                0, NULL, 'h' },
      { "typeNo",              1, NULL, 't' },
      { "forcedPartRunID",     1, NULL, 'f' },
      { "generationNo",        1, NULL, 'g' },
      { NULL,                  0, NULL, ' ' }
    };

    do {
      nextOption = getopt_long( argc, 
				argv,
				shortOptions,
				longOptions,
				NULL );
      switch( nextOption ) {
      case 'e':
	evolutionaryRunID = atol( optarg );
	break;
	
      case 'g':
	generationNo = atol( optarg );
	break;

      case 'f':
	forcedEvolutionaryPartRunID = atol( optarg );
	specifiedPartRunID = true;
	break;

      case 't':
	typeNo = atol( optarg );
	specifiedTypeNo = true;
	break;

      case 'h':
      case '?':
	printUsage();
	break;

      case -1:
	break;

      default:
	fprintf( stderr, "\'%c\' %d\n", nextOption, nextOption );
	ProgrammingError e( "Got to default" );
	e.fillInStackTrace();
	throw e;
      }
    } while ( nextOption != -1 );

    /**
     * Here is the task of jobs we need to do. First we find the
     * population size. Then we load in this number of valid genotypes
     * from the given evolutionary run. Then we go through and print
     * it out as an SQL statement. If the user specified a forced
     * part run ID, then we use this. Otherwise, we create our own 
     * table, assume it's going into an empty database, and use an
     * ID of one.
     */
    GenotypeDatabase database( "pgalUser",
			       "pgal",
			       "127.0.0.1",
			       "pgalDB" );

    unsigned long populationSize;
    map<unsigned long,MemFile> genotypeValues;
    map<unsigned long,int>     sizeInBits;
    map<unsigned long,int>     typeNos;

    populationSize = database.loadPopulationAtGeneration( generationNo,
							  evolutionaryRunID,
							  &genotypeValues,
							  &sizeInBits,
							  &typeNos );
    
    if ( populationSize <= 0 ) {
      InvalidPopulationSizeException e( "Invalid population size" );
      e.fillInStackTrace();
      throw e;
    }

    /**
     * Now we simply output this information as an SQL statement...
     */
    if ( specifiedPartRunID == false ) {
      /**
       * We need to create an evolutionary run and part run.
       */
      printf( "INSERT INTO evolutionaryRun "
	      "( ID, initialPopulationSize, programVersion ) "
              "VALUES "
              "( 1, %lu, \"Created by pgal_extract_generation\" );\n"
              "\n"

	      "INSERT INTO evolutionaryPartRun "
              "( ID, evolutionaryRunID, endTime, startTime, notes ) "
              "VALUES "
              "( 1, 1, null, null, \"Created by pgal_extract_generation\" );\n"
              "\n"

	      "INSERT INTO generation "
	      "( ID, generationNo, evolutionaryPartRunID, timeStamp ) "
	      "VALUES "
	      "( null, 0, 1, null );\n"
	      "\n",
	      populationSize );
    }

    map<unsigned long,MemFile>::iterator i;
    map<unsigned long,int>::iterator     j;
    map<unsigned long,int>::iterator     k;
    for ( i = genotypeValues.begin(); i != genotypeValues.end(); i++ ) {
      unsigned long id = i->first;

      unsigned long noOfBytes;
      const unsigned char *bytes = i->second.getMemFile( &noOfBytes );
      
      SmallString escaped;
      database.EscapeString( escaped, 
			     (const char *)bytes,
			     noOfBytes );
      
      int genotypeTypeNo;
      if ( specifiedTypeNo == true ) {
	genotypeTypeNo = typeNo;
      } else {
	j = typeNos.find( id );
	genotypeTypeNo = j->second;
      }
      
      k = sizeInBits.find( id );
      int genotypeSizeInBits = k->second;

      printf( "INSERT INTO genotype "
	      "( ID, evolutionaryPartRunID, genotypeValue, parentID, "
	      "sizeInBits, firstGenerationNo, lastGenerationNo, typeID ) "
	      "VALUES "
	      "( null, %lu, \"%s\", 0, %lu, 0, null, %d );\n"
	      "\n",
	      forcedEvolutionaryPartRunID,
	      (const char *)escaped,
	      genotypeSizeInBits,
	      genotypeTypeNo );
    }
  } catch( QException& e ) {
    fprintf( stderr, "Exception caught: %s\n%s\n",
	     e.toString(), e.getStackTrace() );
    exit( 1 );
  }

  return 0;
}

