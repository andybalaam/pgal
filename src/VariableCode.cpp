#include "VariableCode.h"
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/**
 * GETBIT_B(array,sizeOfArray,bitnum)
 */
#define GETBIT_B(A,B)        (((A)[(B)/8]>>(7-((B)%8)))&1)
#define SETBIT_B(A,B)        (((A)[(B)/8])|=(1<<(7-((B)%8))))
#define CLEARBIT_B(A,B)      (((A)[(B)/8])&=(~(1<<(7-((B)%8)))))
#define BYTES_IN_BITFIELD(A) (((A)/8)+(((A)%8==0)?0:1))

#define INCREASE_BY 200

VariableCode::VariableCode() : m_Stream( NULL ) {
  m_Instance = new struct S_RealStruct;
  m_Instance->m_Array = new unsigned char[ INCREASE_BY ];
  m_Instance->m_NoOfPointers = 1;
  m_Instance->m_CurrentSize = INCREASE_BY;
  clearInstance();
}

VariableCode::VariableCode( const VariableCode& v ) : m_Stream( NULL ) {
  if ( v.m_Stream == NULL ) {
    m_Instance = v.m_Instance;
    m_Instance->m_NoOfPointers++;
  } else {
    m_Stream         = v.m_Stream;
    m_StartPointBits = v.m_StartPointBits;
    m_EndPointBits   = v.m_EndPointBits;
  }
}

/**
 * This is a special constructor and should be used with care...
 * it sets up the new VariableCode instance as a stream. This
 * is so it can be used within a map class to search for code
 * that begin with different tokens.
 */
VariableCode::VariableCode( const unsigned char *s, 
			    long                 startPointBits,
			    long                 endPointBits ) :
  m_Stream(         s              ),
  m_StartPointBits( startPointBits ),
  m_EndPointBits(   endPointBits   ) { }

VariableCode::~VariableCode() {
  if ( m_Stream == NULL && --m_Instance->m_NoOfPointers == 0 ) {
    delete[] m_Instance->m_Array;
    delete m_Instance;
  }
}

void VariableCode::printAsBinary( string& buffer ) const {
  if ( m_Stream == NULL ) {
    if ( m_Instance->m_BitSize > 0 ) {
      int i;
      for ( i = 0;
	    i < m_Instance->m_BitSize;
	    i++ ) {
	if ( GETBIT_B( m_Instance->m_Array, i ) == 0 ) {
	  buffer += "0";
	} else {
	  buffer += "1";
	}
      }
    }
  } else {
    /**
     * We need to print this off as a stream.
     */
    unsigned long i;
    for ( i = m_StartPointBits; i <= m_EndPointBits; i++ ) {
      if ( GETBIT_B( m_Stream, i ) == 0 ) {
	buffer += "0";
      } else {
	buffer += "1";
      }
    }
  }
}

void VariableCode::readAsBinary( const char *s ) {
  /**
   * We continue going on until we run out of zeros and ones.
   */
  clearInstance();

  for ( ; *s == '0' || *s == '1'; s++ ) {
    if ( BYTES_IN_BITFIELD( m_Instance->m_BitSize + 1 ) >=
	 m_Instance->m_CurrentSize ) {
      long newSize = ( ( ( BYTES_IN_BITFIELD( m_Instance->m_BitSize + 1 ) ) /
			 INCREASE_BY ) + 1 ) * INCREASE_BY;
      
      unsigned char *s = new unsigned char[ newSize ];
      memcpy( s, 
	      m_Instance->m_Array, 
	      BYTES_IN_BITFIELD( m_Instance->m_BitSize ) );
      
      delete[] m_Instance->m_Array;
      m_Instance->m_Array = s;
      m_Instance->m_CurrentSize = newSize;
    }

    if ( *s == '0' ) {
      addBitZero();
    } else {
      addBitOne();
    }
  }
}

void VariableCode::copyInstance() {
  struct S_RealStruct *s = new struct S_RealStruct;
  s->m_NoOfPointers = 1;
  s->m_BitSize      = m_Instance->m_BitSize;
  s->m_Array        = new unsigned char[ m_Instance->m_CurrentSize ];
  s->m_CurrentSize  = m_Instance->m_CurrentSize;
  memcpy( s->m_Array, m_Instance->m_Array, BYTES_IN_BITFIELD( s->m_BitSize ) );

  if ( --m_Instance->m_NoOfPointers == 0 ) {
    delete[] m_Instance->m_Array;
    delete m_Instance;
  }

  m_Instance = s;
}

void VariableCode::clearInstance() {
  if ( m_Instance->m_NoOfPointers > 1 ) {
    copyInstance();
  }

  m_Instance->m_Array[ 0 ] = 0;
  m_Instance->m_BitSize = 0;
}

const unsigned char *VariableCode::getRawBits( int *bitSize ) const {
  *bitSize = m_Instance->m_BitSize;
  return m_Instance->m_Array;
}

bool VariableCode::operator<( const VariableCode& code ) const {
  /**
   * One of us is a stream. So, we do a bit test.
   */
  
  long aStart, aEnd, bStart, bEnd;
  const unsigned char *a, *b;

  if ( m_Stream == NULL ) {
    a       = m_Instance->m_Array;
    aStart  = 0;
    aEnd    = m_Instance->m_BitSize - 1;
  } else {
    a       = m_Stream;
    aStart  = m_StartPointBits;
    aEnd    = m_EndPointBits;
  }
  
  if ( code.m_Stream == NULL ) {
    b       = code.m_Instance->m_Array;
    bStart  = 0;
    bEnd    = code.m_Instance->m_BitSize - 1;
  } else {
    b       = code.m_Stream;
    bStart  = code.m_StartPointBits;
    bEnd    = code.m_EndPointBits;
  }
  
  /**
   * The two bit fields are of equal length. So we must go 
   * through them, finding out where they differ.
   */
  for ( ; 
	aStart < aEnd && bStart < bEnd &&
	  GETBIT_B( a, aStart ) == 
	  GETBIT_B( b, bStart );
	aStart++, bStart++ )
    ;
  
  return GETBIT_B( a, aStart ) < GETBIT_B( b, bStart );
}

void VariableCode::advanceBits( unsigned long l ) {
  if ( m_Stream != NULL ) {
    m_StartPointBits += l;
  }
}

void VariableCode::addBitZero() {
  if ( m_Instance->m_NoOfPointers > 1 ) {
    copyInstance();
  }

  if ( BYTES_IN_BITFIELD( m_Instance->m_BitSize + 1 ) >=
       m_Instance->m_CurrentSize ) {
    long newSize = ( ( ( BYTES_IN_BITFIELD( m_Instance->m_BitSize + 1 ) ) /
		       INCREASE_BY ) + 1 ) * INCREASE_BY;
    
    unsigned char *s = new unsigned char[ newSize ];
    memcpy( s, 
	    m_Instance->m_Array, 
	    BYTES_IN_BITFIELD( m_Instance->m_BitSize ) );
    
    delete[] m_Instance->m_Array;
    m_Instance->m_Array = s;
    m_Instance->m_CurrentSize = newSize;
  }
  
  if ( ( m_Instance->m_BitSize % 8 ) == 0 ) {
    m_Instance->m_Array[ ( m_Instance->m_BitSize / 8 ) + 1 ] = 0;
  }

  CLEARBIT_B( m_Instance->m_Array, m_Instance->m_BitSize );

  m_Instance->m_BitSize++;
}

void VariableCode::addBitOne() {
  if ( m_Instance->m_NoOfPointers > 1 ) {
    copyInstance();
  }
  
  if ( BYTES_IN_BITFIELD( m_Instance->m_BitSize + 1 ) >=
       m_Instance->m_CurrentSize ) {
    long newSize = ( ( ( BYTES_IN_BITFIELD( m_Instance->m_BitSize + 1 ) ) /
		       INCREASE_BY ) + 1 ) * INCREASE_BY;
    
    unsigned char *s = new unsigned char[ newSize ];
    memcpy( s, 
	    m_Instance->m_Array, 
	    BYTES_IN_BITFIELD( m_Instance->m_BitSize ) );
    
    delete[] m_Instance->m_Array;
    m_Instance->m_Array = s;
      m_Instance->m_CurrentSize = newSize;
  }
  
  if ( ( m_Instance->m_BitSize % 8 ) == 0 ) {
    m_Instance->m_Array[ ( m_Instance->m_BitSize / 8 ) + 1 ] = 0;
  }
  
  SETBIT_B( m_Instance->m_Array, m_Instance->m_BitSize );
  m_Instance->m_BitSize++;
}

void VariableCode::removeBit() {
  if ( m_Instance->m_NoOfPointers > 1 ) {
    copyInstance();
  }
  
  m_Instance->m_BitSize--;
  CLEARBIT_B( m_Instance->m_Array, m_Instance->m_BitSize );
}

void VariableCode::addVariableCode( const VariableCode& code ) {
  /**
   * This method adds the bits contained in the given VariableCode
   * onto the end of this instance. We don't want to do it bit by
   * bit though.
   */
  if ( m_Instance->m_NoOfPointers > 1 ) {
    copyInstance();
  }
  
  if ( BYTES_IN_BITFIELD( m_Instance->m_BitSize ) +
       BYTES_IN_BITFIELD( code.m_Instance->m_BitSize ) >=
       m_Instance->m_CurrentSize ) {
    long newSize = ( ( ( BYTES_IN_BITFIELD( m_Instance->m_BitSize ) +
			 BYTES_IN_BITFIELD( code.m_Instance->m_BitSize ) ) /
		       INCREASE_BY ) + 1 ) * INCREASE_BY;

    unsigned char *s = new unsigned char[ newSize ];
    memcpy( s, 
	    m_Instance->m_Array, 
	    BYTES_IN_BITFIELD( m_Instance->m_BitSize ) );

    delete[] m_Instance->m_Array;
    m_Instance->m_Array = s;
    m_Instance->m_CurrentSize = newSize;
  }

  if ( ( m_Instance->m_BitSize % 8 ) == 0 ) {
    /**
     * This is easy! We just add on the bytes.
     */
    int remoteNoOfBytes = BYTES_IN_BITFIELD( code.m_Instance->m_BitSize );
    int localNoOfBytes  = BYTES_IN_BITFIELD( m_Instance->m_BitSize      );
    int i;
    for ( i = 0; i < remoteNoOfBytes; i++ ) {
      m_Instance->m_Array[ i + localNoOfBytes ] = 
	code.m_Instance->m_Array[ i ];
    }
  } else {
    int remoteNoOfBytes = BYTES_IN_BITFIELD( code.m_Instance->m_BitSize );
    int localNoOfBytes  = BYTES_IN_BITFIELD( m_Instance->m_BitSize      );
    int rotateNum       = m_Instance->m_BitSize % 8;
    int i;
    for ( i = 0; i < remoteNoOfBytes; i++ ) {
      unsigned char c = code.m_Instance->m_Array[ i ];
      c = c >> rotateNum;
      unsigned char mask = 0xff >> rotateNum;
      mask = ~mask;
      m_Instance->m_Array[ localNoOfBytes - 1 + i ] =
	m_Instance->m_Array[ localNoOfBytes - 1 + i ] & mask;
      m_Instance->m_Array[ localNoOfBytes - 1 + i ] =
	m_Instance->m_Array[ localNoOfBytes - 1 + i ] | c;
      
      mask = 0xff << ( 8 - rotateNum );
      mask = ~mask;
      c = code.m_Instance->m_Array[ i ];
      c = c << ( 8 - rotateNum );
      
      m_Instance->m_Array[ localNoOfBytes + i ] =
	m_Instance->m_Array[ localNoOfBytes + i ] & mask;
      m_Instance->m_Array[ localNoOfBytes + i ] = 
	m_Instance->m_Array[ localNoOfBytes + i ] | c;
    }
  }
  
  m_Instance->m_BitSize += code.m_Instance->m_BitSize;
}
 
unsigned int VariableCode::getBitSize() const {
  if ( m_Stream == 0 ) {
    return m_Instance->m_BitSize;
  } else {
    return m_EndPointBits - m_StartPointBits + 1;
  }
}
