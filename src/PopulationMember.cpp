#include "PopulationMember.h"

const unsigned char PopulationMember::KNOWN_FITNESS    = 0x00;
const unsigned char PopulationMember::UNKNOWN_FITNESS  = 0x01;
const unsigned char PopulationMember::INVALID_GENOTYPE = 0x02;

PopulationMember::PopulationMember( unsigned long     ID,
				    AbstractGenotype *g,
				    unsigned char     s ) : m_ID( ID ),
							    m_State( s ) {
  m_CommonStructurePointer                 = new struct S_counterStruct;
  m_CommonStructurePointer->m_Genotype     = g;
  m_CommonStructurePointer->m_NoOfPointers = 1;
}

PopulationMember::PopulationMember( const PopulationMember& p ) {
  m_ID      = p.m_ID;
  m_State   = p.m_State;
  m_Fitness = p.m_Fitness;
  
  m_CommonStructurePointer = p.m_CommonStructurePointer;
  m_CommonStructurePointer->m_NoOfPointers++;
}

PopulationMember::~PopulationMember() {
  if ( m_CommonStructurePointer->m_NoOfPointers == 1 ) {
    delete m_CommonStructurePointer->m_Genotype;
    delete m_CommonStructurePointer;
  } else {
    m_CommonStructurePointer->m_NoOfPointers--;
  }
}

PopulationMember& PopulationMember::operator=( const PopulationMember& p ) {
  if ( m_CommonStructurePointer->m_NoOfPointers == 1 ) {
    delete m_CommonStructurePointer->m_Genotype;
    delete m_CommonStructurePointer;
  } else {
    m_CommonStructurePointer->m_NoOfPointers--;
  }

  m_ID      = p.m_ID;
  m_State   = p.m_State;
  m_Fitness = p.m_Fitness;

  m_CommonStructurePointer = p.m_CommonStructurePointer;
  m_CommonStructurePointer->m_NoOfPointers++;

  return *this;
}
