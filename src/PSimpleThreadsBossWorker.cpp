#include "PSimpleThreadsBossWorker.h"
#include <unistd.h>
#include <pthread.h>
#include <sys/signal.h>
#include "PWorker.h"
#include "QErrorLog.h"
#include <string>

using namespace std;

PSimpleThreadsBossWorker::PSimpleThreadsBossWorker( int noOfThreads ) :
    m_NoOfThreads(         noOfThreads ),
    m_NoOfActiveProcesses( 0           ),
    m_NoOfFreeProcesses(   0           ),
    m_ArrayOfThreads(      NULL        ),
    m_ExitFlag(            false       ) {
  /**
   * We have to create a condition variable for all the threads 
   * to wait on.
   */
  pthread_mutexattr_t defaultMutexAttributes;
  pthread_mutexattr_init( &defaultMutexAttributes );

  pthread_mutex_init( &m_FinishedWorkersMutex, &defaultMutexAttributes );
  pthread_mutex_init( &m_GroupMutex,           &defaultMutexAttributes );

  pthread_condattr_t defaultCondAttributes;
  pthread_condattr_init( &defaultCondAttributes );

  pthread_cond_init( &m_GroupCond,                 &defaultCondAttributes );
  pthread_cond_init( &m_AvailableCond,             &defaultCondAttributes );
  pthread_cond_init( &m_FinishedWorkAvailableCond, &defaultCondAttributes );

  m_ArrayOfThreads = new pthread_t[ m_NoOfThreads ];
  int i;

  pthread_mutex_lock( &m_GroupMutex );

  for ( i = 0; i < m_NoOfThreads; i++ ) {
    pthread_create( &( m_ArrayOfThreads[ i ] ),
		    NULL,
		    staticThreadStart,
		    this );
  }

  while ( m_NoOfFreeProcesses < m_NoOfThreads ) {
    pthread_cond_wait( &m_AvailableCond, &m_GroupMutex );
  }

  pthread_mutex_unlock( &m_GroupMutex );
}

PSimpleThreadsBossWorker::~PSimpleThreadsBossWorker() {
  /**
   * We have to stop all the threads.
   */
  m_ExitFlag = true;

  pthread_mutex_lock( &m_GroupMutex );
  pthread_cond_broadcast( &m_GroupCond );

  usleep( 100000 );

  pthread_cond_broadcast( &m_GroupCond );

  usleep( 100000 );

  int i;
  for ( i = 0; i < m_NoOfThreads; i++ ) {
    pthread_kill( m_ArrayOfThreads[ i ], SIGKILL );
  }

  pthread_mutex_unlock( &m_GroupMutex );

  delete m_ArrayOfThreads;

  pthread_cond_destroy(  &m_GroupCond                 );
  pthread_cond_destroy(  &m_AvailableCond             );
  pthread_cond_destroy(  &m_FinishedWorkAvailableCond );

  pthread_mutex_destroy( &m_FinishedWorkersMutex      );
  pthread_mutex_destroy( &m_GroupMutex                );
}

void PSimpleThreadsBossWorker::run( PWorker *worker ) {
  pthread_mutex_lock( &m_GroupMutex );
  m_NextWorkers.push( worker );
  pthread_cond_signal( &m_GroupCond );
  pthread_mutex_unlock( &m_GroupMutex );
}

PWorker *PSimpleThreadsBossWorker::waitForProcessToFinish( bool forceWait ) {
  PWorker *toReturn = NULL;

  pthread_mutex_lock( &m_FinishedWorkersMutex );

  if ( m_FinishedWorkers.empty() == false ) {
    toReturn = m_FinishedWorkers.front();
    m_FinishedWorkers.pop();
    pthread_mutex_unlock( &m_FinishedWorkersMutex );

    return toReturn;
  }
  
  if ( forceWait == false || m_NoOfActiveProcesses == 0 ) {
    pthread_mutex_unlock( &m_FinishedWorkersMutex );
    return NULL;
  }

  while ( m_FinishedWorkers.empty() == true ) {
    pthread_cond_wait( &m_FinishedWorkAvailableCond, &m_FinishedWorkersMutex );
  }

  toReturn = m_FinishedWorkers.front();
  m_FinishedWorkers.pop();
  pthread_mutex_unlock( &m_FinishedWorkersMutex );
  
  return toReturn;
}
      
void *PSimpleThreadsBossWorker::staticThreadStart( void *address ) {
  ((PSimpleThreadsBossWorker*)address)->threadStart();
}

void PSimpleThreadsBossWorker::threadStart() {
  pthread_mutex_lock( &m_GroupMutex );
  m_NoOfFreeProcesses++;
  pthread_cond_broadcast( &m_AvailableCond );
  
  for ( ;; ) {
    while ( m_NextWorkers.empty() == true ) {
      pthread_cond_wait( &m_GroupCond, &m_GroupMutex );
    }
    
    m_NoOfFreeProcesses--;
    m_NoOfActiveProcesses++;
    
    while ( m_NextWorkers.empty() == false ) {
      PWorker *currentWorker = m_NextWorkers.front();
      m_NextWorkers.pop();
      
      pthread_mutex_unlock( &m_GroupMutex );
      
      try {
	currentWorker->start();
      } catch( QException& e ) {
	string errMsg;
	errMsg += "Thread exited - exception caught - ";
	errMsg += e.toString();
	errMsg += "\n";
	errMsg += e.getStackTrace();
	QErrorLog::writeError( errMsg.c_str() );
	pthread_exit( NULL );
      }
      
      pthread_mutex_lock( &m_GroupMutex );
      pthread_mutex_lock( &m_FinishedWorkersMutex );
      m_FinishedWorkers.push( currentWorker );
      pthread_cond_broadcast( &m_FinishedWorkAvailableCond );
      pthread_mutex_unlock( &m_FinishedWorkersMutex );
    } // End while
    
    m_NoOfFreeProcesses++;
    m_NoOfActiveProcesses--;
  }
}

int PSimpleThreadsBossWorker::getNoOfActiveProcesses() const {
  int toReturn;
  pthread_mutex_lock( &m_GroupMutex );
  toReturn = m_NoOfActiveProcesses;
  pthread_mutex_unlock( &m_GroupMutex );

  return toReturn;
}

int PSimpleThreadsBossWorker::getNoOfFreeProcesses() const {
  int toReturn;
  pthread_mutex_lock( &m_GroupMutex );
  toReturn = m_NoOfFreeProcesses;
  pthread_mutex_unlock( &m_GroupMutex );

  return toReturn;
}

int PSimpleThreadsBossWorker::getNoOfFinishedWorkers() const {
  int toReturn;
  pthread_mutex_lock( &m_FinishedWorkersMutex );
  toReturn = m_FinishedWorkers.size();
  pthread_mutex_unlock( &m_FinishedWorkersMutex );

  return toReturn;
}
