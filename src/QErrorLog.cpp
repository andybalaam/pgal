/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "QErrorLog.h"
#include <errno.h>
#include <string>

extern "C" {
    #include <stdio.h>
    #include <time.h>
}

using namespace std;

bool    QErrorLog::m_RunBefore = false;
FILE   *QErrorLog::m_Output    = stderr;
string  QErrorLog::m_Filename;

bool QErrorLog::setFilename( const char *filename ) {
    errno = 0;
    FILE *tempOutput = NULL;
    if ( ( tempOutput = fopen( filename, "a" ) ) == NULL ) {
        string errMsg;
        errMsg += "Cannot open log file ";
        errMsg += filename;
        errMsg += " - error states: ";
        errMsg += strerror( errno );
        writeError( errMsg );

        return false;
    }

    if ( m_Output != stderr ) {
        fclose( m_Output );
    }

    m_Output = tempOutput;
    m_Filename = filename;

    return true;
}

void QErrorLog::__writeErrLog( long  lineNo, 
                               const string& filename, 
                               const string& errMsg,
                               int   flag ) {
    if ( !m_RunBefore ) {
        string date = __DATE__;
        fprintf( m_Output, "\n**** Start of run - compiled on %s ****\n", date.c_str() );
        fflush( m_Output );

        m_RunBefore = true;
    }

    // First get the time.
    time_t tp = time( NULL );
    char timeMsg[ 100 ];
    strftime( timeMsg, 99, "%d/%m/%Y - %H:%M:%S", localtime( &tp ) );
    
    // Now build the message up.
    string message;
    message += timeMsg;
    message += "\t";
    if ( flag ) {
      char number[ 1024 ];
      message += filename;
      message += "\t";
      sprintf( number, "%d", lineNo );
      message += number;
      message += "\t";
    }

    string errMsgStr;
    errMsgStr += errMsg;
    removeSurroundingWhitespace( errMsgStr );
    message += errMsgStr;
    message += "\n";

    fputs( message.c_str(), m_Output );
    fflush( m_Output );
}

void QErrorLog::removeSurroundingWhitespace( string& buffer ) {
  string newBuffer;
  const char *s = buffer.c_str();
  while ( isspace( *s ) ) {
    s++;
  }

  const char *e = &(buffer.c_str()[ strlen( buffer.c_str() ) ]);
  if ( strlen( buffer.c_str() ) > 0 ) {
    e--;
  }

  while ( e > s && isspace( *e ) ) {
    e--;
  }
 
  while ( *s && s <= e ) {
    newBuffer += *s;
    s++;
  }

  buffer = newBuffer;
}
