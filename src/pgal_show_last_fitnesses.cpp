/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "./QException.h"
#include "./GenotypeDatabase.h"
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

#define LOOK_BACK 10000

QEXCEPTIONCLASS( ProgrammingError );

/**
 * This program displays a nicely formatted list of the last
 * fitnesses, or the last fitnesses with a genotype body.
 */

static void printUsage() {
  fprintf( stderr,
	   "pgal_show_last_fitnesses [-e {evolutionary run}] [-h]\n"
           "                         [-p {evolutionary part run}]\n"
	   "                         [-b ] [-n {number}]\n" );
  exit( 1 );
}

int main( int argc, char **argv ) {
  try {
    /**
     * First we parse the command parameters.
     */
    int numberInList = 10,
      evolutionaryRun = -1,
      evolutionaryPartRun = -1,
      nextOption;

    bool printUsageFlag = false,
      bodyNotNull = false;
    
    const char *shortOptions = "e:hp:bn:";
    
    const struct option longOptions[] = {
      { "evolutionaryRun",     1, NULL, 'e'  },
      { "evolutionaryPartRun", 1, NULL, 'p'  },
      { "bodyNotNull",         0, NULL, 'b'  },
      { "help",                0, NULL, 'h'  },
      { "numberInList",        1, NULL, 'n'  },
      { NULL,                  0, NULL, ' '  }
    };

    do {
      nextOption = getopt_long( argc, 
				argv,
				shortOptions,
				longOptions,
				NULL );
      switch( nextOption ) {
      case 'e':
	evolutionaryRun = atol( optarg );
	break;

      case 'p':
	evolutionaryPartRun = atol( optarg );
	break;

      case 'n':
	numberInList = atol( optarg );
	break;

      case 'b':
	bodyNotNull = true;
	break;

      case -1:
	break;

      case 'h':
      case '?':
	printUsage();
	break;

      default:
	ProgrammingError e( "Got to default" );
	e.fillInStackTrace();
	throw e;
      }
    } while ( nextOption != -1 );
      
    if ( evolutionaryRun     == -1 &&
	 evolutionaryPartRun == -1 ) {
      evolutionaryRun = 1;
    }

    if ( numberInList <= 0 ) {
      printUsageFlag = true;
    }

    if ( printUsageFlag == true ) {
      printUsage();
    }

    /**
     * OK - now we have to build up a big list of about LOOK_BACK of the
     * last trials done.
     */
    GenotypeDatabase database( "pgalUser",
			       "pgal",
			       "localhost",
			       "pgalDB" );
    
    map<unsigned long,vector<FLOAT> > IDToFitness;
    map<unsigned long,bool>           IDToHasBodyFlag;
    map<unsigned long,unsigned long>  IDToGenerationNo;
    map<unsigned long,unsigned long>  IDToType;
    map<unsigned long,MemFile>        IDToGenotypeBodies;

    database.findLatestGenotypesFromEvolutionaryPartRun( IDToFitness,
							 IDToHasBodyFlag,
							 IDToGenerationNo,
							 IDToType,
							 IDToGenotypeBodies,
							 evolutionaryPartRun,
							 numberInList,
							 bodyNotNull,
							 false );
    
    map<unsigned long,vector<FLOAT> >::const_iterator i;
    map<unsigned long, bool>::const_iterator          j;
    map<unsigned long,unsigned long>::const_iterator  k;
    map<unsigned long,unsigned long>::const_iterator  n;

    printf( "No.\tID.\tfitness\t\tHas body\tGeneration No.\tType\n" );

    int m;
    for ( i = IDToFitness.begin(), m = 0;
          IDToFitness.size() - m > numberInList;
          i++, m++ )
      ;

    for ( m = 0; 
          i != IDToFitness.end();
          i++, m++ ) {
      j = IDToHasBodyFlag.find(  i->first );
      k = IDToGenerationNo.find( i->first );
      n = IDToType.find(         i->first );

      printf( "%d\t%lu\t", m, i->first );
      vector<FLOAT>::const_iterator z;
      for ( z = i->second.begin(); z != i->second.end(); z++ ) {
	if ( z != i->second.begin() ) {
	  printf( "," );
	}
	printf( "%-03.2f", *z );
      }
      printf( "\t%s\t\t%lu\t\t%lu\n",
	      ( j->second == true ) ? "*" : "",
	      k->second - 1,
	      n->second );
    }
  } catch( QException& e ) {
    fprintf( stderr, "Caught exception: %s\n%s\n",
	     e.toString(),
	     e.getStackTrace() );
    exit( 1 );
  }

  return 0;
}
