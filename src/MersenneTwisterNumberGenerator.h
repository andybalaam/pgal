/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef MERSENNETWISTERNUMBERGENERATOR_H
#define MERSENNETWISTERNUMBERGENERATOR_H

#include <pthread.h>
#include "RandomNumberGenerator.h"

#define N 624

class MersenneTwisterNumberGenerator : public RandomNumberGenerator {
public:
  MersenneTwisterNumberGenerator();

  virtual void           setRandomSeed( unsigned long );
  virtual void           randomise();
  virtual unsigned short random16BitInt( unsigned short range );
  virtual unsigned long  randomLong();
  virtual double         randomDouble();
  virtual bool           randomBit();

  static  void           setInitialNum1( long initialNum );
  static  void           setInitialNum2( long initialNum );

protected:
  static long   s_InitialNum1;
  static long   s_InitialNum2;

  bool          m_DoneRandomise;
  bool          m_DoneFirstRandomNum;
  unsigned long m_mt[ N ];
  int           m_mti;

  pthread_mutex_t m_Mutex;
};

#endif
