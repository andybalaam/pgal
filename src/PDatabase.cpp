/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "PDatabase.h"
#include "PResultSet.h"
#include "SmallString.h"

#include <errno.h>
#include <signal.h>

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

#define MAX_CONSECUTIVE_CONNECTIONS 100
#define TRUE 1
#define FALSE 0

PDatabase::PDatabase( const char *username,
		      const char *password,
		      const char *hostname,
		      const char *databaseName ) : 
  m_Connection( NULL ),
  m_Username( username ),
  m_Password( password ),
  m_Hostname( hostname ),
  m_DatabaseName( databaseName ),
  m_Counter( 0L ),
  write_recovery_SQL_to_file( FALSE )
{ }

PDatabase::~PDatabase() {
  disconnect();
}

void PDatabase::disconnect() {
  if ( m_Connection != NULL ) {
    mysql_close( m_Connection );
    m_Connection = NULL;
  }
}

/**
 * @throw PDatabase::PDatabaseException
 */
long PDatabase::RunInsertQuery( const char *query ) {	

  // Commented because RunActionQuery no longer throws exceptions
  // with the new error recovery code
  
  //try {
    
    RunActionQuery( query );	
    
    if( write_recovery_SQL_to_file ) {
      
      return 0;
      
    } else {
    
    return mysql_insert_id( m_Connection );
      
    }
    
  /*} catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }*/
}

void PDatabase::SaveRecoveryData( const char *query ) {
	
	SaveRecoveryData( query, NULL );
	
}

void PDatabase::SaveRecoveryData( const char *query, QException* e ) {
  
  string recovery_file_name( "pgal_recovery_file.sql" );
  
  ofstream recovery_file;
  
  recovery_file.open( recovery_file_name.c_str(), ios_base::app );
  
  if( recovery_file.good() ) {

    time_t tmp = time(NULL);
	char timeString[255];
	strftime( timeString, 255, "%Y-%m-%d %H:%M", localtime( &tmp ) );
	
	recovery_file << endl
		  << "# " << timeString << endl;
	
	if( e != NULL ) {
	
    	recovery_file
	      << "# Unable to perform the following query due to this exception:"
		  << endl << endl
		  << "#" << e->getMessage() << endl
		  << endl;
		
		cerr << e->getStackTrace() << endl;
		
	} else {
		
		recovery_file
		  << "# Since the database caused an exception earlier, I can't "
		     "perform the following query:" << endl
		  << endl;
		
	}
    
	recovery_file << query << ";" << endl;
	
  } else {
    
    cerr << "# Unable to open recovery file '" << recovery_file_name
	 << "' to write the following:" << endl
	 << query <<";" << endl;
    
  }
  
}

/**
 * @throw PDatabase::PDatabaseException
 */
void PDatabase::RunActionQuery( const char *query ) {
  
  if( !write_recovery_SQL_to_file ) {
  
  try {
    if ( ( ++m_Counter % MAX_CONSECUTIVE_CONNECTIONS ) == 0 ) {
      disconnect();
      connect();
    } else if ( m_Connection == NULL ) {
      connect();
    }
    
    if ( mysql_real_query( m_Connection, query, strlen( query ) ) != 0 ) {
      SmallString errMsg;
      errMsg += "Cannot perform query: \"";
      errMsg += query;
      errMsg += "\"";
      MakeExceptionMessage( errMsg );
      
      PDatabaseException e( errMsg );
      e.fillInStackTrace();
      throw e;
    }
  } catch( QException& e ) {
    
      // If a write has failed:
    
      // Set the flag so we start writing SQL queries to a file
      write_recovery_SQL_to_file = TRUE;
	  
      SaveRecoveryData( query, &e );
    
      kill( 0, SIGTERM );
    
    
  }
  } else {  // If a previous error has occurred and we're logging everything we
            // needed to write to a file:
    
    SaveRecoveryData( query );
    
  }
  
}

/**
 * @throw PDatabase::PDatabaseException
 */
PResultSet *PDatabase::RunSelectQuery( const char *query ) {
  try {
    return RunSelectQuery( query, false );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

/**
 * @throw PDatabase::PDatabaseException
 */
PResultSet* PDatabase::RunSelectQuery( const char *query, bool store ) {
  MYSQL_RES *resultSet = NULL;
  try {
    if ( ( ++m_Counter % MAX_CONSECUTIVE_CONNECTIONS ) == 0 ) {
      disconnect();
      connect();
    } else if ( m_Connection == NULL ) {
      connect();
    }
  
    int returnVal = mysql_query( m_Connection, query );
    
    if ( returnVal != 0 ) {
      SmallString errMsg;
      errMsg += "Can't perform search: \"";
      errMsg += query;
      errMsg += "\"";
      
      MakeExceptionMessage( errMsg );
      PDatabaseException e( errMsg );
      e.fillInStackTrace();
      throw e;		
    }
    
    if( store ) {
      resultSet = mysql_store_result( m_Connection );
    } else {
      resultSet = mysql_use_result( m_Connection );
    }
  
    if ( resultSet == NULL ) {
      SmallString errMsg;
      errMsg += "Can't perform search: \"";
      errMsg += query;
      errMsg += "\"";
      
      MakeExceptionMessage( errMsg );
      PDatabaseException e( errMsg );
      e.fillInStackTrace();
      throw e;
    }
  
    return new PResultSet( resultSet );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

/**
 * @throw PDatabase::PDatabaseException
 */
PResultSet* PDatabase::RunSingleRowSelectQuery( const char *query ) {
  try {
    if ( ( ++m_Counter % MAX_CONSECUTIVE_CONNECTIONS ) == 0 ) {
      disconnect();
      connect();
    } else if ( m_Connection == NULL ) {
      connect();
    }
  
    PResultSet *res = RunSelectQuery( query, true );
    
    if ( res->GetNumRows() != 1 ) {
      if ( res->GetNumRows() == 0 ) {
	SmallString errMsg;
	errMsg += "No rows found for query: \"";
	errMsg += query;
	errMsg += "\"";
      
	MakeExceptionMessage( errMsg );
	PDatabaseException e( errMsg );
	e.fillInStackTrace();
	throw e;  
      } else {
	SmallString errMsg;
	errMsg += "More than one row found for query: \"";
	errMsg += query;
	errMsg += "\"";
	
	MakeExceptionMessage( errMsg );
	PDatabaseException e( errMsg );
	e.fillInStackTrace();
	throw e;		
      }  
    }
    
    return res;
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

const char* PDatabase::EscapeString( SmallString&  dest, 
				     const char   *source,
				     int           len ) {
  if ( ( ++m_Counter % MAX_CONSECUTIVE_CONNECTIONS ) == 0 ) {
    disconnect();
    connect();
  } else if ( m_Connection == NULL ) {
    connect();
  }
  
  char *escaped = new char[ len * 2 + 1 ];
  
  mysql_real_escape_string( m_Connection, escaped, source, len );
  dest = escaped;
  delete[] escaped;

  return dest.GetStr();
}

/**
 * @throw PDatabase::PDatabaseException
 */
void PDatabase::connect() {
  unsigned int  port_num    = 0;
  char         *socket_name = NULL;
  unsigned int  flags       = 0;
  
  m_Connection = mysql_init( NULL );
  if ( m_Connection == NULL ) {
    PDatabaseException e( "mysql_init failed" );
    e.fillInStackTrace();
    throw e;
  }

  if ( mysql_real_connect( m_Connection,
			   m_Hostname,
			   m_Username,
			   m_Password,
			   m_DatabaseName,
			   port_num,
			   socket_name,
			   flags ) == NULL ) {
    SmallString errMsg;
    errMsg += "mysql_real_connect failed";
    
    MakeExceptionMessage( errMsg );
    PDatabaseException e( errMsg );
    e.fillInStackTrace();
    throw e;	
  }
}

void PDatabase::MakeExceptionMessage( SmallString& errMsg ) {
  errMsg += ": error no. ";
  errMsg += mysql_errno( m_Connection );
  errMsg += " - ";
  errMsg += mysql_error( m_Connection );
}
