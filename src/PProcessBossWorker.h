#ifndef PPROCESSBOSSWORKER_H
#define PPROCESSBOSSWORKER_H

/**
 * This implements a simple process worker. Every time a PWorker is
 * 'run', a process is forked in the background to cope with it. 
 * Each PWorker must also be an instance of PSerialisable. When
 * the task is complete, the PWorker is serialised and places in
 * a common memory area.
 *
 * Note that this works a little different from the normal
 * BossWorker pattern. This is designed to be used by sending
 * a whole load of PWorkers which it'll perform in its own time.
 */

#include "PBossWorker.h"
#include "SharedMemoryManager.h"
#include "QException.h"
#include "Serializable.h"
#include <map>
#include <queue>
#include <string>

using namespace std;

class PProcessBossWorker : public PBossWorker {
public:
  QEXCEPTIONCLASS( ChildSignalNotCaught   );
  QEXCEPTIONCLASS( ChildExitStatusNotZero );
  QEXCEPTIONCLASS( UnknownProcess         );
  QEXCEPTIONCLASS( WaitForChildError      );
  QEXCEPTIONCLASS( NoFreeBlocks           );
  QEXCEPTIONCLASS( NoJobsToRun            );
  QEXCEPTIONCLASS( CannotFork             );
  QEXCEPTIONCLASS( NotSerializable        );
  QEXCEPTIONCLASS( UnknownMagicCode       );
  QEXCEPTIONCLASS( SerializedException    );

  PProcessBossWorker( unsigned int noOfProcesses, 
		      unsigned int dataSizeInBytes );
  ~PProcessBossWorker();

  void     run( PWorker * );
  PWorker *waitForProcessToFinish( bool forceWait );
  int      getNoOfActiveProcesses() const;
  int      getNoOfFreeProcesses()   const;
  int      getNoOfFinishedWorkers() const;
  int      getNoOfThreads()         const;

protected:
  struct ProcessDetails {
    pid_t                    m_ProcessID;
    int                      m_BlockNo;
    unsigned char           *m_ParameterMemory;
    PWorker                 *m_Serializable;
  };

  unsigned int                     m_NoOfProcesses;
  map<pid_t,struct ProcessDetails> m_AllProcessDetails;
  SharedMemoryManager              m_SharedMemoryManager;
  queue<PWorker*>                  m_WaitingJobs;
  queue<unsigned int>              m_FreeBlockNos;

  void     removeProcessFromList( pid_t );
  PWorker *transformProcessID(    pid_t );
  void     setHeadOfQueueRunning();
  void     runChild( struct ProcessDetails );
};

#endif
