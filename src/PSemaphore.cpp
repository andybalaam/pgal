#include "PSemaphore.h"
#include "QException.h"
#include <string>
#include <errno.h>

using namespace std;

/**
 * Semaphore class used to make sure single access to
 * resources are possible.
 */

sembuf PSemaphore::s_SEMBUF_POST    = { 0, -1, 0 };
sembuf PSemaphore::s_SEMBUF_RELEASE = { 0,  1, 0 };

PSemaphore::PSemaphore() {
  // Initialise the semaphore...
  errno = 0;
  m_SID = semget( IPC_PRIVATE, 1, 0600 | IPC_CREAT );
  if ( m_SID == -1 ) {
    string errMsg;
    errMsg += "Cannot initialise semaphore - error states: ";
    errMsg += strerror( errno );
    
    CannotCreateSemaphore e( errMsg.c_str() );
    e.fillInStackTrace();
    throw e;
  }
}

PSemaphore::PSemaphore( bool grabStraightAway ) {
  // Initialise the semaphore...
  errno = 0;
  m_SID = semget( IPC_PRIVATE, 1, 0600 | IPC_CREAT );
  if ( m_SID == -1 ) {
    string errMsg;
    errMsg += "Cannot initialise semaphore - error states: ";
    errMsg += strerror( errno );
    
    CannotCreateSemaphore e( errMsg.c_str() );
    e.fillInStackTrace();
    throw e;
  }
  
  if ( !grabStraightAway ) {
    release();
  }
}

PSemaphore::~PSemaphore() {
  if ( m_CurrentlyLocked ) {
    release();
  }

  errno = 0;
  if ( semctl( m_SID, 1, IPC_RMID, 0 ) == -1 ) {
    string errMsg;
    errMsg += "Cannot destroy semaphore - error states: ";
    errMsg += strerror( errno );
    
    CannotDestroySemaphore e( errMsg.c_str() );
    e.fillInStackTrace();
    throw e;
  }
}

void PSemaphore::post() {
  // Now we try to lock it...
  errno = 0;
  if ( semop( m_SID, &s_SEMBUF_POST, 1 ) == -1 ) {
    string errMsg;
    errMsg += "Cannot post semaphore - error states: ";
    errMsg += strerror( errno );
    
    CannotPostSemaphore e( errMsg.c_str() );
    e.fillInStackTrace();
    throw e;
  }

  m_CurrentlyLocked = true;
}

void PSemaphore::release() {
  // Now we try to release it...
  errno = 0;
  if ( semop( m_SID, &s_SEMBUF_RELEASE, 1 ) == -1 ) {
    string errMsg;
    errMsg += "Cannot release mutex - error states: ";
    errMsg += strerror( errno );
    
    CannotReleaseSemaphore e( errMsg.c_str() );
    e.fillInStackTrace();
    throw e;
  }

  m_CurrentlyLocked = false;
}
