#ifndef SERIALIZABLE_H
#define SERIALIZABLE_H

#include <string>

using namespace std;

class Serializable {
public:
  virtual ~Serializable() { }

  virtual const unsigned char *readObject(  const unsigned char * ) = 0;
  virtual unsigned char       *writeObject( unsigned char * )       = 0;
};

#endif
