/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "TopThirdsDescriptor.h"
#include "EvolutionaryRun.h"

TopThirdsDescriptor::TopThirdsDescriptor( int populationSize ) :
    m_PopulationSize( populationSize ),
    m_RandomNumbers( RandomNumberGenerator::getDefaultGoodGenerator() ),
    m_AllFitnesses( NULL ) {
  if ( m_PopulationSize <= 2 || m_PopulationSize % 3 != 0 ) {
    SmallString msg;
    msg += "Invalid population size of ";
    msg += m_PopulationSize;
    msg += " - it must be larger than 2 and exactly divisible by 3";

    TopThirdsDescriptorException e( msg );
    e.fillInStackTrace();
    throw e;
  }

  m_AllFitnesses = new struct S_fitnesses[ m_PopulationSize ];
}

TopThirdsDescriptor::~TopThirdsDescriptor() {
  if ( m_AllFitnesses != NULL ) {
    delete m_AllFitnesses;
  }
}

void TopThirdsDescriptor::getGenotypesToSendToTrial(
    vector<unsigned long>                        *toSendToTrial,
    const map<unsigned long,AbstractGenotype *>&  population ) {
  map<unsigned long,AbstractGenotype *>::const_iterator i;

  if ( population.size() != m_PopulationSize ) {
    SmallString errMsg;
    errMsg += "Found unexpected population size of ";
    errMsg += population.size();
    errMsg += " instead of ";
    errMsg += m_PopulationSize;
 
    TopThirdsDescriptorException e( errMsg );
    e.fillInStackTrace();
    throw e;
  }

  /**
   * We'll pick the entire lot...
   */
  for ( i = population.begin(); i != population.end(); i++ ) {
    toSendToTrial->push_back( i->first );
  }
}

void TopThirdsDescriptor::getNextGenerationInformation(
    set<unsigned long>                      *toBeRemoved,
    vector<unsigned long>                   *toBeReproduced,
    const map<unsigned long,FLOAT>&          fitnesses,
    const map<unsigned long,unsigned char>&  states ) {
  /**
   * We'll order them, and make the first third reproduce twice, 
   * and the second third reproduce once. We'll fill any invalid
   * ones with the best.
   */

  /**
   * We have to quickly copy all the genotype details to
   * our area.
   */
  map<unsigned long,         FLOAT>::const_iterator i;
  map<unsigned long, unsigned char>::const_iterator k;
  int j;
  for ( j = 0, i = fitnesses.begin(); 
	i != fitnesses.end(); 
	j++, i++, k++ ) {
    k = states.find( i->first );
    m_AllFitnesses[ j ].genotypeNo = i->first;
    m_AllFitnesses[ j ].fitness    = i->second;
    m_AllFitnesses[ j ].state      = k->second;
  }

  qsort( m_AllFitnesses,
	 m_PopulationSize,
	 sizeof( struct S_fitnesses ),
	 sortFitnessStructures );

  /**
   * Now we reproduce the top third twice, and the next third
   * once.
   */
  for ( j = 0; j < m_PopulationSize / 3; j++ ) {
    k = states.find( m_AllFitnesses[ j ].genotypeNo );
    if ( k->second != EvolutionaryRun::INVALID_GENOTYPE ) {
      toBeReproduced->push_back( m_AllFitnesses[ j ].genotypeNo );
      toBeReproduced->push_back( m_AllFitnesses[ j ].genotypeNo );
    }
  }

  for ( ; j < 2 * m_PopulationSize / 3; j++ ) {
    k = states.find( m_AllFitnesses[ j ].genotypeNo );
    if ( k->second != EvolutionaryRun::INVALID_GENOTYPE ) {
      toBeReproduced->push_back( m_AllFitnesses[ j ].genotypeNo );
    }
  }

  /**
   * We have to add genotypes to be reproduced because some of them
   * may have been invalid. We do this by randomly picking non-invalid
   * genotypes until the population is the right size.
   */
  for ( j = toBeReproduced->size(); j < m_PopulationSize; j++ ) {
    int randomGenotype = m_RandomNumbers->random16BitInt( m_PopulationSize );
    while ( m_AllFitnesses[ randomGenotype ].state ==
	    EvolutionaryRun::INVALID_GENOTYPE ) {
      randomGenotype = m_RandomNumbers->random16BitInt( m_PopulationSize );
    }
    toBeReproduced->push_back( m_AllFitnesses[ randomGenotype ].genotypeNo );
  }

  /**
   * Remove all existing genotypes
   */
  for ( j = 0; j < m_PopulationSize; j++ ) {
    toBeRemoved->insert( m_AllFitnesses[ j ].genotypeNo );
  }
}

int TopThirdsDescriptor::sortFitnessStructures(
    const void *a1, 
    const void *a2 ) {
  struct S_fitnesses *s1, *s2;
  
  s1 = (struct S_fitnesses *)a1;
  s2 = (struct S_fitnesses *)a2;

  if ( s1->state == EvolutionaryRun::INVALID_GENOTYPE ) {
    return 1;
  } else if ( s2->state == EvolutionaryRun::INVALID_GENOTYPE ) {
    return -1;
  } else {
    return ( s1->fitness >= s2->fitness ) ? -1 : 1;
  }
}
