/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef ABSTRACTGENOTYPEFACTORY_H
#define ABSTRACTGENOTYPEFACTORY_H

/**
 * This class deals with creating and mutating genotypes.
 * The user needs to sub-class this to create their own
 * genotypes.
 */

#include "AbstractGenotype.h"

class AbstractGenotypeFactory {
public:
  virtual ~AbstractGenotypeFactory() { }

  virtual AbstractGenotype *
    createNewRandomGenotype() = 0;

  virtual AbstractGenotype *
    createNewMutatedGenotype( const AbstractGenotype * ) = 0;

  virtual AbstractGenotype *
    createNewCrossedGenotype( const AbstractGenotype *,
			      const AbstractGenotype * ) = 0;

  virtual AbstractGenotype *
    createGenotypeFromBits( const unsigned char *bytes,
			    int                  noOfBits ) = 0;
};

#endif
