#ifndef PSINGLEBOSSWORKER_H
#define PSINGLEBOSSWORKER_H

/**
 * This is a simple implementation of the PBossWorker interface. All
 * it does is immediately call the relevant worker straight way. There
 * is no background threads or processes used here and may be the
 * quickest on a single processor system.
 */

#include "PBossWorker.h"
#include <queue>

using namespace std;

class PSingleBossWorker : public PBossWorker {
public:
  void     run(                    PWorker *         );
  PWorker *waitForProcessToFinish( bool     forceWait );
 
  int getNoOfActiveProcesses() const { return 0;                        }
  int getNoOfFreeProcesses()   const { return 1;                        }
  int getNoOfFinishedWorkers() const { return m_FinishedWorkers.size(); }
  int getNoOfThreads()         const { return 1;                        }

protected:
  queue<PWorker *> m_FinishedWorkers;
};

#endif
