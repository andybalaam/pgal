/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "RandomNumberGenerator.h"
#include "MersenneTwisterNumberGenerator.h"
#include "FastNumberGenerator.h"
#include "QuickRandomNumbers.h"
#include <stdio.h>
#include <math.h>
#include "MersenneTwisterNumberGenerator.h"

RandomNumberGenerator *RandomNumberGenerator::s_DefaultGoodGenerator = NULL;

RandomNumberGenerator *RandomNumberGenerator::s_DefaultFastGenerator = NULL;



RandomNumberGenerator *RandomNumberGenerator::getDefaultGoodGenerator() {
  if ( s_DefaultGoodGenerator == NULL ) {
    s_DefaultGoodGenerator = new MersenneTwisterNumberGenerator;
    s_DefaultGoodGenerator->randomise();
  }

  return s_DefaultGoodGenerator;
}

RandomNumberGenerator *RandomNumberGenerator::getDefaultFastGenerator() {
  if ( s_DefaultFastGenerator == NULL ) {
    s_DefaultFastGenerator = new FastNumberGenerator;
    s_DefaultFastGenerator->randomise();
  }

  return s_DefaultFastGenerator;
}

double RandomNumberGenerator::gaussianRand() {
    static int    iset=0;
    static double gset;
    double fac,r,v1,v2;

    if( iset == 0 ) {
        do {
            v1 = 2.0 * randomDouble() - 1.0;
            v2 = 2.0 * randomDouble() - 1.0;
            r = ( v1 * v1 ) + ( v2 * v2 );
        } while ( r >= 1.0 );

        fac = sqrt( -2.0 * log( r ) / r );
        gset = v1 * fac;
        iset = 1;
        return( v2 * fac );
    } else {
        iset = 0;
        return( gset );
    }
}
