#include "AverageFitnessComparator.h"

int 
AverageFitnessComparator::compareFitness( const vector<vector<FLOAT> >& a,
					  const vector<vector<FLOAT> >& b ) {
  FLOAT averageA = calculateFitness( a );
  FLOAT averageB = calculateFitness( b );
  
  if ( averageA < averageB ) {
    return -1;
  } else if ( averageA > averageB ) {
    return 1;
  }

  return 0;
}

FLOAT 
AverageFitnessComparator::calculateFitness( const vector<vector<FLOAT> >& a ) {
  FLOAT average = 0;
  vector<vector<FLOAT> >::const_iterator i;
  vector<FLOAT>::const_iterator k;
  int j;

  for ( i = a.begin(), j = 0; i != a.end(); i++ ) {
    for ( k = i->begin(); k != i->end(); k++, j++ ) {
      average += *k;
    }
  }

  return average / (FLOAT)j;
}
