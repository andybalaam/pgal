/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "QConfigFile.h"
#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

QConfigFile::QConfigFile( const char  *filename,
			  const char **validNames,
			  const char  *beginToken,
			  const char  *endToken ) : 
  m_Filename( filename ),
  m_ValidNames( validNames ),
  m_BeginToken( beginToken ),
  m_EndToken( endToken ) {
  try {
    if ( filename == NULL || validNames == NULL ) {
      return;
    }

    /**
     * OK - this is horrible, but we quickly copy only the
     * bit of the file we're interested in into a memfile 
     * instance, and then we parse that. Nasty!
     */
    MemFile parsableFile;

    FILE *input;
    errno = 0;
    if ( ( input = fopen( filename, "r" ) ) == NULL ) {
      char errMsg[ 1024 ];
      sprintf( errMsg, "Cannot open file '%s' - error states: %s",
	       filename, strerror( errno ) );
      
      InvalidFilename e( errMsg );
      e.fillInStackTrace();
      throw e;
    }

    bool performCopy = false;
    char inputStr[ 1024 ];
    while ( fgets( inputStr, 1023, input ) != NULL ) {
      char *s = inputStr;

      for ( ; performCopy == false && *s != '\0'; s++ ) {
         
	if ( strncmp( s, 
                      (const char *)m_BeginToken, 
                      m_BeginToken.getLength() ) == 0 ) {
	  s += m_BeginToken.getLength();
	  performCopy = true;
	}
      }

      for ( ; performCopy == true && *s != '\0'; s++ ) {
 
 	if ( strncmp( s, 
                      (const char *)m_EndToken, 
                      m_EndToken.getLength() ) == 0 ) {
	  s += m_EndToken.getLength();
	  performCopy = false;
	} else {
	  parsableFile.append( (unsigned char *)s, 1L );
	}
      }
    }

    parsableFile.seek( 0L );

    /*{ // DEBUG block
      unsigned long noOfBytes;
      const unsigned char *temp = parsableFile.getMemFile( &noOfBytes );
      fprintf( stderr, "Parsable file = \'%s\'\n", temp );
    }*/

    int nameToken;
    int valueToken;
    SmallString tokenString1, tokenString2;

    nameToken = getToken( parsableFile, tokenString1 ); 
    while ( nameToken != TOKEN_END ) {
      if ( nameToken == TOKEN_STRING ) {
	char errMsg[ 1024 ];
	sprintf( errMsg, "Syntax error - I don't know the variable name '%s'",
                 (const char *)tokenString1 );
        
        SyntaxErrorException e( errMsg );
	e.fillInStackTrace();
	throw e;
      }

      valueToken = getToken( parsableFile, tokenString2 );
      if ( valueToken == TOKEN_END ) {
	char errMsg[ 1024 ];
	sprintf( errMsg,  "Unexpected end of file '%s' '%s'",
		 (const char *)tokenString1,
		 (const char *)tokenString2 );

	SyntaxErrorException e( errMsg );
	e.fillInStackTrace();
	throw e;
      }

      if ( valueToken != TOKEN_EQUALS ) {
	char errMsg[ 1024 ];
	sprintf( errMsg, "Syntax error around the token '%s'",
		 (const char *)tokenString1 );
	
	SyntaxErrorException e( errMsg );
	e.fillInStackTrace();
	throw e;
      }

      valueToken = getToken( parsableFile, tokenString2 );

      if ( valueToken == EOF ) {
	SyntaxErrorException e( "Unexpected end of file" );
	e.fillInStackTrace();
	throw e;
      }
      
      if ( valueToken != TOKEN_STRING ) {
	char errMsg[ 1024 ];
	sprintf( errMsg, "Syntax error around the tokens '%s' and '%s'",
		 (const char *)tokenString1,
		 (const char *)tokenString2 );
	
	SyntaxErrorException e( errMsg );
	e.fillInStackTrace();
	throw e;
      }

      struct S_pairing pairing;
      pairing.name  = tokenString1;
      pairing.value = tokenString2;
      m_NamesToValues.push_back( pairing );

      nameToken = getToken( parsableFile, tokenString1 ); 
    } // End while
    
    fclose( input );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

int QConfigFile::getToken( MemFile& input, SmallString& value ) {
  int ch;
  SmallString name;
  bool done = false;
  
  value = "";
  
  ch = input.mgetc();

  /**
   * First we get rid of the stuff we don't want...
   */
  while ( done == false ) {
    done = true;
    
    if ( ch == '#' ) {
      ch = input.mgetc();
      while ( ch != EOF && ch != '\n' ) {
	ch = input.mgetc();
      }
      if ( ch == '\n' ) {
	ch = input.mgetc();
      }
      done = false;
    }
    
    if ( isspace( ch ) ) {
      while ( isspace( ch ) ) {
	ch = input.mgetc();
      }
      done = false;
    }
    
    if ( ch == EOF ) {
      return TOKEN_END;
    }
  }
  
  if ( ch == '=' ) {
    value += "=";
    return TOKEN_EQUALS;
  }
  
  while ( ch != EOF && ch != '=' && ch != '\n' ) {
    value.Add( (char)ch );
    ch = input.
mgetc();
  }

  input.mungetc( ch );

  /**
   * Now we need to get rid of all the whitespace at the end of the
   * token.
   */
  if ( value.getLength() > 0 ) {
    char newVal[ 1024 ];
    strncpy( newVal, 
	     (const char *)value, 
	     value.getLength() > 1023 ? 1023 : value.getLength() );
    newVal[ value.getLength() ] = '\0';
  
    char *s = &(newVal[ strlen( newVal ) ]);
    while ( s >= newVal && ( isspace( *s ) || *s == '\0' ) ) {
      *s-- = '\0';
    }
    value = newVal;
  }

  int i;
  for ( i = 0; m_ValidNames[ i ] != NULL; i++ ) {
    if ( strcmp( m_ValidNames[ i ], 
		  (const char *)value ) == 0 ) {
      return i;
    }
  }
  
  return TOKEN_STRING;
}
