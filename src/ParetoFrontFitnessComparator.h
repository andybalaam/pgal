/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef PARETOFRONTFITNESSCOMPARATOR_H
#define PARETOFRONTFITNESSCOMPARATOR_H

/**
 * This orders the fitness vectors in a pareto front way. The fitnesses
 * are in an ordered vector and we can compare item with item.
 * The two main variables are:
 *
 * 1) Size of unordered set. One set is deemed fitter than another if
 *    *all* the items within it are larger.
 *
 * 2) Size of ordered set. One set is deemed fitter than another if
 *    each individual item is larger than the other.
 *
 * In addition, you can specify which value goes off to do the stats.
 * Either put in a positive integer or one of the variables:
 *
 * AVERAGE_UNORDERED_SET
 * AVERAGE_ALL
 */

#include "AbstractFitnessComparator.h"
#include "QException.h"
#include <vector>

using namespace std;

class ParetoFrontFitnessComparator : public AbstractFitnessComparator {
public:
  QEXCEPTIONCLASS( InvalidStatVariableException );
  QEXCEPTIONCLASS( InvalidFitnessesSize         );
  QEXCEPTIONCLASS( InvalidSetSizeException      );

  enum { AVERAGE_UNORDERED_SET = -1,
	 AVERAGE_ALL           = -2 };

  ParetoFrontFitnessComparator( int sizeOfUnorderedSet,
				int sizeOfOrderedSet,
				int statVariable = AVERAGE_ALL );
  virtual ~ParetoFrontFitnessComparator() { }

  virtual int  compareFitness(    const vector<vector<FLOAT> >& a, 
				  const vector<vector<FLOAT> >& b );

  virtual FLOAT calculateFitness( const vector<vector<FLOAT> >& );

  int getSizeOfUnorderedSet() const { return m_SizeOfUnorderedSet; }
  int getSizeOfOrderedSet()   const { return m_SizeOfOrderedSet;   }

protected:
  int         m_SizeOfUnorderedSet;
  int         m_SizeOfOrderedSet;
  vector<int> m_IndexNos;
};

#endif
