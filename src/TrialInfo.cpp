#include "TrialInfo.h"
#include <stdio.h>
#include <algorithm>
#include "PopulationMember.h"
#include "EvolutionaryRun.h"

const unsigned char TrialInfo::VALID_TRIAL   = 0x05;
const unsigned char TrialInfo::INVALID_TRIAL = 0x06;

bool TrialInfo::Trio::operator==( const Trio& trio ) const {
  if ( typeNo == trio.typeNo ) {
    return true;
  } else {
    return ( genotypeID == trio.genotypeID ) ? true : false;
  }
}

bool TrialInfo::Trio::operator<(  const Trio& trio ) const {
  if ( typeNo < trio.typeNo ) {
    return true;
  } else if ( typeNo > trio.typeNo ) {
    return false;
  } else {
    return ( genotypeID < trio.genotypeID ) ? true : false;
  }
}

bool TrialInfo::Trio::operator>(  const Trio& trio ) const {
  if ( typeNo > trio.typeNo ) {
    return true;
  } else if ( typeNo < trio.typeNo ) {
    return false;
  } else {
    return ( genotypeID > trio.genotypeID ) ? true : false;
  }
}

const unsigned char *TrialInfo::Trio::readObject( const unsigned char *s ) {
  /**
   * We have to do some fiddling about to ensure that FLOATs 
   * and ints are align properly.
   */
  memcpy( &genotypeID, s, sizeof( unsigned long ) );
  s += sizeof( unsigned long );

  memcpy( &databaseFitness, s, sizeof( FLOAT ) );
  s += sizeof( FLOAT );

  fitnesses.clear();
  int i;
  memcpy( &i, s, sizeof( int ) );
  s += sizeof( int );
  for ( ; i > 0; i-- ) {
    FLOAT tempFloat;
    memcpy( &tempFloat, s, sizeof( FLOAT ) );
    fitnesses.push_back( tempFloat );
    s += sizeof( FLOAT );
  }

  memcpy( &typeNo, s, sizeof( int ) );
  s += sizeof( int );

  state = *s;
  s++;

  return s;
}

unsigned char *TrialInfo::Trio::writeObject( unsigned char *s ) {
  memcpy( s, &genotypeID, sizeof( unsigned long ) );
  s += sizeof( unsigned long );

  memcpy( s, &databaseFitness, sizeof( FLOAT ) );
  s += sizeof( FLOAT );

  int i = fitnesses.size();
  memcpy( s, &i, sizeof( int ) );
  s += sizeof( int );

  vector<FLOAT>::const_iterator j;
  for ( j = fitnesses.begin(); j != fitnesses.end(); j++ ) {
    memcpy( s, &(*j), sizeof( FLOAT ) );
    s += sizeof( FLOAT );
  }

  memcpy( s, &typeNo, sizeof( int ) );
  s += sizeof( int );

  *s = state;
  s++;

  return s;
}

TrialInfo::TrialInfo() : m_TrialState( VALID_TRIAL ) { }

void TrialInfo::addGenotype( unsigned long genotypeID ) {
  Trio trio;
  trio.genotypeID      = genotypeID;
  trio.databaseFitness = 0;
  trio.state           = PopulationMember::UNKNOWN_FITNESS;
  trio.typeNo          = 0;

  m_GenotypeInfo.push_back( trio );
  
  sort( m_GenotypeInfo.begin(), m_GenotypeInfo.end() );
}

bool TrialInfo::getGenotypeInfo( int             num,
				 unsigned long  *genotypeID,
				 vector<FLOAT>&  fitnesses,
				 FLOAT          *databaseFitness,
				 unsigned char  *state,
				 int            *typeNo ) const {
  if ( num >= m_GenotypeInfo.size() ) {
    return false;
  }

  Trio trio = m_GenotypeInfo[ num ];

  if ( genotypeID != NULL ) {
    *genotypeID = trio.genotypeID;
  }

  if ( databaseFitness != NULL ) {
    *databaseFitness = trio.databaseFitness;
  }

  fitnesses = trio.fitnesses;

  if ( state != NULL ) {
    *state = trio.state;
  }

  if ( typeNo != NULL ) {
    *typeNo = trio.typeNo;
  }

  return true;
}

bool TrialInfo::getGenotypeInfo( int             num,
				 unsigned long  *genotypeID,
				 FLOAT          *fitness,
				 unsigned char  *state,
				 int            *typeNo ) const {
  if ( num >= m_GenotypeInfo.size() ) {
    return false;
  }

  Trio trio = m_GenotypeInfo[ num ];

  if ( genotypeID != NULL ) {
    *genotypeID = trio.genotypeID;
  }

  if ( fitness != NULL ) {
    *fitness = trio.databaseFitness;
  }

  if ( state != NULL ) {
    *state = trio.state;
  }

  if ( typeNo != NULL ) {
    *typeNo = trio.typeNo;
  }

  return true;
}

bool TrialInfo::operator<( const TrialInfo& t ) const {
  vector<Trio>::const_iterator i, j;
  for ( i =  m_GenotypeInfo.begin(), j =  t.m_GenotypeInfo.begin();
        i != m_GenotypeInfo.end() && j != t.m_GenotypeInfo.end();
        i++, j++ ) {
    if ( (*i) > (*j) ) {
      return false;
    } else if ( (*i) < (*j) ) {
      return true;
    }
  }

  /**
   * So they must be the same length thus far...
   */
  if ( m_GenotypeInfo.size() < t.m_GenotypeInfo.size() ) {
    return true;
  }

  return false;
}

bool TrialInfo::operator>( const TrialInfo& t ) const {
  if ( !( *this < t ) && !( *this == t ) ) {
    return true;
  } else {
    return false;
  }
}

bool TrialInfo::operator==( const TrialInfo& t ) const {
  if ( m_GenotypeInfo.size() != t.m_GenotypeInfo.size() ) {
    return false;
  }

  vector<Trio>::const_iterator i, j;
  for ( i =  m_GenotypeInfo.begin(), j = t.m_GenotypeInfo.begin();
        i != m_GenotypeInfo.end() && j != m_GenotypeInfo.end();
        i++, j++ ) {
    if ( !( *i == *j ) ) {
      return false;
    }
  }

  return true;
}

void TrialInfo::toString( SmallString& buffer ) const {
  buffer += "TrialInfo=\n";

  vector<Trio>::const_iterator i;
  for ( i = m_GenotypeInfo.begin(); i < m_GenotypeInfo.end(); i++ ) {
    buffer += i->genotypeID;
    buffer += "\t";
    
    vector<FLOAT>::const_iterator j;
    for ( j = i->fitnesses.begin(); j != i->fitnesses.end(); j++ ) {
      if ( j != i->fitnesses.begin() ) {
	buffer += ", ";
      }
      buffer += *j;
    }
    
    buffer += "\t";
    
    buffer += i->databaseFitness;
    buffer += "\t";

    if ( i->state == PopulationMember::KNOWN_FITNESS ) {
      buffer += "KNOWN_FITNESS\t";
    } else if ( i->state == PopulationMember::UNKNOWN_FITNESS ) {
      buffer += "UNKNOWN_FITNESS\t";
    } else if ( i->state == PopulationMember::INVALID_GENOTYPE ) {
      buffer += "INVALID_GENOTYPE\t";
    } else {
      buffer += "Don't know!\t";
    }

    buffer += i->typeNo;
    buffer += "\n";
  }
}

void TrialInfo::toBytes( MemFile& memFile ) const {
  memFile.append( &m_TrialState, sizeof( unsigned long ) );

  unsigned long noOfGenotypes = m_GenotypeInfo.size();
  memFile.append( (const unsigned char *)&noOfGenotypes, 
		  sizeof( unsigned long ) );

  int                           noOfFitnesses;
  vector<FLOAT>::const_iterator j;
  vector<Trio>::const_iterator  i;
  for ( i = m_GenotypeInfo.begin(); i != m_GenotypeInfo.end(); i++ ) {
    memFile.append( (const unsigned char *)&(i->genotypeID), 
		    sizeof( unsigned long ) );

    noOfFitnesses = i->fitnesses.size();
    memFile.append( (const unsigned char *)&noOfFitnesses,
		    sizeof( int ) );

    for ( j = i->fitnesses.begin(); j != i->fitnesses.end(); j++ ) {
      memFile.append( (const unsigned char *)&(*j),
		      sizeof( FLOAT ) );
    }

    memFile.append( (const unsigned char *)&(i->databaseFitness),
		    sizeof( FLOAT ) );

    memFile.append( (const unsigned char *)&(i->state),      
		    sizeof( unsigned char ) );

    memFile.append( (const unsigned char *)&(i->typeNo),
		    sizeof( int ) );
  }

  unsigned long sizeInBytes;
  const unsigned char *bytes = m_ExtraData.getMemFile( &sizeInBytes );
  memFile.append( (const unsigned char *)&sizeInBytes,
		  sizeof( unsigned long ) );
  memFile.append( bytes, sizeInBytes );
}

unsigned char *TrialInfo::readBytes( unsigned char *s ) {
  m_GenotypeInfo.clear();

  memcpy( &m_TrialState, s, sizeof( unsigned long ) );
  s += sizeof( unsigned long );

  unsigned long noOfGenotypes;
  memcpy( &noOfGenotypes, s, sizeof( unsigned long ) );
  s += sizeof( unsigned long );

  unsigned long i, number;
  int noOfFitnesses;
  for ( i = 0; i < noOfGenotypes; i++ ) {
    Trio trio;

    memcpy( &(trio.genotypeID), s, sizeof( unsigned long ) );
    s += sizeof( unsigned long );

    memcpy( &noOfFitnesses,     s, sizeof( int           ) );
    s += sizeof( int );

    while ( noOfFitnesses-- > 0 ) {
      trio.fitnesses.push_back( *(FLOAT *)s );
      s += sizeof( FLOAT );
    }

    memcpy( &(trio.databaseFitness), s, sizeof( FLOAT    ) );
    s += sizeof( FLOAT );

    trio.state = *s++;
   
    memcpy( &(trio.typeNo),     s, sizeof( int           ) );
    s += sizeof( int );

    m_GenotypeInfo.push_back( trio );
  }

  sort( m_GenotypeInfo.begin(), m_GenotypeInfo.end() );

  unsigned long sizeInBytes;
  memcpy( (unsigned char *)&sizeInBytes, s, sizeof( unsigned long ) );
  s += sizeof( unsigned long );

  m_ExtraData.makeEmpty();
  m_ExtraData.append( s, sizeInBytes );
  
  s += sizeInBytes;

  return s;
}

void TrialInfo::setGenotypeInfo( unsigned long        genotypeID,
				 const vector<FLOAT>& fitnesses,
				 FLOAT                databaseFitness,
				 unsigned char        state,
				 int                  typeNo ) const {
  try {
    vector<Trio>::iterator i;
    for ( i = m_GenotypeInfo.begin(); i != m_GenotypeInfo.end(); i++ ) {
      if ( i->genotypeID == genotypeID ) {
	break;
      }
    }

    if ( i == m_GenotypeInfo.end() ) {
      char errMsg[ 1024 ];
      sprintf( errMsg, "Cannot set genotype %lu",
	       genotypeID );

      EvolutionaryRun::InvalidTrial e( errMsg );
      e.fillInStackTrace();
      throw e;
    }

    i->fitnesses       = fitnesses;
    i->databaseFitness = databaseFitness;
    i->state           = state;
    i->typeNo          = typeNo;

    sort( m_GenotypeInfo.begin(), m_GenotypeInfo.end() );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

void TrialInfo::setGenotypeInfo( unsigned long        genotypeID,
				 FLOAT                fitness,
				 unsigned char        state,
				 int                  typeNo ) const {
  try {
    vector<Trio>::iterator i;
    for ( i = m_GenotypeInfo.begin(); i != m_GenotypeInfo.end(); i++ ) {
      if ( i->genotypeID == genotypeID ) {
	break;
      }
    }

    if ( i == m_GenotypeInfo.end() ) {
      char errMsg[ 1024 ];
      sprintf( errMsg, "Cannot set genotype %lu",
	       genotypeID );

      EvolutionaryRun::InvalidTrial e( errMsg );
      e.fillInStackTrace();
      throw e;
    }

    i->fitnesses.clear();
    i->fitnesses.push_back( fitness );

    i->databaseFitness = fitness;
    i->state           = state;
    i->typeNo          = typeNo;

    sort( m_GenotypeInfo.begin(), m_GenotypeInfo.end() );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

const unsigned char *
TrialInfo::getExtraData( unsigned long *sizeInBytes ) const {
  m_ExtraData.getMemFile( sizeInBytes );
}

void TrialInfo::setExtraData( const unsigned char *b,
			      unsigned long        sizeInBytes ) {
  m_ExtraData.makeEmpty();
  m_ExtraData.append( b, sizeInBytes );
}

void TrialInfo::setDatabaseFitness( unsigned long genotypeID,
				    FLOAT         fitness ) {
  try {
    vector<Trio>::iterator i;
    for ( i = m_GenotypeInfo.begin(); i != m_GenotypeInfo.end(); i++ ) {
      if ( i->genotypeID == genotypeID ) {
	break;
      }
    }

    if ( i == m_GenotypeInfo.end() ) {
      char errMsg[ 1024 ];
      sprintf( errMsg, "Cannot set genotype %lu",
	       genotypeID );

      EvolutionaryRun::InvalidTrial e( errMsg );
      e.fillInStackTrace();
      throw e;
    }

    i->databaseFitness = fitness;
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

const MemFile& TrialInfo::getExtraDataAsMemFile() const {
  return m_ExtraData;
}

const unsigned char *TrialInfo::readObject( const unsigned char *s ) {
  m_TrialState = *s++;
  
  int i;
  memcpy( &i, s, sizeof( int ) );
  s += sizeof( int );

  m_GenotypeInfo.clear();
  for ( ; i > 0; i-- ) {
    Trio tempTrio;
    s = tempTrio.readObject( s );
    m_GenotypeInfo.push_back( tempTrio );
  }

  /*{ // DEBUG block
    fprintf( stderr, "********** TrialInfo\n" );
    vector<Trio>::iterator j;
    for ( j = m_GenotypeInfo.begin(); j != m_GenotypeInfo.end(); j++ ) {
      fprintf( stderr, 
	       "Trio:\n"
	       "databaseFitness = %f\n"
	       "state           = %02x\n"
	       "typeNo          = %d\n"
	       "genotypeID      = %lu\n",
	       j->databaseFitness,
	       j->state,
	       j->typeNo,
	       j->genotypeID );
      vector<FLOAT>::iterator k;
      for ( k = j->fitnesses.begin(); k != j->fitnesses.end(); k++ ) {
	fprintf( stderr, "    sub fitness = %f\n", *k );
      }
    
      fprintf( stderr, "\n\n" );
    }
    }*/
	     
  return s;
}

unsigned char *TrialInfo::writeObject( unsigned char *s ) {
  *s = m_TrialState;
  s++;

  int i = m_GenotypeInfo.size();
  memcpy( s, &i, sizeof( int ) );
  s += sizeof( int );

  vector<Trio>::iterator j;
  for ( j = m_GenotypeInfo.begin(); j != m_GenotypeInfo.end(); j++ ) {
    s = j->writeObject( s );
  }
}
