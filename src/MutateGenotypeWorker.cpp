#include "MutateGenotypeWorker.h"
#include "QErrorLog.h"
#include <string>

using namespace std;

MemoryPool MutateGenotypeWorker::s_Pool( sizeof( MutateGenotypeWorker ), 50 );

void MutateGenotypeWorker::start() {
  try {
    m_NewGenotype = m_Factory->createNewMutatedGenotype( m_Genotype );
  } catch( QException& e ) {
    e.fillInStackTrace();

    string errMsg;
    errMsg += "Caught exception in thread - ";
    errMsg += e.toString();
    errMsg += "\n";
    errMsg += e.getStackTrace();
    QErrorLog::writeError( errMsg.c_str() );
    
    throw;
  }
}
