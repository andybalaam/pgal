/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef PDATABASE_H
#define PDATABASE_H

/**
 * A general MySQL database access class.
 */

#include "QException.h"
#include <mysql/mysql.h>

class PResultSet;
class SmallString;

class PDatabase {
public:
  QEXCEPTIONCLASS( PDatabaseException );
  
  /**
   * Constructor - supply the name, password etc.
   * Does not connect to the database until needed.
   */
  PDatabase( const char *username,
	     const char *password,
	     const char *hostname,
	     const char *databaseName );
  
  virtual ~PDatabase();
  
  /**
   * Run a query, not storing the result
   *
   * @throw PDatabase::PDatabaseException
   */
  PResultSet *RunSelectQuery( const char *query );
  
  /**
   * Run a query, storing the result if store is true.
   *
   * @throw PDatabase::PDatabaseException
   */
  PResultSet *RunSelectQuery( const char *query, bool store );
  
  /**
   * Run a query which is expected to contain only one row in its result set
   *
   * @throw PDatabase::PDatabaseException
   */
  PResultSet* RunSingleRowSelectQuery( const char *query );
  
  /**
   * Performs an INSERT query and returns the id of the inserted record.
   *
   * @throw PDatabase::PDatabaseException
   */
  long RunInsertQuery( const char *query );
  
  /**
   * Appends an error message and the unexecuted SQL to a recovery file
   */
  void SaveRecoveryData( const char *query, QException* e );
  /**
   * Appends the unexecuted SQL to a recovery file
   */
  void SaveRecoveryData( const char *query );
  
  /**
   * Performs an INSERT, UPDATE or DELETE query and provides no return value.
   *
   * @throw PDatabase::PDatabaseException
   */
  void RunActionQuery( const char *query );
  
  /**
   * Fills the supplied destination SmallString with a copy of the source
   * SmallString with characters escaped to be a valid MySQL string for
   * use in a query.
   *
   * @throw PDatabase::PDatabaseException
   * @return 	a pointer to the char array of destination SmallString
   */
  const char* EscapeString( SmallString& dest, 
			    const char  *source,
			    int          len );
	
protected:
  /**
   * @throw PDatabase::PDatabaseException
   */
  void connect();
  void disconnect();

  void MakeExceptionMessage( SmallString& msg );
  
  MYSQL       *m_Connection;
  SmallString  m_Username;
  SmallString  m_Password;
  SmallString  m_Hostname;
  SmallString  m_DatabaseName;
  unsigned int m_Counter;
  
  bool write_recovery_SQL_to_file;
  
};

#endif
