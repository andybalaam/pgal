/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "GenotypeDatabase.h"
#include "EvolutionaryRun.h"
#include "bitfield.h"
#include <string.h>
#include <time.h>
#include "PResultSet.h"
#include "PRow.h"
#include "QErrorLog.h"
#include <set>

using namespace std;

#define DEFAULT_NO_OF_RECORDS 1000

GenotypeDatabase::GenotypeDatabase( const char *username,
                    const char *password,
                    const char *hostname,
                    const char *databaseName ) :
                                    PDatabase( username, 
                           password, 
                           hostname, 
                           databaseName ) { }

unsigned long GenotypeDatabase::createEvolutionaryRunRecord( 
    unsigned long  initialPopulationSize,
    const char    *programVersion ) {
  try {
    SmallString   escapedVersion;
    
    EscapeString( escapedVersion, 
          programVersion,
          strlen( programVersion ) );
    
    SmallString query;
    query = "INSERT INTO evolutionaryRun      "
            "(initialPopulationSize, programVersion) "
            "VALUES(";
    query += initialPopulationSize;
    query += ", '";
    query += escapedVersion;
    query += "')";
    
    return RunInsertQuery( query );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

unsigned long GenotypeDatabase::createEvolutionaryPartRunRecord( 
    unsigned long  evolutionaryRunID,
    const char    *notes ) {
  try {
    SmallString escapedNotes;
    
    EscapeString( escapedNotes, 
          notes,
          strlen( notes ) );
    
    SmallString query;
    query = "INSERT INTO evolutionaryPartRun     "
            "(evolutionaryRunID, startTime, notes) "
            "VALUES(";
    query += evolutionaryRunID;
    query += ", null, '";
    query += escapedNotes;
    query += "')";
    
    return RunInsertQuery( query );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

unsigned long GenotypeDatabase::createTimePeriodInfoRecord(
    unsigned long evolutionaryPartRunID,
    unsigned long timePeriodSeconds,
    unsigned long noOfGenerations ) {
  try {
    char query[ 1024 ];
    sprintf( query, 
             "INSERT INTO timePeriodInfo "
             "( evolutionaryPartRunID,   "
             "  timePeriodSeconds,       "
             "  noOfGenerations,         "
             "  realTime )               "
             "VALUES( %lu,%lu,%lu,null)",
             evolutionaryPartRunID,
             timePeriodSeconds,
             noOfGenerations );
    
    return RunInsertQuery( query );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

unsigned long 
GenotypeDatabase::createGenerationRecord( unsigned long generationNo,
                      unsigned long evolutionaryPartRun ) {
  try {
    char query[ 1024 ];
    sprintf( query, 
             "INSERT INTO generation "
             "(generationNo, evolutionaryPartRunID,timeStamp) "
             "VALUES(%lu,%lu,null)",
             generationNo,
             evolutionaryPartRun );
    
    return RunInsertQuery( query );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

unsigned long
GenotypeDatabase::createTrialRecord( const TrialInfo& trialInfo,
                     unsigned long    generationID ) {
  try {
    char query[ 1024 ];
    sprintf( query,
	     "INSERT INTO trial "
             "(generationID,flags) "
             "VALUES (%lu,%lu)",
	     generationID,
	     (unsigned long)trialInfo.getTrialState() );
  
    unsigned long trialID = RunInsertQuery( query );

    int i;
    unsigned long genotypeID;
    FLOAT         fitness;
    unsigned char state;
    int           typeNo;
    char number[ 100 ];

    for ( i = 0; 
	  trialInfo.getGenotypeInfo( i, 
				     &genotypeID, 
				     &fitness, 
				     &state, 
				     &typeNo );
	  i++ ) {
      sprintf( query,
	       "INSERT INTO trialGenotype "
	       "(trialID,genotypeID,fitness,flags,generationID) "
	       "VALUES (%lu,%lu,%.10f,%lu,%lu)",
	       trialID,
	       genotypeID,
	       fitness,
	       (unsigned long)state,
	       generationID );
    
      RunInsertQuery( query );
    }

    return trialID;
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw e;
  }
}

unsigned long GenotypeDatabase::loadLatestGeneration( 
    unsigned long                         evolutionaryRun,
    AbstractGenotypeFactory              *genotypeFactory,
    unsigned long                        *generationNo,
    map<unsigned long, PopulationMember> *population ) {
  try {
    return loadPopulationAtGeneration( -1, 
                       evolutionaryRun,
                       genotypeFactory,
                       generationNo,
                       population );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw e;
  }
}

unsigned long GenotypeDatabase::loadPopulationAtGeneration( 
    long                                  generationNo,
    unsigned long                         evolutionaryRunID,
    AbstractGenotypeFactory              *genotypeFactory,
    unsigned long                        *newGenerationNo,
    map<unsigned long, PopulationMember> *population ) {
  /**
   * We load a particular population by loading every genotype
   * where 'generation' is larger than the firstGeneration
   * field, but smaller or equal to the lastGeneration
   * field or where lastGeneration field is set to null.
   *
   * Then we load the fitnesses and states from all the latest
   * trial records for these genotypes.
   */

  PResultSet *resultSet  = NULL;
  PResultSet *resultSet2 = NULL;
  PRow       *row        = NULL;
  PRow       *row2       = NULL;
  
  unsigned long populationSize;

  try {
    if( ( genotypeFactory != NULL ) && ( population != NULL ) ) {
      population->clear();  
    }
    
    SmallString query;
    
    if ( generationNo == -1 ) {
      /**
       * Right, we have to find the latest generation for this
       * evolutionary run
       */
      query =  "SELECT generation.generationNo                               "
               "FROM generation                                              "
               "LEFT JOIN evolutionaryPartRun                                "
               "ON generation.evolutionaryPartRunID=evolutionaryPartRun.ID   "
               "WHERE evolutionaryPartRun.evolutionaryRunID=                 ";
      query += evolutionaryRunID;
      query += " ORDER BY generationNo DESC "
           "LIMIT 1";

      resultSet = RunSingleRowSelectQuery( query );
      
      /**
       * We pick the first (the only!) item
       */
      row = resultSet->GetNextRow();
      generationNo = row->GetLongField( 0L );

      *newGenerationNo = generationNo;
      
      delete row;       row = NULL;
      delete resultSet; resultSet = NULL;
    }
    
    query = "SELECT genotype.ID,                                           "
            "       genotype.typeID                                        ";
       
    if( genotypeFactory != NULL ) {
      query += "      ,genotypeValue,                                      "
               "       sizeInBits                                          ";
    }

    query += "FROM genotype                                                "
             "LEFT JOIN evolutionaryPartRun                                "
             "  ON genotype.evolutionaryPartRunID = evolutionaryPartRun.ID "
             "WHERE evolutionaryPartRun.evolutionaryRunID =                ";
    query += evolutionaryRunID;
    query += " AND genotype.firstGenerationNo <=                           ";
    query += generationNo;
    query += " AND (genotype.lastGenerationNo >                            ";
    query += generationNo;
    query += " OR genotype.lastGenerationNo IS null)                       ";

    /**
     * Now we load in these genotypes.
     */
    resultSet = RunSelectQuery( query, true );
    
    populationSize = resultSet->GetNumRows();
    
    if ( populationSize == 0 ) {
      SmallString errMsg;
      errMsg += "Cannot find any genotypes from evolutionary run ";
      errMsg += evolutionaryRunID;
      MakeExceptionMessage( errMsg );
      
      PDatabaseException e( errMsg );
      e.fillInStackTrace();
      throw e;
    }
    
    map<unsigned long, PopulationMember>::iterator j;
    int i;
    for ( i = 0; i < populationSize; i++ ) {
      row = resultSet->GetNextRow();
      
      unsigned long genotypeID   = row->GetLongField( 0L );
      unsigned long genotypeType = row->GetLongField( 1 );
      
      AbstractGenotype *genotype;
      if( genotypeFactory != NULL ) {
	
        unsigned char *bytes       = (unsigned char *)row->GetStringField( 2 );
        unsigned long  noOfBits    = row->GetLongField( 3 );
	
        /**
	 * Now we create a genotype instance.
	 */
        genotype = genotypeFactory->createGenotypeFromBits( bytes, noOfBits );
        
	genotype->setIndexNo( genotypeID   );
        genotype->setType(    genotypeType );
	
	j = population->find( genotypeID );
        
        if ( j != population->end() ) {
          SmallString errMsg;
          errMsg += "Found two genotypes with the same index no (";
          errMsg += genotypeID;
          errMsg += ")";
          
	  PDatabaseException e( errMsg );
          e.fillInStackTrace();
          throw e;
        }
      } else {
        genotype = NULL;  
      }
      
      /**
       * Get the fitness if possible
       */
      query =  "SELECT flags,      "
	       "       fitness     "
	       "FROM trialGenotype "
	       "WHERE genotypeID = ";
      query += genotypeID;
      query += " ORDER BY ID DESC LIMIT 1";
      
      resultSet2 = RunSelectQuery( query, true );
      
      unsigned long noTrials = resultSet2->GetNumRows();
      
      unsigned char flags;
      FLOAT         fitness;
      
      if ( noTrials == 1 ) {
	row2 = resultSet2->GetNextRow();
	
	flags      = (unsigned char)row2->GetLongField( 0L );
	fitness    = row2->GetFloatField(               1L );
	
	delete row2; row2 = NULL;
      } else if ( noTrials == 0 ) {
	flags   = PopulationMember::UNKNOWN_FITNESS;
        fitness = 0;          
      }
      
      /**
       * Now let's add it to the population!
       */
      PopulationMember populationMember( genotypeID,
					 genotype,
					 flags );
      populationMember.setFitness( fitness );
      population->insert( 
          pair<unsigned long,PopulationMember>( genotypeID,
						populationMember ) 
	  );
      
      delete resultSet2; resultSet2 = NULL;
      delete row; row = NULL;
    } // End for i = all in population
    
    delete resultSet; resultSet = NULL;
  } catch( QException& e ) {
    if ( resultSet != NULL ) {
      delete resultSet;
    }
    
    if ( row != NULL ) {
      delete row;
    }
    
    if ( resultSet2 != NULL ) {
      delete resultSet2;
    }

    if ( row2 != NULL ) {
      delete row2;
    }

    e.fillInStackTrace();
    throw;
  }

  return populationSize;
}

unsigned long GenotypeDatabase::loadPopulationAtGeneration( 
    long                         generationNo,
    unsigned long                evolutionaryRunID,
    map<unsigned long, MemFile> *genotypeValues,
    map<unsigned long, int>     *sizeInBits,
    map<unsigned long, int>     *typeNos ) {
  /**
   * We load a particular population by loading every genotype
   * where 'generation' is larger than the firstGeneration
   * field, but smaller or equal to the lastGeneration
   * field or where lastGeneration field is set to null.
   *
   * Then we load the fitnesses and states from all the latest
   * trial records for these genotypes.
   */

  PResultSet *resultSet  = NULL;
  PRow       *row        = NULL;
  
  unsigned long populationSize;

  try {
    genotypeValues->clear();  
    sizeInBits->clear();
    typeNos->clear();

    SmallString query;
    
    if ( generationNo == -1 ) {
      /**
       * Right, we have to find the latest generation for this
       * evolutionary run
       */
      query =  "SELECT generation.generationNo                               "
               "FROM generation                                              "
               "LEFT JOIN evolutionaryPartRun                                "
               "ON generation.evolutionaryPartRunID=evolutionaryPartRun.ID   "
               "WHERE evolutionaryPartRun.evolutionaryRunID=                 ";
      query += evolutionaryRunID;
      query += " ORDER BY generationNo DESC "
           "LIMIT 1";

      resultSet = RunSingleRowSelectQuery( query );
      
      /**
       * We pick the first (the only!) item
       */
      row = resultSet->GetNextRow();
      generationNo = row->GetLongField( 0L );

      delete row;       row = NULL;
      delete resultSet; resultSet = NULL;
    }
    
    query = "SELECT genotype.ID,                                          "
            "       genotype.typeID,                                      "
            "       genotype.genotypeValue,                               "
        "       genotype.sizeInBits                                   "
            "FROM genotype                                                "
            "LEFT JOIN evolutionaryPartRun                                "
            "  ON genotype.evolutionaryPartRunID = evolutionaryPartRun.ID "
            "WHERE evolutionaryPartRun.evolutionaryRunID =                ";
    query += evolutionaryRunID;
    query += " AND genotype.firstGenerationNo <=                          ";
    query += generationNo;
    query += " AND (genotype.lastGenerationNo >                           ";
    query += generationNo;
    query += " OR genotype.lastGenerationNo IS null)                      ";
    
    /**
     * Now we load in these genotypes.
     */
    resultSet = RunSelectQuery( query, true );
    
    populationSize = resultSet->GetNumRows();
    
    if ( populationSize == 0 ) {
      SmallString errMsg;
      errMsg += "Cannot find any genotypes from evolutionary run ";
      errMsg += evolutionaryRunID;
      MakeExceptionMessage( errMsg );
      
      PDatabaseException e( errMsg );
      e.fillInStackTrace();
      throw e;
    }
    
    int i;
    for ( i = 0; i < populationSize; i++ ) {
      row = resultSet->GetNextRow();
      
      unsigned long genotypeID   = row->GetLongField( 0L );
      unsigned long genotypeType = row->GetLongField( 1 );
      unsigned char *bytes       = (unsigned char *)row->GetStringField( 2 );
      unsigned long  noOfBits    = row->GetLongField( 3 );
      
      MemFile value;
      value.append( bytes, BYTES_IN_BITFIELD( noOfBits ) );
      genotypeValues->insert( pair<unsigned long,MemFile>(genotypeID,value) );
      sizeInBits->insert( pair<unsigned long,int>(genotypeID,(int)noOfBits));
      typeNos->insert( pair<unsigned long,int>( genotypeID, genotypeType ) );

      delete row; row = NULL;
    } // End for i = all in population
  
    delete resultSet; resultSet = NULL;
  } catch( QException& e ) {
    if ( resultSet != NULL ) {
      delete resultSet;
    }

    if ( row != NULL ) {
      delete row;
    }
    
    e.fillInStackTrace();
    throw;
  }

  return populationSize;
}

/**
 * Return the total number of generations in this evolutionary run so far.
 */
unsigned long GenotypeDatabase::findHowManyGenerations( 
    unsigned long evolutionaryRunID ) {

  unsigned long  numGens;
  PResultSet    *res;
  PRow          *row;
  SmallString    query;
  
  query = "SELECT MAX(generationNo) FROM generation INNER JOIN "
          "evolutionaryPartRun ON evolutionaryPartRunID = "
          "evolutionaryPartRun.ID "
          "WHERE evolutionaryRunID = ";
  query += evolutionaryRunID;
  
  res = RunSingleRowSelectQuery( query );
  row = res->GetNextRow();
  
  numGens = row->GetLongField( 0L );
  
  delete row;
  delete res;
  
  return numGens;
  
}

/**
 * Get a list of all the evolutionary runs that have taken place, and put their
 * start and finish dates/times into the given variable (as strings).
 */
unsigned long *
GenotypeDatabase::findAllEvolutionaryRuns( unsigned long  *numberOfRuns,
                       SmallString      **startDates,
                       SmallString      **endDates,
                       SmallString      **notes) {
  PResultSet    *res                     = NULL;
  PRow          *row                     = NULL;
  unsigned long *indexOfEvolutionaryRuns = NULL;

  *startDates = NULL;
  *endDates   = NULL;
  *notes      = NULL;

  try {
    unsigned long  numRows;
    unsigned long  i;
    SmallString    query;
    
    query = "SELECT evolutionaryRunID,  "
            "       MIN(startTime),   "
            "       MAX(endTime),     "
        "       notes             "
        "FROM evolutionaryPartRun "
        "GROUP BY evolutionaryRunID ";
  
    res = RunSelectQuery( query, true );
    
    numRows = res->GetNumRows();
    
    indexOfEvolutionaryRuns = new unsigned long[ numRows ];
    *startDates             = new SmallString[   numRows ];
    *endDates               = new SmallString[   numRows ];
    *notes                  = new SmallString[   numRows ];
    *numberOfRuns           = numRows;
  
    for ( i = 0; i < numRows; i++ ) {
      row = res->GetNextRow();
      
      indexOfEvolutionaryRuns[i] = row->GetLongField(   0L );
      (*startDates)[i]           = row->GetStringField( 1L );
      (*endDates)[i]             = row->GetStringField( 2L );
      (*notes)[i]            = row->GetStringField( 3L );
      
      delete row; row = NULL;  
    }
    
    delete res; res = NULL;
  } catch( QException& e ) {
    if ( row != NULL ) {
      delete row;
    }

    if ( res != NULL ) {
      delete res;
    }

    if ( *notes != NULL ) {
      delete[] notes;
    }

    if ( *startDates != NULL ) {
      delete[] startDates;
    }

    if ( *endDates != NULL ) {
      delete[] endDates;
    }

    if ( indexOfEvolutionaryRuns != NULL ) {
      delete indexOfEvolutionaryRuns;
    }

    e.fillInStackTrace();
    throw;
  }

  if ( row != NULL ) {
    delete row;
  }
  
  if ( res != NULL ) {
    delete res;
  }
  
  return indexOfEvolutionaryRuns;
}

unsigned long *GenotypeDatabase::findAllEvolutionaryPartRuns( 
    unsigned long   evolutionaryRunID,
    unsigned long  *numberOfParts,
    unsigned long  *populationSize,
    SmallString   **startDates,
    SmallString   **endDates,
    SmallString   **notes) {
  unsigned long *indexOfPartRuns = NULL;
  PResultSet    *res             = NULL;
  PRow          *row             = NULL;
  
  *startDates = NULL;
  *endDates   = NULL;
  *notes      = NULL;

  try {
    unsigned long i;
    unsigned long numParts;
    SmallString   query;
    
    query = "SELECT initialPopulationSize "
            "FROM evolutionaryRun  "
            "WHERE ID =            ";
    query += evolutionaryRunID;
    
    res = RunSingleRowSelectQuery( query );
    
    row = res->GetNextRow();
    
    *populationSize = row->GetLongField( 0L );
    
    delete row; row = NULL;
    delete res; res = NULL;
    
    query = "SELECT ID,                "
            "       startTime,         "
            "       endTime,           "
            "       notes              "
            "FROM evolutionaryPartRun  "
            "WHERE evolutionaryRunID = ";
    query += evolutionaryRunID; 
    
    res = RunSelectQuery( query, true );
    
    numParts = res->GetNumRows();
    
    *numberOfParts = numParts;
    
    indexOfPartRuns = new unsigned long[ numParts ];
    *startDates     = new SmallString[   numParts ];
    *endDates       = new SmallString[   numParts ];
    *notes          = new SmallString[   numParts ];
    
    for( i = 0; i < numParts; i++ ) {
      row = res->GetNextRow();
      
      indexOfPartRuns[i] = row->GetLongField(   0L );
      (*startDates)[i]   = row->GetStringField( 1L );
      (*endDates)[i]     = row->GetStringField( 2L );
      (*notes)[i]        = row->GetStringField( 3L );
      
      delete row; row = NULL;
    }
    
    delete res; res = NULL;
  } catch( QException& e ) {
    if ( row != NULL ) {
      delete row;
    }

    if ( res != NULL ) {
      delete res;
    }

    if ( *startDates != NULL ) {
      delete[] *startDates;
    }

    if ( *endDates != NULL ) {
      delete[] *endDates;
    }

    if ( *notes != NULL ) {
      delete[] *notes;
    }

    if ( indexOfPartRuns != NULL ) {
      delete indexOfPartRuns;
    }

    e.fillInStackTrace();
    throw;
  }

  if ( row != NULL ) {
    delete row;
  }
  
  if ( res != NULL ) {
    delete res;
  }
  
  return indexOfPartRuns;    
}

unsigned char *
GenotypeDatabase::loadGenotype( unsigned long    genotypeID,
                unsigned long   *sizeInBits,
                unsigned long   *typeID ) {
  PResultSet    *res           = NULL;
  PRow          *row           = NULL;
  unsigned char *bytesToReturn = NULL;

  try {
    SmallString    query;
    
    query = "SELECT genotypeValue, "
            "       sizeInBits,    "
            "       typeID         "
            "FROM genotype         "
            "WHERE ID =            ";
    query += genotypeID;
    
    res = RunSingleRowSelectQuery( query );
    
    row = res->GetNextRow();
    
    *sizeInBits = row->GetLongField( 1 );
    *typeID     = row->GetLongField( 2 );

    bytesToReturn = new unsigned char[ BYTES_IN_BITFIELD( *sizeInBits ) ];
    memcpy( bytesToReturn, row->GetStringField( 0L ),
        BYTES_IN_BITFIELD( *sizeInBits ) );
    
    delete row; row = NULL;
    delete res; res = NULL;
  } catch( QException& e ) {
    if ( row != NULL ) {
      delete row;
    }

    if ( res != NULL ) {
      delete res;
    }

    if ( bytesToReturn != NULL ) {
      delete bytesToReturn;
    }

    e.fillInStackTrace();
    throw;
  }

  if ( row != NULL ) {
    delete row;
  }
  
  if ( res != NULL ) {
    delete res;
  }
  
  return bytesToReturn;
}  

void GenotypeDatabase::finishEvolutionaryPartRun( 
    unsigned long evolutionaryPartRunID ) {
  try {
    SmallString query;
    query += "UPDATE evolutionaryPartRun "
             "SET endTime = NOW()        "
             "WHERE ID =                 ";
    query += evolutionaryPartRunID;

    RunActionQuery( query );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

unsigned long GenotypeDatabase::addGenotype( 
    const AbstractGenotype *genotype,
    unsigned long           evolutionaryPartRun,
    unsigned long           generation,
    unsigned long           parent,
    bool                    addBody ) {
  try {
    unsigned long sizeInBits;        
    const unsigned char *bytes;
    char *buf;
    
    bytes = genotype->getGenotypeAsBits( &sizeInBits );
     
    if ( addBody == true ) { 
      // FIXME - should this be inside a new EscapeBytes method?
      
      genotype->setIsSaved( TRUE );
      
      buf = new char[ 2*BYTES_IN_BITFIELD( sizeInBits ) +  10 ];
      
      mysql_escape_string( buf, 
               (char *)bytes, 
               BYTES_IN_BITFIELD( sizeInBits ) );
    }

    // /FIXME
    string query;
    query += "INSERT INTO genotype ( evolutionaryPartRunID, "
             "                       genotypeValue,         "
             "                       parentID,              "
             "                       sizeInBits,            "
             "                       typeID,                "
             "                       firstGenerationNo )    "
             "VALUES( ";
    char number[ 100 ];
    sprintf( number, "%lu,", evolutionaryPartRun );
    query += number;
    
    if ( addBody == true ) {
      query += "'"; 
      query += buf;                 
      query += "', ";

      delete[] buf;
    } else {
      query += "null,";
    }

    if ( parent == ULONG_MAX ) {
      query += "null,";
    } else {
      sprintf( number, "%lu,", parent );
      query += number;
    }
    
    sprintf( number, "%lu,", sizeInBits );
    query += number;

    sprintf( number, "%lu,", genotype->getType() );
    query += number;

    sprintf( number, "%lu )", generation );
    query += number;
    
    return RunInsertQuery( query.c_str() );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

void 
GenotypeDatabase::setLastGenerationForGenotype( unsigned long genotypeNo,
                        unsigned long lastGeneration) {
  /**
   * This sets the lastGeneration field in the genotype database. It marks
   * the last generation a genotype exists in.
   */
  try {
    char query[ 1024 ];
    sprintf( query, "UPDATE genotype SET lastGenerationNo = %lu "
                    "WHERE ID = %lu",
             lastGeneration, genotypeNo );
    
    RunInsertQuery( query );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

void GenotypeDatabase::addQuartileData( unsigned long  generationID,
					const FLOAT   *quartiles ) {
  try {
    /**
     * This method adds the given quartile data into the 
     * generation table.
     */
    string query;
    char number[ 1024 ];
    query += "UPDATE generation "
             "SET quartile1 = "; 
    
    sprintf( number, "%.10f", quartiles[ 0 ] );
    query += number;

    sprintf( number, "%.10f", quartiles[ 1 ] );
    query += ",   quartile2 = ";  query += number;
 
    sprintf( number, "%.10f", quartiles[ 2 ] );
    query += ",   quartile3 = ";  query += number;

    sprintf( number, "%.10f", quartiles[ 3 ] );
    query += ",   quartile4 = ";  query += number;

    sprintf( number, "%.10f", quartiles[ 4 ] );
    query += ",   quartile5 = ";  query += number;

    sprintf( number, "%.10f", quartiles[ 5 ] );
    query += ",   quartile6 = ";  query += number;

    sprintf( number, "%.10f", quartiles[ 6 ] );
    query += ",   quartile7 = ";  query += number;
 
    sprintf( number, "%.10f", quartiles[ 7 ] );
    query += ",   quartile8 = ";  query += number;

    sprintf( number, "%.10f", quartiles[ 8 ] );
    query += ",   quartile9 = ";  query += number;

    sprintf( number, "%.10f", quartiles[ 9 ] );
    query += ",   quartile10 = "; query += number;

    query += " WHERE ID = ";
    sprintf( number, "%lu", generationID );
    query += number;
    
    RunInsertQuery( query.c_str() );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

void GenotypeDatabase::addExtraQuartileData( unsigned long  generationID,
                    const unsigned int fitnessNo,
					const FLOAT       *quartiles ) {
  try {
    /**
     * This method adds the given quartile data into the 
     * generation table.
     */
    string query;
    char number[ 1024 ];
    query += "INSERT INTO generationFitnesses ( "
             "generationID, fitnessNo, "
             "quartile1, quartile2, quartile3, quartile4, quartile5, "
             "quartile6, quartile7, quartile8, quartile9, quartile10 "
             " ) VALUES ( ";

    sprintf( number, "%lu,", generationID );
    query += number;

    sprintf( number, "%u,", fitnessNo );
    query += number;
             
    sprintf( number, "%.10f,", quartiles[ 0 ] ); query += number; 
    sprintf( number, "%.10f,", quartiles[ 1 ] ); query += number;
    sprintf( number, "%.10f,", quartiles[ 2 ] ); query += number;
    sprintf( number, "%.10f,", quartiles[ 3 ] ); query += number;
    sprintf( number, "%.10f,", quartiles[ 4 ] ); query += number;
    sprintf( number, "%.10f,", quartiles[ 5 ] ); query += number;
    sprintf( number, "%.10f,", quartiles[ 6 ] ); query += number;
    sprintf( number, "%.10f,", quartiles[ 7 ] ); query += number;
    sprintf( number, "%.10f,", quartiles[ 8 ] ); query += number;
    sprintf( number, "%.10f)", quartiles[ 9 ] ); query += number;
    
    RunInsertQuery( query.c_str() );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

void GenotypeDatabase::findLatestGenotypesFromEvolutionaryPartRun( 
    map<unsigned long,vector<FLOAT> >& IDToFitness,
    map<unsigned long,bool>&           IDToHasBodyFlag,
    map<unsigned long,unsigned long>&  IDToGenerationNo, 
    map<unsigned long,unsigned long>&  IDToType,
    map<unsigned long,MemFile>&        IDToGenotypeBodies,
    int                                evolutionaryPartRunID, 
    int                                numberInList,
    bool                               bodyNotNull,
    bool                               loadGenotypeBodies ) {

  unsigned long lastNoOfFoundRecords = DEFAULT_NO_OF_RECORDS * 2, 
                noOfFoundRecords;
  try {
    bool stillHaveRecords = true;

    IDToFitness.clear();
    IDToHasBodyFlag.clear();
    IDToGenerationNo.clear();
    int recordIDNumber;
    if ( bodyNotNull == true ) {
      recordIDNumber = DEFAULT_NO_OF_RECORDS * 5;
    } else {
      recordIDNumber = numberInList * 5;
    }

    map<unsigned long,FLOAT> tempFitnesses;
    while ( IDToFitness.size() < numberInList &&
        stillHaveRecords == true ) {
      SmallString query;
      query = "SELECT genotypeID,                                       " 
              "       fitness,                                          "
              "       generationNo                                      "
              "FROM trialGenotype                                       "
          "LEFT JOIN generation                                     "
              "ON trialGenotype.generationID=generation.ID              "
          "WHERE evolutionaryPartRunID=";
      
      query += evolutionaryPartRunID;

      query += " ORDER BY trialGenotype.ID DESC LIMIT ";
      query += recordIDNumber;
      
      PResultSet *resultSet = RunSelectQuery( query, true );

      noOfFoundRecords = resultSet->GetNumRows();

      if ( noOfFoundRecords == 0 &&
       IDToFitness.size() == 0 ) {
    SmallString errMsg;
    errMsg += "EvolutionaryPartRun ID of ";
    errMsg += evolutionaryPartRunID;
    errMsg += " is invalid";
    
    InvalidEvolutionaryPartRunID e( errMsg );
    e.fillInStackTrace();
    throw e;
      }

      if ( noOfFoundRecords == 0 ||
       noOfFoundRecords == lastNoOfFoundRecords ) {
    stillHaveRecords = false;
      } else {
    PRow         *row;
    int           i;
    unsigned long id, generationNo;
    FLOAT         fitness;
    map<unsigned long,FLOAT>::const_iterator p;
     
    for ( i = 0; 
          i < resultSet->GetNumRows();
          i++ ) {
      row          = resultSet->GetNextRow();
      
      id           = row->GetLongField(  0L );
      fitness      = row->GetFloatField( 1L );
      generationNo = row->GetLongField(  2L );

      p = tempFitnesses.find( id );
      if ( p == tempFitnesses.end() ) {
        vector<FLOAT> tempFitnessVector;
        tempFitnessVector.push_back( fitness );
        IDToFitness.insert( 
            pair<unsigned long,vector<FLOAT> >( id, tempFitnessVector ) );

        tempFitnesses.insert( pair<unsigned long,FLOAT>( id, fitness ) );
        IDToGenerationNo.insert( 
              pair<unsigned long,unsigned long>( id, generationNo ) );
             } else {
        map<unsigned long,vector<FLOAT> >::iterator k;
        k = IDToFitness.find( id );
        if ( k == IDToFitness.end() ) {
          /**
           * OK - we just add it all in.
           */
          vector<FLOAT> tempFitnessVector;
          tempFitnessVector.push_back( fitness );
          IDToFitness.insert( 
              pair<unsigned long,vector<FLOAT> >( id,tempFitnessVector ) );
      
          IDToGenerationNo.insert( 
                  pair<unsigned long,unsigned long>( id, generationNo ) );
        } else {
          k->second.push_back( fitness );
        }
      }
      
      delete row;
    }
    
    delete resultSet;
    
    /**
     * Now we have to go through and get the body flag
     */
    query = "SELECT ID     ";
    
    if ( loadGenotypeBodies == true ) {
      query += ", genotypeValue, "
               "   sizeInBits     ";
    }

    query += "FROM genotype "
             "WHERE ( ID=";
      
    bool doneFirst = false;
    map<unsigned long,vector<FLOAT> >::const_iterator j;
    for ( j = IDToFitness.begin(); j != IDToFitness.end(); j++ ) {
      if ( doneFirst == true ) {
        query += " OR ID=";
      } else {
        doneFirst = true;
      }
      
      query += j->first;
    }

    query += ") AND genotypeValue IS NOT NULL";

    resultSet = RunSelectQuery( query, true );

    set<unsigned long> IDsWithBodies;
    for ( i = 0; i < resultSet->GetNumRows(); i++ ) {
      row = resultSet->GetNextRow();
      
      id = row->GetLongField( 0L );
      
      IDsWithBodies.insert( id );
      
      if ( loadGenotypeBodies == true ) {
        unsigned long sizeInBits = row->GetLongField( 2 );
       
        MemFile value;
        if ( sizeInBits > 0 ) {
          value.append( (const unsigned char *)row->GetStringField( 1 ),
                BYTES_IN_BITFIELD( sizeInBits ) );
        }

        IDToGenotypeBodies.insert( 
            pair<unsigned long,MemFile>( id, value ) );
      }
      delete row;
    }
    
    delete resultSet;
    
    set<unsigned long>::const_iterator k;
    map<unsigned long,FLOAT>::iterator q;
    if ( bodyNotNull == true ) {
      /**
       * If we only want the ones that have bodies, then
       * we have to remove the ones that don't have any
       * bodies.
       */
      vector<unsigned long> toRemove;
      for ( j = IDToFitness.begin(); j != IDToFitness.end(); j++ ) {
        k = IDsWithBodies.find( j->first );
        if ( k == IDsWithBodies.end() ) {
          /**
           * OK - we need to delete this one...
           */
          toRemove.push_back( j->first );
        }
      }

      vector<unsigned long>::iterator r;
      for ( r = toRemove.begin();
        r != toRemove.end();
        r++ ) {
        IDToFitness.erase( *r );
      }
    }

    for ( j = IDToFitness.begin(); j != IDToFitness.end(); j++ ) {
      k = IDsWithBodies.find( j->first );
      if ( k == IDsWithBodies.end() ) {
        if ( bodyNotNull == false ) {
          IDToHasBodyFlag.insert(pair<unsigned long,bool>(j->first,false));
        }
      } else {
        IDToHasBodyFlag.insert( pair<unsigned long,bool>( j->first,true ));
      }
    }
      }

      recordIDNumber += DEFAULT_NO_OF_RECORDS;
      lastNoOfFoundRecords = noOfFoundRecords;
    } // End while

    /**
     * We now have a list of genotypes. We need to quickly find the
     * type so we can return that.
     */
    { 
      SmallString query;
      query += "SELECT ID,      "
               "       typeID   "
           "FROM   genotype "
            "WHERE ID=";
      
      map<unsigned long,vector<FLOAT> >::const_iterator i;
      for ( i = IDToFitness.begin(); i != IDToFitness.end(); i++ ) {
    if ( i != IDToFitness.begin() ) {
      query += " OR ID=";
    }

    query += i->first;
      }

      PResultSet *resultSet = RunSelectQuery( query, true );

      noOfFoundRecords = resultSet->GetNumRows();

      int j;
      for ( j = 0; 
        j < resultSet->GetNumRows();
        j++ ) {
    PRow *row = resultSet->GetNextRow();
    
    unsigned long ID      = row->GetLongField( 0L );
    unsigned long typeNum = row->GetLongField( 1L );

    IDToType.insert( pair<unsigned long,unsigned long>( ID,typeNum ) );

    delete row;
      }

      delete resultSet;
    }
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

unsigned long 
GenotypeDatabase::findPopulationSize( unsigned long evolutionaryPartRunID ) {
  unsigned long sizeOfPopulation;
  SmallString query;
  query = "SELECT evolutionaryRun.initialPopulationSize                "
          "FROM evolutionaryRun                                        "
          "LEFT JOIN evolutionaryPartRun                               "
          "ON evolutionaryRun.ID=evolutionaryPartRun.evolutionaryRunID "
          "WHERE evolutionaryPartRun.ID = ";
  query += evolutionaryPartRunID;

  PResultSet *resultSet = RunSingleRowSelectQuery( query );
  PRow       *row       = resultSet->GetNextRow();

  unsigned long populationSize = row->GetLongField( 0L );

  delete row;
  delete resultSet;

  return populationSize;
}
