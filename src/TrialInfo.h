#ifndef TRIALINFO_H
#define TRIALINFO_H

/**
 * This class represents the information about a particular trial
 * that occurred.
 */

#include <vector>
#include "SmallString.h"
#include "MemFile.h"
#include "Serializable.h"

using namespace std;

class TrialInfo : public Serializable {
public:
  static const unsigned char VALID_TRIAL;
  static const unsigned char INVALID_TRIAL;

  TrialInfo();
  virtual ~TrialInfo() { }

  void setTrialState( unsigned char s ) { m_TrialState = s; }
  
  /**
   * There is a list of fitnesses stored and a single sumFitness that's
   * used for representing the fitness within the database. You can just
   * use the single fitness value and you'll be OK. However, some trials
   * must have more than one fitness returned (for example, Pareto front)
   * so you can also set a list. You must include a database fitness in
   * that case.
   */
  bool getGenotypeInfo( int                  num,
			unsigned long       *genotypeID,
			vector<FLOAT>&       fitnesses,
			FLOAT               *databaseFitness,
			unsigned char       *state,
			int                 *typeNo ) const;

  bool getGenotypeInfo( int                  num,
			unsigned long       *genotypeID,
			FLOAT               *fitness,
			unsigned char       *state,
			int                 *typeNo ) const;

  void setGenotypeInfo( unsigned long        genotypeID,
			const vector<FLOAT>& fitness,
			FLOAT                databaseFitness,
			unsigned char        state, 
			int                  typeNo ) const;

  void setGenotypeInfo( unsigned long        genotypeID,
			FLOAT                fitness,
			unsigned char        state, 
			int                  typeNo ) const;

  void setDatabaseFitness( unsigned long genotypeID,
			   FLOAT         fitness );

  unsigned char getTrialState() const { return m_TrialState; }

  bool operator<(  const TrialInfo& t ) const;
  bool operator>(  const TrialInfo& t ) const;
  bool operator==( const TrialInfo& t ) const;

  void toString( SmallString& ) const;

  int getNoOfGenotypes() const { return m_GenotypeInfo.size(); }

  void toBytes( MemFile& ) const;

  unsigned char *readBytes( unsigned char * );

  void addGenotype( unsigned long );

  const unsigned char *getExtraData( unsigned long *sizeInBytes ) const;
  void                 setExtraData( const unsigned char *,
				     unsigned long        sizeInBytes );
  const MemFile& getExtraDataAsMemFile() const;

  /**
   * From Serializable
   */
  const unsigned char *readObject(  const unsigned char * );
  unsigned char       *writeObject( unsigned char * );

protected:
  class Trio : public Serializable {
  public:
    Trio() : databaseFitness( 0 ) { }

    bool operator==( const Trio& ) const;
    bool operator<(  const Trio& ) const;
    bool operator>(  const Trio& ) const;

    unsigned long genotypeID;
    FLOAT         databaseFitness;
    vector<FLOAT> fitnesses;
    unsigned char state;
    int           typeNo;

    const unsigned char *readObject(  const unsigned char * );
    unsigned char       *writeObject( unsigned char * );
  };

  unsigned char        m_TrialState;
  mutable vector<Trio> m_GenotypeInfo;
  MemFile              m_ExtraData;

  friend class EvolutionaryRun;
};

#endif
