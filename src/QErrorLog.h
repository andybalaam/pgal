/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef QERRORLOG_H
#define QERRORLOG_H

extern "C" {
    #include <stdio.h>
}

#include <string>
#include <fstream>

using namespace std;

#ifndef writeError
#ifdef DEBUG
#define writeError(A) __writeErrLog(__LINE__,__FILE__,A,1)
#else
#define writeError(A) __writeErrLog(__LINE__,__FILE__,A,0)
#endif
#endif

class QErrorLog {
protected:
  static bool       m_RunBefore;
  static FILE      *m_Output;
  static string     m_Filename;
  
  static void removeSurroundingWhitespace( string& );

public:
  static bool       setFilename( const char *filename );
  static bool       setFilename( string& filename )      { return setFilename( filename.c_str() ); }
  static string&    getFilename()                        { return m_Filename; }
  
  static void  __writeErrLog( long, 
			      const string&, 
			      const string&, 
			      int );
  
  static void  __writeErrLog( long        line, 
			      const char *file,
			      const char *msg,
			      int         flag ) {
    __writeErrLog( line, (string)file, (string)msg, flag );
  }
};

#endif

