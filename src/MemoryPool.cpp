#include "MemoryPool.h"
#include <stdio.h>

MemoryPool::MemoryPool( int blockSize,
			int increaseBySize ) : 
    m_IncreaseBySize(      increaseBySize  ),
    m_NoOfFreeBlocks(      0               ),
    m_NoOfAllocatedBlocks( 0               ) {
  pthread_mutexattr_t defaultMutexAttributes;
  pthread_mutexattr_init( &defaultMutexAttributes );

  pthread_mutex_init( &m_Mutex, &defaultMutexAttributes );

  if ( ( blockSize % 16 ) != 0 ) {
    blockSize += blockSize % 16;
  } 

  m_BlockSize      = blockSize;
  m_HeadOfFreeList = NULL;
}

MemoryPool::~MemoryPool() {
  vector<unsigned char *>::iterator i;
  for ( i = m_AllocatedMemory.begin();
	i != m_AllocatedMemory.end();
	i++ ) {
    delete *i;
  }
}

void MemoryPool::initialiseMemorySpace( unsigned char *s,
					int            noOfBlocks ) {
  unsigned char *t = NULL;
  int i;

  for ( i = 0; i < noOfBlocks; i++ ) {
    ((struct S_NodeItem *)s)->m_Left = (struct S_NodeItem *)t;
    
    if ( t != NULL ) {
      ((struct S_NodeItem *)t)->m_Right = (struct S_NodeItem *)s;
    }

    t = s;
    s += m_BlockSize + sizeof( struct S_NodeItem );
  }

  ((struct S_NodeItem *)t)->m_Right = NULL;

  m_NoOfFreeBlocks += noOfBlocks;
}

void MemoryPool::allocateMoreMemory( int noOfBlocks ) {
  if ( m_HeadOfFreeList == NULL ) {
    unsigned char *newMemory = 
      new unsigned char[ noOfBlocks * 
			 ( m_BlockSize + sizeof( struct S_NodeItem ) ) ];
    
    m_AllocatedMemory.push_back( newMemory );

    initialiseMemorySpace( newMemory, noOfBlocks );

    m_HeadOfFreeList = (struct S_NodeItem *)newMemory;
  }
}
