#ifndef QUICKRANDOMNUMBERS_H
#define QUICKRANDOMNUMBERS_H

/**
 * This is a quick and very dirty number generator. Be care
 * about using it.
 */

#include "RandomNumberGenerator.h"
#include <limits.h>
#include <stdio.h>
#include <time.h>

class QuickRandomNumbers : public RandomNumberGenerator {
public:
  QuickRandomNumbers();

  inline void           setRandomSeed( unsigned long );
  inline void           randomise();
  inline unsigned short random16BitInt( unsigned short range );
  inline unsigned long  randomLong();
  inline double         randomDouble();
  inline bool           randomBit();

protected:
  unsigned long m_Mult;
  unsigned long m_Tot;
  unsigned long m_Sh;
  unsigned long m_Inc;
};

inline QuickRandomNumbers::QuickRandomNumbers() {
  randomise();
}

inline void QuickRandomNumbers::randomise() {
  setRandomSeed( time( NULL ) );
}

inline void QuickRandomNumbers::setRandomSeed( unsigned long s ) {
  int x = ( s >> 16 ) & 0xffff;
  int y = s & 0xffff;

  m_Sh   = x % 8 + 4;
  m_Tot  = x / 8 + y;
  m_Mult = y * 2 + 31;
  m_Inc  = x % 255 * y % 255;
}

inline unsigned long QuickRandomNumbers::randomLong() {
  m_Tot = ( m_Tot * m_Mult + m_Inc ) ^ ( m_Tot >> m_Sh );
  return m_Tot;
}

inline unsigned short 
QuickRandomNumbers::random16BitInt( unsigned short range ) {
  return randomLong() % range;
}

inline double QuickRandomNumbers::randomDouble() {
  return (double)randomLong() / (double)ULONG_MAX;
}

inline bool QuickRandomNumbers::randomBit() {
  return ( ( randomLong() & 0x0040 ) == 0 );
}

#endif
