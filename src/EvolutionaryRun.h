/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef EVOLUTIONARYRUN_H
#define EVOLUTIONARYRUN_H

/**
 * This class deals with a complete evolutionary run, including
 * parallalizing code across different processors and saving genotypes
 * to the database. The user has to create their own trial and trial 
 * factory code. The program catches CTRL-C and finishes nicely
 * if required to do so. This means that there shouldn't be any corrupted
 * data written anywhere.
 */

#include "AbstractGenotype.h"
#include "AbstractGenotypeFactory.h"
#include "AbstractTrial.h"
#include "AbstractTrialFactory.h"
#include "GenotypeDatabase.h"
#include "RandomNumberGenerator.h"
#include "QException.h"
#include "AbstractGenerationDescriptor.h"
#include "StartGenerationInterface.h"
#include "EndGenerationInterface.h"
#include "PBossWorker.h"
#include <vector>
#include <map>
#include "TrialInfo.h"
#include "PopulationMember.h"
#include "SmallString.h"
#include "MemFile.h"
#include "AbstractCompressor.h"

using namespace std;

/**
 * Only one of these may be safely created at a time.
 */
class EvolutionaryRun {
public:
  QEXCEPTIONCLASS(        EvolutionaryRunException );
  QEXCEPTIONCLASS(        ExitProgram              );
  QEXCEPTIONCLASS(        InvalidTrial             );
  QEXCEPTIONCLASS(        InvalidGenotype          );
  QEXCEPTIONCLASS(        OutputError              );
  QEXCEPTIONDERIVEDCLASS( TooManyErrorsException, 
			  EvolutionaryRunException );

  EvolutionaryRun( unsigned long                 initialPopulationSize,
		   AbstractGenotypeFactory      *genotypeFactory,
                   AbstractGenerationDescriptor *generation,
		   AbstractTrialFactory         *trialFactory,
		   int                           maxNoOfProcesses,
		   const char                   *databaseUsername,
		   const char                   *databasePassword,
		   const char                   *hostname,
		   const char                   *databaseName,
		   long                          maxNoOfGenerations = 0L,
		   GenotypeDatabase             *defaultDatabase = NULL );

  EvolutionaryRun( unsigned long                 evolutionaryRunNo,
		   unsigned long                 initialPopulationSize,
		   AbstractGenotypeFactory      *genotypeFactory,
                   AbstractGenerationDescriptor *generation,
		   AbstractTrialFactory         *trialFactory,
		   int                           maxNoOfProcesses,
		   const char                   *databaseUsername,
		   const char                   *databasePassword,
		   const char                   *hostname,
		   const char                   *databaseName,
		   long                          maxNoOfGenerations = 0L,
		   GenotypeDatabase             *defaultDatabase = NULL );

  virtual ~EvolutionaryRun();

  /**
   * This throws an EvolutionaryRunException in case of error,
   * and a ExitProgram when the program should end due to
   * a CTRL-C or SIGTERM.
   *
   * @throw EvolutionaryRunException
   * @throw ExitProgram
   */
  void start(                       const char *notes = ""     );
  void setMicrosecondsToWait(       unsigned long              );
  void setRandomNumberGenerator(    RandomNumberGenerator    * );
  void setStartGenerationInterface( StartGenerationInterface * );
  void setEndGenerationInterface(   EndGenerationInterface   * );
  void setNoOfPopulations(          int                        );

  void          setTimePeriod( unsigned long t ) { m_TimePeriod = t;    }
  unsigned long getTimePeriod() const            { return m_TimePeriod; }

  void setWriteGenotypeBodyEvery( unsigned long g ) {
    m_WriteGenotypeBodyEvery = g;
  }

  unsigned long getEvolutionaryPartRunID() const {
    return m_EvolutionaryPartRun;
  }

  unsigned long getEvolutionaryRunID() const {
    return m_EvolutionaryRun;
  }

protected:
  /**
   * These are state variables, used internally for processes, etc.
   */
  static const char          *PROGRAM_VERSION;

  class FitnessUnit {
  public:
    unsigned long   ID;
    FLOAT           fitness;

    bool operator<( const FitnessUnit& a ) const {
      return fitness < a.fitness ? true : false;
    }
  };

  static void   exitProgram( int );

  unsigned long createGenerationRecord(          unsigned long    generation );
  unsigned long createTrialRecord(               const TrialInfo& trialInfo  );
  void          createInitialPopulation(         const char      *notes = "" );
  void          loadPreviousPopulation(          const char      *notes = "" );
  void          createEvolutionaryPartRunRecord( const char      *notes = "" );
  void          finishEvolutionaryPartRun();
  void          createEvolutionaryRunRecord();
  void          loadLastPopulation();
  void          runEvolutionaryRun();
  void          waitForChildren();
  void          checkForExit();
  void          calculateQuartileData();
  void          reproducePopulation( queue<unsigned long>& toBeReproduced );
  void          performTrials( queue<TrialInfo>&  toSendToTrial,
			       vector<TrialInfo>& processed );

  bool          calculateSubPopulationQuartileData( int    genotypeTypeID,
                            int    fitnessNo,
						    FLOAT *quartiles );

  void          makeSureCurrentGenotypeRecordsHaveBody();

  map<unsigned long,PopulationMember>    m_Population;
  SmallString                            m_DatabaseUsername;
  SmallString                            m_DatabasePassword;
  SmallString                            m_Hostname;
  SmallString                            m_DatabaseName;
  RandomNumberGenerator                 *m_RandomNumberGenerator;
  bool                                   m_FinishEvolutionaryPartRun;
  bool                                   m_NewTrialEachTime;
  int                                    m_InitialPopulationSize;
  int                                    m_MaxNoOfProcesses;
  int                                    m_NoOfPopulations;
  unsigned long                          m_WriteGenotypeBodyEvery;
  unsigned long                          m_EvolutionaryRun;
  unsigned long                          m_EvolutionaryPartRun;
  unsigned long                          m_TimePeriod;
  unsigned long                          m_TimeStamp;
  unsigned long                          m_GenerationsCounter;
  unsigned long                          m_Generation;
  unsigned long                          m_LatestGeneration;
  unsigned long                          m_GenerationIndexNo;
  long                                   m_MaxNoOfGenerations;
  long                                   m_NoOfNewGenotypes;
  fd_set                                 m_rfds;
  struct timeval                         m_tv;
  GenotypeDatabase                      *m_GenotypeDatabase;
  AbstractGenotypeFactory               *m_GenotypeFactory;
  AbstractGenerationDescriptor          *m_GenerationDescriptor;
  AbstractTrialFactory                  *m_TrialFactory;
  AbstractCompressor                    *m_Compressor;
  StartGenerationInterface              *m_StartGenerationInterface;
  EndGenerationInterface                *m_EndGenerationInterface;
  PBossWorker                           *m_BossWorker;
  bool                                   m_UseDefaultDatabase;
};

#endif
