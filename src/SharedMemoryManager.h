#ifndef SHAREDMEMORYMANAGER_H
#define SHAREDMEMORYMANAGER_H

/**
 * This is a brain dead memory manager for variable length 
 * strings. Their is a set number of memory blocks of
 * fixed size. There is no semaphore access linked here 
 * at all, this must all be performed by the user.
 */

#include "QException.h"
#include <sys/ipc.h>
#include <sys/shm.h>

class SharedMemoryManager {
public:
  QEXCEPTIONCLASS( CannotAllocateSharedMemory );
  QEXCEPTIONCLASS( CannotGrabMemory           );
  QEXCEPTIONCLASS( CannotFindSharedMemory     );

  SharedMemoryManager( int    noOfBlocks,
		       size_t sizeOfEachBlock );
  virtual ~SharedMemoryManager();

  unsigned char *grabSpace( int blockNo );

protected:
  void           *m_RawMemory;
  unsigned int    m_NoOfBlocks;
  unsigned int    m_SizeOfEachBlock;
  int             m_ShmID;
};

#endif
