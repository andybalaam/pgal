#include "PProcessBossWorker.h"
#include "PWorker.h"
#include "QErrorLog.h"
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include "EvolutionaryRun.h"

PProcessBossWorker::PProcessBossWorker( unsigned int noOfProcesses,
					unsigned int dataSizeInBytes ) :
    m_NoOfProcesses(       noOfProcesses                  ),
    m_SharedMemoryManager( noOfProcesses, dataSizeInBytes ) {
  unsigned int i;
  for ( i = 0; i < noOfProcesses; i++ ) {
    m_FreeBlockNos.push( i );
  }
}

PProcessBossWorker::~PProcessBossWorker() {
  /**
   * We have to kill all our child processes...
   */
  map<pid_t,struct ProcessDetails>::iterator i;
  
  for ( i = m_AllProcessDetails.begin(); 
	i != m_AllProcessDetails.end(); 
	i++ ) {
    kill( i->second.m_ProcessID, SIGKILL );
  }

  /**
   * Now we'll wait for them to finish...
   */
  int status;
  for ( i = m_AllProcessDetails.begin(); 
	i != m_AllProcessDetails.end(); 
	i++ ) {
    wait( &status );
  }
}

PWorker *PProcessBossWorker::waitForProcessToFinish( bool forceWait ) {
  /**
   * If forceWait is false, we check to see if a child has finished
   * without waiting. If one has, we just return it. If not, we 
   * return NULL.
   * If forceWait is true, we have to wait until one finishes, then
   * we do the same thing.
   */
  try {
    while ( m_AllProcessDetails.size() < m_NoOfProcesses &&
	    m_WaitingJobs.empty() == false ) {
      setHeadOfQueueRunning();
    } 

    if ( m_AllProcessDetails.empty() == true ) {
      return NULL;
    }

    int status;
    pid_t processID;
    
    errno = 0;
    if ( forceWait == false ) {
      processID = waitpid( -1, &status, WNOHANG );
      if ( processID == 0 ) {
	return NULL;
      }
    } else {
      processID = wait( &status );
    }

    if ( processID == -1 ) {
      if ( errno == EINTR ) {
        EvolutionaryRun::ExitProgram e( "EINTR" );
        e.fillInStackTrace();
        throw e;
      }

      string errMsg;
      errMsg += "Error returned from wait call: error states: ";
      errMsg += strerror( errno );
      
      WaitForChildError e( errMsg.c_str() );
      e.fillInStackTrace();
      throw e;
    }
    
    if ( WIFSIGNALED( status ) ) {
      removeProcessFromList( processID );
      
      char errMsg[ 1024 ];
      sprintf( errMsg, "Child process %d didn't catch signal %d",
	       processID, WTERMSIG( status ) );
      
      ChildSignalNotCaught e( errMsg );
      e.fillInStackTrace();
      throw e;
    }
    
    if ( WIFEXITED( status ) && WEXITSTATUS( status ) ) {
      removeProcessFromList( processID );
      
      char errMsg[ 1024 ];
      sprintf( errMsg, "Child exit status is %d", WEXITSTATUS( status ) );
      
      ChildExitStatusNotZero e( errMsg );
      e.fillInStackTrace();
      throw e;
    }
    
    return transformProcessID( processID );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

void PProcessBossWorker::removeProcessFromList( pid_t processID ) {
  try {
    map<pid_t,struct ProcessDetails>::iterator i;
    i = m_AllProcessDetails.find( processID );
    if ( i == m_AllProcessDetails.end() ) {
      char errMsg[ 1024 ];
      sprintf( errMsg, "Unknown process found: %d", (int)processID );
      
      UnknownProcess e( errMsg );
      e.fillInStackTrace();
      throw e;
    }
 
    m_FreeBlockNos.push( i->second.m_BlockNo );

    m_AllProcessDetails.erase( i );
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

PWorker *PProcessBossWorker::transformProcessID( pid_t processID ) {
  try {
    map<pid_t,struct ProcessDetails>::iterator i;
    i = m_AllProcessDetails.find( processID );
    if ( i == m_AllProcessDetails.end() ) {
      char errMsg[ 1024 ];
      sprintf( errMsg, "Unknown process found: %d", (int)processID );
      
      UnknownProcess e( errMsg );
      e.fillInStackTrace();
      throw e;
    }
    
    /**
     * The memory block contains a serialized version of the class. Note
     * that it must also deal with exceptions being passed.
     */
    const unsigned char *s = i->second.m_ParameterMemory;
    if ( *s == 'E' ) {
      s++;

      SerializedException e( "SerialisedException" );
      s = e.readObject( s );
      e.fillInStackTrace();
      throw e;
    } else if ( *s == 'T' ) {
      s++;
      dynamic_cast<Serializable*>
	( i->second.m_Serializable )->readObject( s );
    
      PWorker *pWorker = i->second.m_Serializable;
      
      removeProcessFromList( processID );
      
      while ( m_AllProcessDetails.size() < m_NoOfProcesses &&
	      m_WaitingJobs.empty()  == false ) {
	setHeadOfQueueRunning();
      }
      
      return pWorker;
    } else {
      char errMsg[ 1024 ];
      sprintf( errMsg, "Unknown code of '%c'", *s );
      
      UnknownMagicCode e( errMsg );
      e.fillInStackTrace();
      throw e;
    }
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

void PProcessBossWorker::setHeadOfQueueRunning() {
  try {
    /**
     * We'll take the first item in the job queue and set it
     * running in the background.
     */
    if ( m_FreeBlockNos.empty() == true ) {
      NoFreeBlocks e( "No blocks left" );
      e.fillInStackTrace();
      throw e;
    }

    if ( m_WaitingJobs.empty() == true ) {
      NoJobsToRun e( "No jobs to run" );
      e.fillInStackTrace();
      throw e;
    }

    struct ProcessDetails processDetails;

    processDetails.m_BlockNo = m_FreeBlockNos.front();
    m_FreeBlockNos.pop();

    processDetails.m_ParameterMemory = 
      m_SharedMemoryManager.grabSpace( processDetails.m_BlockNo );

    processDetails.m_Serializable = m_WaitingJobs.front();
    m_WaitingJobs.pop();

    /**
     * Now we perform a fork.
     */
    errno = 0;
    switch( ( processDetails.m_ProcessID = fork() ) ) {
    case -1: 
      {
	string errMsg;
	errMsg += "fork error: error states: ";
	errMsg += strerror( errno );
	
	CannotFork e( errMsg.c_str() );
	e.fillInStackTrace();
	throw e;
      }
      break;

    case 0:
      {
	struct sigaction sa;
	memset( &sa, 0, sizeof( sa ) );
	sa.sa_handler = SIG_IGN;
	sigaction( SIGTERM, &sa, NULL );
	sigaction( SIGINT,  &sa, NULL );
      
	runChild( processDetails );
	exit( 0 );
      }
      break;

    default:
      /**
       * All went well...
       */
      m_AllProcessDetails.insert( pair<pid_t,struct ProcessDetails>
				  ( processDetails.m_ProcessID, 
				    processDetails ) );
    }
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

int PProcessBossWorker::getNoOfActiveProcesses() const {
  return m_AllProcessDetails.size();
}

int PProcessBossWorker::getNoOfFreeProcesses()   const {
  return m_NoOfProcesses - m_AllProcessDetails.size();
}

int PProcessBossWorker::getNoOfFinishedWorkers() const {
  return 0;
}

int PProcessBossWorker::getNoOfThreads() const {
  return m_NoOfProcesses;
}

void PProcessBossWorker::run( PWorker *worker ) {
  try {
    /**
     * The worker must also be of a Serializable type.
     */
    if ( dynamic_cast<Serializable*>( worker ) == NULL ) {
      NotSerializable e( "PWorker not also of type Serializable" );
      e.fillInStackTrace();
      throw e;
    }

    m_WaitingJobs.push( worker );
    
    while ( m_AllProcessDetails.size() < m_NoOfProcesses &&
	    m_WaitingJobs.empty()  == false ) {
      setHeadOfQueueRunning();
    }
  } catch( QException& e ) {
    e.fillInStackTrace();
    throw;
  }
}

void PProcessBossWorker::runChild( struct ProcessDetails processDetails ) {
  try {
    /**
     * We run the child and then serialize it to the given
     * memory space.
     */
    processDetails.m_Serializable->start();
    
    unsigned char *s = processDetails.m_ParameterMemory;
    *s++ = 'T';

    dynamic_cast<Serializable*>
      ( processDetails.m_Serializable )->writeObject( s );

    exit( 0 );
  } catch( QException& e ) {
    /**
     * We have to send the exception on.
     */
    unsigned char *s = processDetails.m_ParameterMemory;
    *s++ = 'E';

    dynamic_cast<Serializable*>( &e )->writeObject( s );

    { // DEBUG block
      string errMsg;
      errMsg += "Caught exception in child: ";
      errMsg += e.toString();
      errMsg += "\n";
      errMsg += e.getStackTrace();
      QErrorLog::writeError( errMsg.c_str() );
    }

    exit( 0 );
  }
}
