/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef STEADYSTATEDESCRIPTOR_H
#define STEADYSTATEDESCRIPTOR_H

#include "AbstractGenerationDescriptor.h"
#include "RandomNumberGenerator.h"
#include "QException.h"

class SteadyStateDescriptor : public AbstractGenerationDescriptor {
public:
  QEXCEPTIONCLASS( SteadyStateDescriptorException );

  SteadyStateDescriptor( int populationSize );
  
  void getGenotypesToSendToTrial( 
      vector<unsigned long>                        *toSendToTrial,
      const map<unsigned long,AbstractGenotype *>&  population );

  void getNextGenerationInformation( 
      set<unsigned long>                      *toBeRemoved,
      vector<unsigned long>                   *toBeReproduced,
      const map<unsigned long,FLOAT>&          fitnesses,
      const map<unsigned long,unsigned char>&  states );

protected:
  int                    m_PopulationSize;
  unsigned long          m_GenotypeNo1;
  unsigned long          m_GenotypeNo2;

  RandomNumberGenerator *m_RandomNumbers;
};

#endif
