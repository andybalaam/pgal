/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef ABSTRACTGENOTYPE_H
#define ABSTRACTGENOTYPE_H

#define TRUE 1
#define FALSE 0

/**
 * This class represents a generic genotype. Derived
 * instances of this class is to be created by derived
 * instances of AbstractGenotypeFactory.
 */

#include "MemFile.h"
#include <stdio.h>

using namespace std;

class AbstractGenotype {
public:
  static const FLOAT INVALID_FITNESS;

  AbstractGenotype() : m_PrivateIndexNo( 0 ), m_TypeNo( 0 ), m_IsSaved( FALSE )
    { }

  virtual ~AbstractGenotype() { }

  /**
   * This must return a representation of the genotype as an array of
   * bytes. The space allocated for this must be dealt with by the
   * derived genotype class. The size in bits must also be returned.
   * This method is used to store the genotype in the database.
   */
  virtual const unsigned char *getGenotypeAsBits( unsigned long *sizeInBits )
  	const = 0;

  virtual int                  getType() const { return m_TypeNo; }

  /**
   * This returns the index number of the genotype within the
   * database. If the number doesn't exist (i.e. it's not been
   * added to the database, 0 is returned).
   */
  unsigned long getIndexNo() const { return m_PrivateIndexNo; }

  bool getIsSaved() const { return m_IsSaved; }

  // ---
  
  void setIndexNo( unsigned long n ) {
    m_PrivateIndexNo = n;
  }
  virtual void setType( int n ) { m_TypeNo = n; }
  void setIsSaved( bool isSaved ) const { m_IsSaved = isSaved; }


private:
  unsigned long m_PrivateIndexNo;
  int           m_TypeNo;
  mutable bool  m_IsSaved;
};

#endif
