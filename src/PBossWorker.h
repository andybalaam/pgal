#ifndef PBOSSWORKER_H
#define PBOSSWORKER_H

/**
 * This interface is for the Boss/Worker design pattern. Behind
 * it, you can either use threads, processes, or just perform 
 * the actions straight away.
 */

class PWorker;

class PBossWorker {
public:
  virtual ~PBossWorker() { }

  /**
   * This method will start the PWorker perhaps in the background.
   * It starts it by running PWorker's start method.
   */
  virtual void run( PWorker * ) = 0;

  /**
   * This process will retrieve the results of a worker within
   * the given MemFile. The return value is true if a workers
   * results are present, or false if they are not. The actions 
   * are different depending upon thhe value of forceWait.
   * 
   * If forceWait is false:
   *
   * 1) If there is a worker that was running in the back ground
   *    but is now finished, then return the worker immediately.
   * 2) Otherwise return NULL immediately.
   *
   * If forceWait is true, the method always returns a finished 
   * worker unless there are no workers running or finished at
   * all:
   * 
   * 1) If there is a worker that was running in the back ground
   *    but is now finished, then return the worker immediately.
   * 2) Otherwise wait until a worker has finished.
   */
  virtual PWorker *waitForProcessToFinish( bool forceWait ) = 0;

  /**
   * This returns the number of active processes.
   */
  virtual int getNoOfActiveProcesses() const = 0;

  virtual int getNoOfFreeProcesses()   const = 0;

  virtual int getNoOfFinishedWorkers() const = 0;

  virtual int getNoOfThreads()         const = 0;
};

#endif
