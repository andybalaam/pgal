/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef TWOSUBPOPULATIONSRANKSELDES_H
#define TWOSUBPOPULATIONSRANKSELDES_H

/**
 * This is slightly  different from TwoPopulationsRankSelDes because the
 * populations can be different sizes, and the trials sent back are 
 * every member of the first population pitted against every member of 
 * the second.
 */

#include "AbstractGenerationDescriptor.h"
#include "AbstractFitnessComparator.h"
#include "AverageFitnessComparator.h"
#include "QException.h"
#include "RandomNumberGenerator.h"
#include "PopulationMember.h"
#include "TrialInfo.h"

class TwoSubPopulationsRankSelDes : public AbstractGenerationDescriptor {
public:
  QEXCEPTIONCLASS( TwoSubPopulationsRankSelDesException );

  TwoSubPopulationsRankSelDes( int pop1Size, int pop2Size );
  virtual ~TwoSubPopulationsRankSelDes();

  void getGenotypesToSendToTrial( 
      queue<TrialInfo>&                          toSendToTrial,
      const map<unsigned long,PopulationMember>& population );

  void getNextGenerationInformation( 
      set<unsigned long>&                   toBeRemoved,
      queue<unsigned long>&                 toBeReproduced,
      map<unsigned long,PopulationMember>&  population,
      const vector<TrialInfo>&              trials );

  bool setNoOfElite1( int noOfElite );
  bool setNoOfElite2( int noOfElite );
  bool setNoOfTrialsPerGenotype( int );
  void setFitnessComparator1( AbstractFitnessComparator * );
  void setFitnessComparator2( AbstractFitnessComparator * );

protected:
  class S_fitnesses {
  public:
    vector<vector<FLOAT> >     fitnesses;    
    unsigned long              genotypeNo;
    unsigned char              state;
    FLOAT                      lowerProbability;
    FLOAT                      higherProbability;
    bool                       alreadySet;
    AbstractFitnessComparator *fitnessComparator;
  };

  static int sortFitnessStructures( const void *, const void * );

  static AverageFitnessComparator m_DefaultFitnessComparator;

  int                     m_Pop1Size;
  int                     m_Pop2Size;
  struct S_fitnesses    **m_Fitnesses1;
  struct S_fitnesses    **m_Fitnesses2;
  RandomNumberGenerator  *m_RandomNumbers;
  int                     m_NoOfElite1;
  int                     m_NoOfElite2;
  int                     m_NoOfTrialsPerGenotype;
};

#endif
