#ifndef RUNTRIALWORKER_H
#define RUNTRIALWORKER_H

/**
 * This class creates a trial instances, manages the data required
 * to run it, and runs it.
 */

#include "PWorker.h"
#include "AbstractGenotype.h"
#include "TrialInfo.h"
#include "AbstractTrial.h"
#include "AbstractTrialFactory.h"
#include "Serializable.h"
#include <vector>
#include "MemFile.h"
#include "MemoryPool.h"
#include "QException.h"

using namespace std;

class RunTrialWorker : public PWorker, public Serializable {
public:
  RunTrialWorker( AbstractTrialFactory       *trialFactory,
                  const TrialInfo&            trialInfo,
		  vector<AbstractGenotype*>&  genotypes,
		  unsigned long               generationNo,
		  const MemFile&              extraBytes );

  void start();

  TrialInfo m_TrialInfo;

  /*static void *operator new( size_t s ) {
    return (void *)s_Pool.allocateMemory();
  }

  static void operator delete( void *p, size_t s = 0 ) {
    s_Pool.freeMemory( (unsigned char *)p );
  }*/

  /**
   * From Serializable
   *
   * This is designed for use in the PProcessBossWorker 
   * class. So, it only needs to write the information
   * required *after* the trial have been run. This 
   * includes the trial info structure.
   */
  const unsigned char *readObject(  const unsigned char * );
  unsigned char       *writeObject( unsigned char * );

protected:
  AbstractTrial              *m_Trial;
  vector<AbstractGenotype *>  m_Genotypes;
  unsigned long               m_GenerationNo;
  MemFile                     m_ExtraBytes;
  AbstractTrialFactory       *m_TrialFactory;

  static MemoryPool s_Pool;
};

#endif
