/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef ABSTRACTFITNESSCOMPARATOR_H
#define ABSTRACTFITNESSCOMPARATOR_H

/**
 * This class is used to get a value on whether fitnesses are
 * greater or less than another. It isn't used directly by
 * EvolutionaryRun but by the supporting classes such as
 * RankSelectionDescriptor.
 */

#include <vector>

using namespace std;

class AbstractFitnessComparator {
public:
  virtual ~AbstractFitnessComparator() { }

  /**
   * This returns -1 if a < b, 0 if a == b, and 1 if a > b
   */
  virtual int  compareFitness(   const vector<vector<FLOAT> >& a, 
			         const vector<vector<FLOAT> >& b ) = 0;

  /**
   * This produces a fitness for the database, it isn't used for
   * comparison or anything.
   */
  virtual FLOAT calculateFitness( const vector<vector<FLOAT> >& )   = 0;
};

#endif
