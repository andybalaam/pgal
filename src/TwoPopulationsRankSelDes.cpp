/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "TwoPopulationsRankSelDes.h"
#include "EvolutionaryRun.h"
#include "QErrorLog.h"

AverageFitnessComparator TwoPopulationsRankSelDes::m_DefaultFitnessComparator;

/**
 * This class depends upon all the genotypes having a particular type.
 */
TwoPopulationsRankSelDes::TwoPopulationsRankSelDes( int populationSize ) :
    m_RandomNumbers( RandomNumberGenerator::getDefaultGoodGenerator() ),
    m_Fitnesses1( NULL ),
    m_Fitnesses2( NULL ),
    m_NoOfElite( 0 ),
    m_NoOfTrialsPerGenotype( 1 ) {
  if ( populationSize <= 4 ||
       ( populationSize % 2 ) == 1 ) {
    SmallString msg;
    msg += "Invalid population size of ";
    msg += populationSize;
    msg += " - it must be larger than 3 and "
           "an even number.";

    TwoPopulationsRankSelDesException e( msg );
    e.fillInStackTrace();
    throw e;
  }

  m_SubPopulationSize = populationSize / 2;
  m_Fitnesses1 = new S_fitnesses *[ m_SubPopulationSize ];
  m_Fitnesses2 = new S_fitnesses *[ m_SubPopulationSize ];

  int i;
  for ( i = 0; i < m_SubPopulationSize; i++ ) {
    m_Fitnesses1[ i ] = new S_fitnesses;
    m_Fitnesses2[ i ] = new S_fitnesses;
  }

  setFitnessComparator( &m_DefaultFitnessComparator );
}

TwoPopulationsRankSelDes::~TwoPopulationsRankSelDes() {
  int i;
  if ( m_Fitnesses1 != NULL ) {
    for ( i = 0; i < m_SubPopulationSize; i++ ) {
      delete m_Fitnesses1[ i ];
    }
    delete[] m_Fitnesses1;
  }
  
  if ( m_Fitnesses2 != NULL ) {
    for ( i = 0; i < m_SubPopulationSize; i++ ) {
      delete m_Fitnesses2[ i ];
    }
    delete[] m_Fitnesses2;
  }
}

void TwoPopulationsRankSelDes::getGenotypesToSendToTrial(
    queue<TrialInfo>&                          toSendToTrial,
    const map<unsigned long,PopulationMember>& population ) {
  /**
   * In this method, we have to match a member from one
   * population with a member from another. This depends 
   * upon both types having the same number of members
   * within the population.
   */
  int j;
  for ( j = 0; j < m_SubPopulationSize; j++ ) {
    m_Fitnesses1[ j ]->fitnesses.clear();
    m_Fitnesses2[ j ]->fitnesses.clear();
  }

  vector<FLOAT> randomVector;
  int z;
  for ( z = 0; z < m_NoOfTrialsPerGenotype; z++ ) {
    int popIndex1 = 0;
    int popIndex2 = 0;
    map<unsigned long,PopulationMember>::const_iterator i;
    
    if ( population.size() != m_SubPopulationSize * 2 ) {
      SmallString errMsg;
      errMsg += "Found unexpected population size of ";
      errMsg += population.size();
      errMsg += " instead of ";
      errMsg += m_SubPopulationSize * 2;
      
      TwoPopulationsRankSelDesException e( errMsg );
      e.fillInStackTrace();
      throw e;
    }
    
    for ( i = population.begin(); i != population.end(); i++ ) {
      if ( i->second.getGenotype()->getType() == 0 ) {
	if ( popIndex1 == m_SubPopulationSize ) {
	  SmallString errMsg;
	  errMsg += "Found mismatching population sizes, sub "
	            "population size should be ";
	  errMsg += m_SubPopulationSize;
	  errMsg += ", whilst a sub population size is larger";
	  
	  TwoPopulationsRankSelDesException e( errMsg );
	  e.fillInStackTrace();
	  throw e;
	}

	m_Fitnesses1[ popIndex1   ]->genotypeNo = i->first;
	randomVector.clear();
	randomVector.push_back( m_RandomNumbers->randomDouble() );
	m_Fitnesses1[ popIndex1   ]->fitnesses.push_back( randomVector );
	m_Fitnesses1[ popIndex1++ ]->state = PopulationMember::KNOWN_FITNESS;
      } else if ( i->second.getGenotype()->getType() == 1 ) {
	if ( popIndex2 == m_SubPopulationSize ) {
	  SmallString errMsg;
	  errMsg += "Found mismatching population sizes, sub "
	            "population size should be ";
	  errMsg += m_SubPopulationSize;
	  errMsg += ", whilst a sub population size is larger";
	  
	  TwoPopulationsRankSelDesException e( errMsg );
	  e.fillInStackTrace();
	  throw e;
	}
	
	m_Fitnesses2[ popIndex2   ]->genotypeNo = i->first;
        randomVector.clear();
	randomVector.push_back( m_RandomNumbers->randomDouble() );
	m_Fitnesses2[ popIndex2   ]->fitnesses.push_back( randomVector );
	m_Fitnesses2[ popIndex2++ ]->state = PopulationMember::KNOWN_FITNESS;
      }
    }
    
    if ( popIndex1 != m_SubPopulationSize ||
	 popIndex2 != m_SubPopulationSize ) {
      SmallString errMsg;
      errMsg += "Found mismatching population sizes, sub "
	        "population size should be ";
      errMsg += m_SubPopulationSize;
      errMsg += ", whilst sub population sizes seem are currently ";
      errMsg += popIndex1;
      errMsg += " and ";
      errMsg += popIndex2;
      
      TwoPopulationsRankSelDesException e( errMsg );
      e.fillInStackTrace();
      throw e;
    }
    
    /**
     * Now we'll randomise the two population orders and match
     * one against the other. We'll do this by setting up random
     * fitnesses for each genotype and then sorting them, and then
     * pairing up each with the other.
     */
    qsort( m_Fitnesses1,
	   m_SubPopulationSize,
	   sizeof( S_fitnesses * ),
	   sortFitnessStructures );
    
    qsort( m_Fitnesses2,
	   m_SubPopulationSize,
	   sizeof( S_fitnesses * ),
	   sortFitnessStructures );
    
    for ( j = 0; j < m_SubPopulationSize; j++ ) {
      TrialInfo trialInfo;
      trialInfo.addGenotype( m_Fitnesses1[ j ]->genotypeNo );
      trialInfo.setGenotypeInfo( m_Fitnesses1[ j ]->genotypeNo,
				 0.0,
				 PopulationMember::UNKNOWN_FITNESS,
				 0 );
      
      trialInfo.addGenotype( m_Fitnesses2[ j ]->genotypeNo );
      trialInfo.setGenotypeInfo( m_Fitnesses2[ j ]->genotypeNo,
				 0.0,
				 PopulationMember::UNKNOWN_FITNESS,
				 1 );
      
      toSendToTrial.push( trialInfo );
    }
  }

  /*{ // DEBUG block
    SmallString msg;
    msg += "Sending these to trial:\n";
    
    vector<TrialInfo>::iterator j;
    int i;
    unsigned long genotypeID;
    for ( j = toSendToTrial.begin(); j != toSendToTrial.end(); j++ ) {
      for ( i = 0; 
	    j->getGenotypeInfo( i, &genotypeID, NULL, NULL, NULL ); 
	    i++ ) {
	if ( i > 0 ) {
	  msg += "\t";
	}
	
	msg += genotypeID;
      }
      msg += "\n";
    }
    
    QErrorLog::writeError( msg );
    }*/
}

void TwoPopulationsRankSelDes::getNextGenerationInformation(
    set<unsigned long>&                  toBeRemoved,
    queue<unsigned long>&                toBeReproduced,
    map<unsigned long,PopulationMember>& population,
    const vector<TrialInfo>&             trials ) {
  /**
   * We'll order them, and the chances of each genotype 
   * reproducing is proportional to it's order in the 
   * rank.
   */

  /*{ // DEBUG block
    SmallString msg;
    msg += "Here are a list of trials:";
    vector<TrialInfo>::const_iterator b;
    for ( b = trials.begin(); b != trials.end(); b++ ) {
      msg += "\n**************\n";
      b->toString( msg );
    }
    QErrorLog::writeError( msg );
    }*/

  /**
   * Set our structure to unset...
   */
  int j;
  for ( j = 0; j < m_SubPopulationSize; j++ ) {
    m_Fitnesses1[ j ]->state = PopulationMember::UNKNOWN_FITNESS;
    m_Fitnesses1[ j ]->alreadySet = false;
    m_Fitnesses1[ j ]->fitnesses.clear();

    m_Fitnesses2[ j ]->state = PopulationMember::UNKNOWN_FITNESS;
    m_Fitnesses2[ j ]->alreadySet = false;
    m_Fitnesses2[ j ]->fitnesses.clear();
  }
  
  /**
   * We have to quickly copy all the genotype details to
   * our area.
   */
  vector<TrialInfo>::const_iterator i;
  if ( population.size() != m_SubPopulationSize * 2 ) {
    SmallString errMsg;
    errMsg += "Found unexpected population size of ";
    errMsg += population.size();
    errMsg += " instead of ";
    errMsg += m_SubPopulationSize * 2;
 
    TwoPopulationsRankSelDesException e( errMsg );
    e.fillInStackTrace();
    throw e;
  }

  map<unsigned long,int> position1;
  map<unsigned long,int> position2;
  int j1, j2;
  
  for ( i = trials.begin(), j1 = 0, j2 = 0; 
	i != trials.end(); 
	i++, j1++, j2++ ) {
    if ( i->getNoOfGenotypes() != 2 ) {
      char errMsg[ 1024 ];
      sprintf( errMsg, "Expected two genotypes per trial - found one with %d",
	       i->getNoOfGenotypes() );

      TwoPopulationsRankSelDesException e( errMsg );
      e.fillInStackTrace();
      throw e;
    }

    int z;
    for ( z = 0; z < 2; z++ ) {
      unsigned long genotypeID;
      vector<FLOAT> fitnesses;
      FLOAT         databaseFitness;
      unsigned char state;
      int           typeNo;

      i->getGenotypeInfo( z,
			  &genotypeID,
			  fitnesses,
			  &databaseFitness,
			  &state,
			  &typeNo );
      if ( typeNo != z ) {
	char errMsg[ 1024 ];
	sprintf( errMsg, 
		 "Wrong type!" );
	
	TwoPopulationsRankSelDesException e( errMsg );
	e.fillInStackTrace();
	throw e;
      }

      static int counter = 0;

      if ( typeNo == 0 ) {
	int index = j1;
	map<unsigned long,int>::iterator before;
	before = position1.find( genotypeID );
	if ( before != position1.end() ) {
	  index = before->second;
	  j1--;
	} else {
	  position1.insert( pair<unsigned long,int>( genotypeID, j1 ) );
	}

	if ( index == m_SubPopulationSize ) {
	  SmallString errMsg;
	  errMsg += "Found mismatching population sizes, sub "
	            "population size should be ";
	  errMsg += m_SubPopulationSize;
	  errMsg += ", whilst a sub population size is larger";
	  
	  TwoPopulationsRankSelDesException e( errMsg );
	  e.fillInStackTrace();
	  throw e;
	}
	
	i->getGenotypeInfo( 0,
			    &genotypeID,
			    fitnesses,
			    &databaseFitness,
			    &state,
			    &typeNo );
	
	m_Fitnesses1[ index ]->genotypeNo = genotypeID;
	m_Fitnesses1[ index ]->fitnesses.push_back( fitnesses );
	if ( m_Fitnesses1[ index ]->state != 
	     PopulationMember::INVALID_GENOTYPE ) {
	  m_Fitnesses1[ index ]->state = state;
	}
	m_Fitnesses1[ index ]->alreadySet = true;
	m_Fitnesses1[ index ]->databaseFitness = 
          m_Fitnesses1[ index ]->fitnessComparator->calculateFitness( 
	    m_Fitnesses1[ index ]->fitnesses );

	map<unsigned long,PopulationMember>::const_iterator h;
	h = population.find( genotypeID );
	if ( h == population.end() ) {
	  char errMsg[ 1024 ];
	  sprintf( errMsg, "Choosen genotype I can't find in the "
		   "population!" );
	  
	  TwoPopulationsRankSelDesException e( errMsg );
	  e.fillInStackTrace();
	  throw e;
	}

	if ( h->second.getState() == PopulationMember::INVALID_GENOTYPE ) {
	  m_Fitnesses1[ index ]->state = PopulationMember::INVALID_GENOTYPE;
	}
      } else if ( typeNo == 1 ) {
	int index = j2;
	map<unsigned long,int>::iterator before;
	before = position2.find( genotypeID );
	if ( before != position2.end() ) {
	  index = before->second;
	  j2--;
	} else {
	  position2.insert( pair<unsigned long,int>( genotypeID, j2 ) );
	}

	if ( index == m_SubPopulationSize ) {
	  SmallString errMsg;
	  errMsg += "Found mismatching population sizes, sub "
	            "population size should be ";
	  errMsg += m_SubPopulationSize;
	  errMsg += ", whilst a sub population size is larger";
	  
	  TwoPopulationsRankSelDesException e( errMsg );
	  e.fillInStackTrace();
	  throw e;
	}
	
	i->getGenotypeInfo( 1,
			    &genotypeID,
			    fitnesses,
			    &databaseFitness,
			    &state,
			    &typeNo );
	
	m_Fitnesses2[ index ]->genotypeNo = genotypeID;
	m_Fitnesses2[ index ]->fitnesses.push_back( fitnesses );
	if ( m_Fitnesses2[ index ]->state != 
	     PopulationMember::INVALID_GENOTYPE ) {
	  m_Fitnesses2[ index ]->state = state;
	}
	m_Fitnesses2[ index ]->alreadySet = true;
	m_Fitnesses2[ index ]->databaseFitness = 
          m_Fitnesses2[ index ]->fitnessComparator->calculateFitness( 
	    m_Fitnesses2[ index ]->fitnesses );

	map<unsigned long,PopulationMember>::const_iterator h;
	h = population.find( genotypeID );
	if ( h == population.end() ) {
	  char errMsg[ 1024 ];
	  sprintf( errMsg, "Choosen genotype I can't find in the "
		   "population!" );
	  
	  TwoPopulationsRankSelDesException e( errMsg );
	  e.fillInStackTrace();
	  throw e;
	}

	if ( h->second.getState() == PopulationMember::INVALID_GENOTYPE ) {
	  m_Fitnesses2[ index ]->state = PopulationMember::INVALID_GENOTYPE;
	}

	/*int index = j2;
	map<unsigned long,int>::iterator before;
	before = position2.find( genotypeID );
	if ( before != position2.end() ) {
	  index = before->second;
	  j2--;
	} else {
	  position2.insert( pair<unsigned long,int>( genotypeID, j2 ) );
	}
       
	if ( index == m_SubPopulationSize ) {
	  SmallString errMsg;
	  errMsg += "Found mismatching population sizes, sub "
	            "population size should be ";
	  errMsg += m_SubPopulationSize;
	  errMsg += ", whilst a sub population size is larger";
	  
	  TwoPopulationsRankSelDesException e( errMsg );
	  e.fillInStackTrace();
	  throw e;
	}
	
	if ( m_Fitnesses2[ index ]->state != 
	     PopulationMember::INVALID_GENOTYPE ) {
	  i->getGenotypeInfo( 1,
			      &genotypeID,
			      &fitness,
			      &state,
			      &typeNo );
	  
	  m_Fitnesses2[ index ]->genotypeNo = genotypeID;
	  m_Fitnesses2[ index ]->fitnesses.push_back( fitness );
	  m_Fitnesses2[ index ]->state = state;
	  m_Fitnesses2[ index ]->alreadySet = true;
	  }*/
      } // End if type no
    } // End for z
  } // End for all trials

  if ( j1 != m_SubPopulationSize ||
       j2 != m_SubPopulationSize ) {
    SmallString errMsg;
    errMsg += "Found mismatching population sizes, sub "
              "population size should be ";
    errMsg += m_SubPopulationSize;
    errMsg += ", whilst sub population sizes seem are currently ";
    errMsg += j1;
    errMsg += " and ";
    errMsg += j2;
    
    TwoPopulationsRankSelDesException e( errMsg );
    e.fillInStackTrace();
    throw e;
  }

  qsort( m_Fitnesses1,
	 m_SubPopulationSize,
	 sizeof( S_fitnesses * ),
	 sortFitnessStructures );
  
  qsort( m_Fitnesses2,
	 m_SubPopulationSize,
	 sizeof( S_fitnesses * ),
	 sortFitnessStructures );

  /**
   * We'll quickly sort out the fitnesses for stats...
   */
  for ( j = 0; j < m_SubPopulationSize; j++ ) {
    map<unsigned long,PopulationMember>::iterator x;
    x = population.find( m_Fitnesses1[ j ]->genotypeNo );
    if ( x == population.end() ) {
      char errMsg[ 1024 ];
      sprintf( errMsg, "Programming error - trying to find the population "
	       "member of %lu but I can't find it",
	       m_Fitnesses1[ j ]->genotypeNo );
      TwoPopulationsRankSelDesException e( errMsg );
      e.fillInStackTrace();
      throw e;
    }

    x->second.setFitness( m_Fitnesses1[ j ]->databaseFitness );
    x->second.setFitnesses( m_Fitnesses1[ j ]->fitnesses[0] );
    x->second.setState(   m_Fitnesses1[ j ]->state           );
  }

  for ( j = 0; j < m_SubPopulationSize; j++ ) {
    map<unsigned long,PopulationMember>::iterator x;
    x = population.find( m_Fitnesses2[ j ]->genotypeNo );
    if ( x == population.end() ) {
      char errMsg[ 1024 ];
      sprintf( errMsg, "Programming error - trying to find the population "
	       "member of %lu but I can't find it",
	       m_Fitnesses2[ j ]->genotypeNo );
      TwoPopulationsRankSelDesException e( errMsg );
      e.fillInStackTrace();
      throw e;
    }

    x->second.setFitness( m_Fitnesses2[ j ]->databaseFitness );
    x->second.setFitnesses( m_Fitnesses2[ j ]->fitnesses[0] );
    x->second.setState(   m_Fitnesses2[ j ]->state           );
  }

  /**
   * Now we have to reproduce populationSize genotypes
   * with the probability proportional to their place
   * in the rank.
   */
  
  FLOAT currentProbability = 0.0;
  for ( j = 0; j < m_SubPopulationSize; j++ ) {
    m_Fitnesses1[ j ]->lowerProbability = currentProbability;
    currentProbability += j + 1;
    m_Fitnesses1[ j ]->higherProbability = currentProbability;
  }

  /*{ // DEBUG block
    SmallString msg;
    msg += "Here is the order/fitnesses (pop 1):";
    for ( j = 0; j < m_SubPopulationSize; j++ ) {
      msg += "\n";
      msg += j;
      msg += ":\t";
      msg += m_Fitnesses1[ j ]->genotypeNo;
      msg += ":\t(";
      vector<FLOAT>::iterator z;
      for ( z = m_Fitnesses1[ j ]->fitnesses.begin();
	    z != m_Fitnesses1[ j ]->fitnesses.end();
	    z++ ) {
	if ( z != m_Fitnesses1[ j ]->fitnesses.begin() ) {
	  msg += ",";
	}
	msg += *z;
      }
      msg += "):\t";
      msg += m_Fitnesses1[ j ]->state;
      msg += ":\t";
      msg += m_Fitnesses1[ j ]->lowerProbability;
      msg += ":\t";
      msg += m_Fitnesses1[ j ]->higherProbability;
    }
    
    QErrorLog::writeError( msg );
    }*/

  vector<unsigned long> toBeReproduced1;
  while ( toBeReproduced1.size() < m_SubPopulationSize - m_NoOfElite ) {
    int genotypeToReproduce = -1;

    while ( genotypeToReproduce == -1 ) {
      FLOAT randomNo = m_RandomNumbers->randomDouble() * currentProbability;

      /**
       * Where does this lie in our ranking?
       */
      for ( j = 0; 
	    j < m_SubPopulationSize && genotypeToReproduce == -1; 
	    j++ ) {
	if ( randomNo >= m_Fitnesses1[ j ]->lowerProbability &&
	     randomNo <  m_Fitnesses1[ j ]->higherProbability ) {
	  genotypeToReproduce = m_Fitnesses1[ j ]->genotypeNo;
	}
      }

      if ( genotypeToReproduce == -1 ) {
	SmallString errMsg;
	errMsg += "Cannot find a genotype for the random number ";
	errMsg += randomNo;
	
	TwoPopulationsRankSelDesException e( errMsg );
	e.fillInStackTrace();
	throw e;
      }
      
      /**
       * Is it valid?
       */
      map<unsigned long,PopulationMember>::const_iterator s;
      s = population.find( genotypeToReproduce );
      if ( s->second.getState() == PopulationMember::INVALID_GENOTYPE ) {
	genotypeToReproduce = -1;
      } else {
	toBeReproduced1.push_back( genotypeToReproduce );
      } 
    }
  }

  /**
   * Now for the next sub population.
   */
  currentProbability = 0.0;
  for ( j = 0; j < m_SubPopulationSize; j++ ) {
    m_Fitnesses2[ j ]->lowerProbability = currentProbability;
    currentProbability += j + 1;
    m_Fitnesses2[ j ]->higherProbability = currentProbability;
  }

  /*{ // DEBUG block
    SmallString msg;
    msg += "Here is the order/fitnesses (pop 2):";
    for ( j = 0; j < m_SubPopulationSize; j++ ) {
      msg += "\n";
      msg += j;
      msg += ":\t";
      msg += m_Fitnesses2[ j ]->genotypeNo;
      msg += ":\t(";
      vector<FLOAT>::iterator z;
      for ( z = m_Fitnesses2[ j ]->fitnesses.begin();
	    z != m_Fitnesses2[ j ]->fitnesses.end();
	    z++ ) {
	if ( z != m_Fitnesses2[ j ]->fitnesses.begin() ) {
	  msg += ",";
	}
	msg += *z;
      }
      msg += "):\t";
      msg += m_Fitnesses2[ j ]->state;
      msg += ":\t";
      msg += m_Fitnesses2[ j ]->lowerProbability;
      msg += ":\t";
      msg += m_Fitnesses2[ j ]->higherProbability;
    }
    
    QErrorLog::writeError( msg );
    QErrorLog::writeError( "*************************" );
    }*/

  vector<unsigned long> toBeReproduced2;
  while ( toBeReproduced2.size() < m_SubPopulationSize - m_NoOfElite ) {
    int genotypeToReproduce = -1;

    while ( genotypeToReproduce == -1 ) {
      FLOAT randomNo = m_RandomNumbers->randomDouble() * currentProbability;
    
      /**
       * Where does this lie in our ranking?
       */
      for ( j = 0; 
	    j < m_SubPopulationSize && genotypeToReproduce == -1; 
	    j++ ) {
	if ( randomNo >= m_Fitnesses2[ j ]->lowerProbability &&
	     randomNo <  m_Fitnesses2[ j ]->higherProbability ) {
	  genotypeToReproduce = m_Fitnesses2[ j ]->genotypeNo;
	}
      }

      if ( genotypeToReproduce == -1 ) {
	SmallString errMsg;
	errMsg += "Cannot find a genotype for the random number ";
	errMsg += randomNo;
	
	TwoPopulationsRankSelDesException e( errMsg );
	e.fillInStackTrace();
	throw e;
      }
      
      /**
       * Is it valid?
       */
      map<unsigned long,PopulationMember>::const_iterator s;
      s = population.find( genotypeToReproduce );
      if ( s == population.end() ) {
	TwoPopulationsRankSelDesException e( "Choosen genotype that doesn't "
					     "exist in the population!" );
	e.fillInStackTrace();
	throw e;
      }

      if ( s->second.getState() == PopulationMember::INVALID_GENOTYPE ) {
	genotypeToReproduce = -1;
      } else {
	toBeReproduced2.push_back( genotypeToReproduce );
      } 
    }
  }

  vector<unsigned long>::const_iterator k;
  for ( k = toBeReproduced1.begin(); k != toBeReproduced1.end(); k++ ) {
    toBeReproduced.push( *k );
  }

  for ( k = toBeReproduced2.begin(); k != toBeReproduced2.end(); k++ ) {
    toBeReproduced.push( *k );
  }

  /**
   * Now we have to set up the right genotypes to be removed.
   */
  for ( j = 0; j < m_SubPopulationSize - m_NoOfElite; j++ ) {
    toBeRemoved.insert( m_Fitnesses1[ j ]->genotypeNo );
  }
  
  for ( j = 0; j < m_SubPopulationSize - m_NoOfElite; j++ ) {
    toBeRemoved.insert( m_Fitnesses2[ j ]->genotypeNo );
  }

  /**
   * Now we remove all the fitnesses, ready for next time.
   */
  for ( j = 0; j < m_SubPopulationSize; j++ ) {
    m_Fitnesses1[ j ]->fitnesses.clear();
    m_Fitnesses2[ j ]->fitnesses.clear();
  }

  /*{ // DEBUG block
    SmallString msg;
    msg += "Reproducing these:";
    vector<unsigned long>::iterator z;
    for ( z = toBeReproduced.begin(); z != toBeReproduced.end(); z++ ) {
      msg += "\n";
      msg += *z;
    }
  
    msg += "\nTo be removed:";
    set<unsigned long>::iterator y;
    for ( y = toBeRemoved.begin(); y != toBeRemoved.end(); y++ ) {
      msg += " ";
      msg += *y;
    }

    msg += "\n";
    QErrorLog::writeError( msg );
    }*/
}

int TwoPopulationsRankSelDes::sortFitnessStructures(
    const void *a1, 
    const void *a2 ) {
  S_fitnesses *s1, *s2;
  
  s1 = *(S_fitnesses **)a1;
  s2 = *(S_fitnesses **)a2;

  if ( s1->state == PopulationMember::INVALID_GENOTYPE ) {
    return -1;
  } else if ( s2->state == PopulationMember::INVALID_GENOTYPE ) {
    return 1;
  } else {
    return s1->fitnessComparator->compareFitness( s1->fitnesses, 
						  s2->fitnesses );
  }
}

bool TwoPopulationsRankSelDes::setNoOfElite( int noOfElite ) {
  if ( noOfElite < 0 ) {
    return false;
  } 

  m_NoOfElite = noOfElite;
  
  return true;
}

bool TwoPopulationsRankSelDes::setNoOfTrialsPerGenotype( int i ) {
  if ( i < 1 ) {
    return false;
  }

  m_NoOfTrialsPerGenotype = i;

  return true;
}

void 
TwoPopulationsRankSelDes::setFitnessComparator(AbstractFitnessComparator *c) {
  /**
   * OK - this is a bit kludgy, but sortFitnessStructures is a static
   * function but we need to pass some class members to it, so we hide
   * it within the struct S_fitness structure.
   */
  int i;
  for ( i = 0; i < m_SubPopulationSize; i++ ) {
    m_Fitnesses1[ i ]->fitnessComparator = c;
    m_Fitnesses2[ i ]->fitnessComparator = c;
  }
}
