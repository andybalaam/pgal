#ifndef PBOSSWORKERFACTORY_H
#define PBOSSWORKERFACTORY_H

/**
 * This class creates Boss/Worker patterns on request. Using
 * noOfWorkers set to 0 ensures no background threads or processes 
 * are used, otherwise the given no of threads are used.
 */

#include "PBossWorker.h"
#include "QException.h"

class PBossWorkerFactory {
public:
  QEXCEPTIONCLASS( InvalidNoOfWorkers );

  static PBossWorker *createBossWorker( int noOfWorkers );
};

#endif


