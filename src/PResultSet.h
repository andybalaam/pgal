/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef PRESULTSET_H
#define PRESULTSET_H

/**
 * This class holds the result set of an SQL query performed by PDatabase.
 */

#include "QException.h"
#include <mysql/mysql.h>
#include <string>
#include <vector>

using namespace std;

class PRow;

class PResultSet {
public:
  QEXCEPTIONCLASS( PResultSetException );

  /**
   * Constructor - supply a MYSQL_RES* created with mysql_store_result or
   * mysql_use_result.
   */
  PResultSet( MYSQL_RES *mysql_res );
  ~PResultSet();
  
  PRow *GetNextRow();
	
  /**
   * NOTE: this will return an unexpected result if the recordset was opened
   * with mysql_use_result rather than mysql_store_result.
   */
  unsigned long GetNumRows();
	
  unsigned long GetFieldNo(     const char    *fieldName );
  unsigned long GetFieldLength( unsigned long  fieldNo   );
	
private:
  PResultSet( PResultSet& );
  
  void FindFieldNames();

  MYSQL_RES           *m_ResultSet;
  vector<SmallString>  m_FieldNames;
  vector<long>         m_FieldLengths;
  bool                 m_HaveGotFieldNames;
};

#endif
