/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef MEMFILE_H
#define MEMFILE_H

/**
 * This holds a variable length area of memory that
 * shrinks and increases as the data changes. It
 * keeps a record of its end, so it should be quicker
 * that strcat'ing all the time.
 */

#include "QException.h"

#define START_LENGTH 512
#define INCREASE_BY  512
#define SHRINK_IF    (2*1024)

#define getSizeMemFile(A) (A->currentSize)

class MemFile {
private:
  struct internalMemFile {
    unsigned char *memory;
    unsigned char *endOfMemory;
    unsigned long currentSize;
    unsigned long spaceAllocated;

    unsigned long noOfPointers;
  } *m_MemFileInstance;

  unsigned long m_CurrentWritingPos;

  void makeNewEmptyInstance();
  void makeCopy();
  void increaseSize( int size );

public:
  QEXCEPTIONCLASS( MemFileException );

  MemFile();
  MemFile( const MemFile& );
  ~MemFile();

  MemFile& operator=( const MemFile& );

  void append( const unsigned char *bytes, unsigned long noOfBytes );
  void append( const MemFile& );
  void makeEmpty();
  int  getSize() const;
  const unsigned char *getMemFile( unsigned long *size ) const;

  /**
   * These are the file type functions
   */
  void          write( const unsigned char *bytes, unsigned long noOfBytes );
  void          seek( unsigned long pos );
  unsigned long tell();
  int           mgetc();
  void          mungetc( int );
};

#endif

