#ifndef FIRSTFITNESSCOMPARATOR_H
#define FIRSTFITNESSCOMPARATOR_H

/**
 * This class compares vectors of fitness by simply comparing the first element.
 */

#include "AbstractFitnessComparator.h"

class FirstFitnessComparator : public AbstractFitnessComparator {
public:
  /**
   * This returns -1 if a < b, 0 if a == b, and 1 if a > b
   */
  int   compareFitness(   const vector<vector<FLOAT> >& a, 
			  const vector<vector<FLOAT> >& b );

  FLOAT calculateFitness( const vector<vector<FLOAT> >& );
};

#endif
