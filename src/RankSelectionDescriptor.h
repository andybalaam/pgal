/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef RANKSELECTIONDESCRIPTOR_H
#define RANKSELECTIONDESCRIPTOR_H

#include "AbstractGenerationDescriptor.h"
#include "QException.h"
#include "RandomNumberGenerator.h"
#include "PopulationMember.h"
#include "TrialInfo.h"
#include "AverageFitnessComparator.h"
#include <vector>
#include <queue>
#include <map>

using namespace std;

class RankSelectionDescriptor : public AbstractGenerationDescriptor {
public:
  QEXCEPTIONCLASS( RankSelectionDescriptorException );

  RankSelectionDescriptor( int populationSize );
  virtual ~RankSelectionDescriptor();

  virtual void getGenotypesToSendToTrial( 
      queue<TrialInfo>&                          toSendToTrial,
      const map<unsigned long,PopulationMember>& population );
  
  virtual void getNextGenerationInformation( 
      set<unsigned long>&                   toBeRemoved,
      queue<unsigned long>&                 toBeReproduced,
      map<unsigned long,PopulationMember>&  population,
      const vector<TrialInfo>&              trials );

  bool setNoOfElite( int noOfElite );
  bool setNoOfTrialsPerGenotype( int );
  void setFitnessComparator( AbstractFitnessComparator * );

protected:
  class S_fitnesses {
  public:
    /**
     * The top vector is for each trial the genotype is run in. The
     * inside one is each subtrial within that trial. So if the vectors
     * were as follows:
     *
     * trial one: 45 56 23 65
     * trial two: 34 34 65 23
     *
     * then the average comparator would produce a four number vector
     * containing the average of (45,34) and (56,34) etc.
     */
    vector<vector<FLOAT> >     fitnesses;
    FLOAT                      databaseFitness;
    unsigned long              genotypeNo;
    unsigned char              state;
    FLOAT                      lowerProbability;
    FLOAT                      higherProbability;
    bool                       alreadySet;
    AbstractFitnessComparator *fitnessComparator;
  };

  static int sortFitnessStructures( const void *, const void * );

  static AverageFitnessComparator m_DefaultFitnessComparator;

  int                     m_NoOfTrialsPerGenotype;
  int                     m_PopulationSize;
  struct S_fitnesses    **m_AllFitnesses;
  RandomNumberGenerator  *m_RandomNumbers;
  int                     m_NoOfElite;
};

#endif
