#include "FirstFitnessComparator.h"

int 
FirstFitnessComparator::compareFitness( const vector<vector<FLOAT> >& a,
					  const vector<vector<FLOAT> >& b ) {
  
  FLOAT v = *(a.begin()->begin()) - *(b.begin()->begin());
  return v == 0 ? 0 : ( v < 0 ? -1 : 1 );
  
}

FLOAT 
FirstFitnessComparator::calculateFitness( const vector<vector<FLOAT> >& a ) {
  
  return *(a.begin()->begin());
  
}
