/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "PResultSet.h"
#include "PRow.h"

PResultSet::PResultSet( MYSQL_RES* mysql_res ) : 
  m_ResultSet( mysql_res ),
  m_HaveGotFieldNames( false ) { }

PResultSet::~PResultSet() {
  mysql_free_result( m_ResultSet );
}

unsigned long PResultSet::GetNumRows() {
  return mysql_num_rows( m_ResultSet );
}

unsigned long PResultSet::GetFieldNo( const char *fieldName ) {
  if ( !m_HaveGotFieldNames ) {
    FindFieldNames();
  }
  
  int i;
  for ( i=0; i < m_FieldNames.size(); i++ ) {
    if ( m_FieldNames[ i ] == fieldName ) {
      return i;
    }
  }
	
  SmallString errMsg;
  errMsg += "Field name '";
  errMsg += fieldName;
  errMsg += "' not found";

  PResultSetException e( errMsg );
  e.fillInStackTrace();
  throw e;
}

unsigned long PResultSet::GetFieldLength( unsigned long fieldNo ) {
  if( !m_HaveGotFieldNames ) {
    FindFieldNames();
  }
  
  return m_FieldLengths[ fieldNo ];
}

PRow *PResultSet::GetNextRow() {
  return new PRow( mysql_fetch_row( m_ResultSet ), this );
}

void PResultSet::FindFieldNames() {
  MYSQL_FIELD   *field;
  unsigned long  i;
  unsigned long *lengths;
  
  lengths = mysql_fetch_lengths( m_ResultSet );
  
  m_HaveGotFieldNames = true;
  for ( i = 0; field = mysql_fetch_field( m_ResultSet ); i++ ) {
    m_FieldNames.push_back( field->name );
    m_FieldLengths.push_back( lengths[ i ] );
  }
  
  delete lengths;	
}
