/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "NumberGenotype.h"
#include <pgal/bitfield.h>
#include <pgal/RandomNumberGenerator.h>
#include <pgal/EvolutionaryRun.h>
#include <pgal/QErrorLog.h>

#define ARRAY_SIZE 10
#define ABS(A) ((A)>0?(A):-(A))

NumberGenotype::NumberGenotype() {
  int i;

  m_RandomNumberGenerator = RandomNumberGenerator::getDefaultGoodGenerator();

  m_AllNumbers = new int[ ARRAY_SIZE ];

  for ( i = 0; i < ARRAY_SIZE; i++ ) {
    m_AllNumbers[ i ] = m_RandomNumberGenerator->random16BitInt( ARRAY_SIZE+1 );
  }
}

NumberGenotype::NumberGenotype( const NumberGenotype& g1 ) {
  m_RandomNumberGenerator = g1.m_RandomNumberGenerator;
  m_AllNumbers = new int[ ARRAY_SIZE ];
  memcpy( m_AllNumbers, g1.m_AllNumbers, sizeof( int ) * ARRAY_SIZE );
}

NumberGenotype::NumberGenotype( const unsigned char *bytes, int sizeInBits ) {
  m_RandomNumberGenerator = RandomNumberGenerator::getDefaultGoodGenerator();

  m_AllNumbers = NULL;

  if ( BYTES_IN_BITFIELD( sizeInBits ) != ARRAY_SIZE * sizeof( int ) ) {
    char errMsg[ 1024 ];
    sprintf( errMsg, "size in bits (%d) doesn\'t match proper size (%d)",
	           sizeInBits, ARRAY_SIZE );
    NumberGenotypeException e( errMsg );
    e.fillInStackTrace();
    throw e;
  }

  m_AllNumbers = new int[ ARRAY_SIZE ];
  memcpy( m_AllNumbers, bytes, BYTES_IN_BITFIELD( sizeInBits ) );
}

NumberGenotype::~NumberGenotype() {
  if ( m_AllNumbers != NULL ) {
    delete m_AllNumbers;
  }
}

const unsigned char *NumberGenotype::getGenotypeAsBits( unsigned long *sizeInBits )const{
  *sizeInBits = (long)ARRAY_SIZE * sizeof( int ) * 8;
  return (const unsigned char *)m_AllNumbers;
}

void NumberGenotype::mutate() {
  bool goOn = true;
  int indexNo, randomNum;
  int noOfTries;

  indexNo = m_RandomNumberGenerator->random16BitInt( ARRAY_SIZE+1 );

  switch( m_RandomNumberGenerator->random16BitInt( 2 ) ) {
  case 0:
    m_AllNumbers[ indexNo ]++;
    if ( m_AllNumbers[ indexNo ] > ARRAY_SIZE ) {
      m_AllNumbers[ indexNo ] = ARRAY_SIZE;
    }
    break;
    
  default:
    m_AllNumbers[ indexNo ]--;
    if ( m_AllNumbers[ indexNo ] < 0 ) {
      m_AllNumbers[ indexNo ] = 0;
    }
    break;
  }
}

float NumberGenotype::calculateFitness() const {
  int i, totalFitness = 0;
  long j;

  for ( i = 0; i < ARRAY_SIZE; i++ ) {
    if ( m_AllNumbers[ i ] == -1 ) {
      EvolutionaryRun::InvalidTrial e( "Invalid" );
      e.fillInStackTrace();
      throw e;
    }

    /*if ( m_AllNumbers[ i ] == 3 ) {
      EvolutionaryRun::InvalidTrial e( "Invalid" );
      e.fillInStackTrace();
      throw e;
      }*/

    totalFitness += m_AllNumbers[ i ];
  }

  return totalFitness / (float)ARRAY_SIZE;
}

void NumberGenotype::print() {
  int i;
  for ( i = 0; i < ARRAY_SIZE; i++ ) {
    fprintf( stderr, "% 3d ", m_AllNumbers[ i ] );
  }
  fputs( "\n", stderr );
}

