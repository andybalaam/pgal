/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "NumberTrialFactory.h"
#include "NumberGenotypeFactory.h"
#include <pgal/ParetoFrontFitnessComparator.h>
#include <pgal/TwoSubPopulationsRankSelDes.h>
#include <pgal/RandomNumberGenerator.h>
#include <pgal/EvolutionaryRun.h>
#include <stdlib.h>
#include <time.h>

int main( int argc, char **argv ) {
  try {
    NumberGenotypeFactory       genotypeFactory( 10, 4 );
    NumberTrialFactory          trialFactory;
    TwoSubPopulationsRankSelDes generationDescriptor( 10, 4 );

    ParetoFrontFitnessComparator p1( 2, 2, 0 );
    generationDescriptor.setFitnessComparator1( &p1 );

    ParetoFrontFitnessComparator p2( 1, 1, 0 );
    generationDescriptor.setFitnessComparator2( &p2 );

    /*generationDescriptor.setNoOfElite( 1 );
      generationDescriptor.setNoOfTrialsPerGenotype( 1 );*/
    
    EvolutionaryRun *evolutionaryRun;
    
    if ( argc == 1 ) {
      evolutionaryRun = new EvolutionaryRun( 14,
					     &genotypeFactory,
					     &generationDescriptor,
					     &trialFactory,
					     0,
					     "pgalUser",
					     "pgal",
					     "127.0.0.1",
					     "pgalDB",
                                             0 );
    } else {
      evolutionaryRun = new EvolutionaryRun( 1,
					     14,
					     &genotypeFactory,
					     &generationDescriptor,
					     &trialFactory,
					     0,
					     "pgalUser",
					     "pgal",
					     "127.0.0.1",
					     "pgalDB",
                                             0 );
    }

    //evolutionaryRun->setWriteGenotypeBodyEvery( 100 );

    evolutionaryRun->setNoOfPopulations( 2 );
    evolutionaryRun->start( "Hello" );

    delete evolutionaryRun;
  } catch( QException& e ) {
    fprintf( stderr, "Caught exception - %s\n%s\n",
	     e.getMessage(),
	     e.getStackTrace() );
    exit( 1 );
  }

  return 0;
}
