/**
 * Copyright 2002 Ian Macinnes, Andrew Balaam
 *
 * This file is part of the Parallel Genetic Algorithm Library (PGAL).
 *
 * PGAL is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "NumberTrial.h"
#include <pgal/PopulationMember.h>

#define NO_OF_SUBTRIALS1 4
#define NO_OF_SUBTRIALS2 2

NumberTrial::NumberTrial( NumberGenotype *g1,
			  NumberGenotype *g2 ) : 
  m_Genotype1( g1 ),
  m_Genotype2( g2 ) { }

void NumberTrial::performTrial( TrialInfo& trialInfo ) {
  vector<FLOAT> subTrials;
  int i;
  FLOAT average = 0;

  for ( i = 0; i < NO_OF_SUBTRIALS1 / 2; i++ ) {
    FLOAT fitness = m_Genotype1->calculateFitness();
    average += fitness;
    subTrials.push_back( fitness );
  }

  for ( i = NO_OF_SUBTRIALS1 / 2; i < NO_OF_SUBTRIALS1; i++ ) {
    FLOAT fitness = m_Genotype1->calculateFitness() * 10;
    average += fitness;
    subTrials.push_back( fitness );
  }
  
  average /= NO_OF_SUBTRIALS1;

  trialInfo.setGenotypeInfo( m_Genotype1->getIndexNo(),
			     subTrials,
                             average,
			     PopulationMember::KNOWN_FITNESS,
			     0 );

  subTrials.clear();
  average = 0;

  for ( i = 0; i < NO_OF_SUBTRIALS2 / 2; i++ ) {
    FLOAT fitness = m_Genotype2->calculateFitness();
    average += fitness;
    subTrials.push_back( fitness );
  }

  for ( i = NO_OF_SUBTRIALS2 / 2; i < NO_OF_SUBTRIALS2; i++ ) {
    FLOAT fitness = m_Genotype2->calculateFitness() * 10;
    average += fitness;
    subTrials.push_back( fitness );
  }
  
  average /= NO_OF_SUBTRIALS2;

  trialInfo.setGenotypeInfo( m_Genotype2->getIndexNo(),
                             subTrials,
			     average,
			     PopulationMember::KNOWN_FITNESS,
			     1 );
}
